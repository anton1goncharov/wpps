DROP TABLE IF EXISTS pjm_load;

CREATE TABLE pjm_load
(
    id      BIGINT PRIMARY KEY AUTO_INCREMENT,
    time    DATETIME        NOT NULL,
    area    VARCHAR(10)     NOT NULL,
    type    VARCHAR(10)     NOT NULL,
    load_kw DECIMAL(36, 18) NOT NULL,
    FOREIGN KEY (area) REFERENCES dict_pjm_area (name) ON UPDATE CASCADE,
    FOREIGN KEY (type) REFERENCES dict_pjm_load_type (name) ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4
    ROW_FORMAT = COMPRESSED
    KEY_BLOCK_SIZE = 4;

CREATE UNIQUE INDEX pjm_load$bk ON pjm_load (time, area, type);
