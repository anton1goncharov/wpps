DROP TABLE IF EXISTS dict_pjm_load_type;

CREATE TABLE dict_pjm_load_type
(
    name  VARCHAR(10) PRIMARY KEY,
    label VARCHAR(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

INSERT INTO dict_pjm_load_type
VALUES ('I_5M', 'Instantaneous (5 min)'),
       ('F_H', 'Forecast (hourly)');