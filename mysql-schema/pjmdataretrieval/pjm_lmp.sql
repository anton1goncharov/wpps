DROP TABLE IF EXISTS pjm_lmp;

CREATE TABLE pjm_lmp
(
    id        BIGINT PRIMARY KEY AUTO_INCREMENT,
    time      DATETIME        NOT NULL,
    area      VARCHAR(10)     NOT NULL,
    type      VARCHAR(10)     NOT NULL,
    kwh_price DECIMAL(36, 18) NOT NULL,
    FOREIGN KEY (area) REFERENCES dict_pjm_area (name) ON UPDATE CASCADE,
    FOREIGN KEY (type) REFERENCES dict_pjm_lmp_type (name) ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4
    ROW_FORMAT = COMPRESSED
    KEY_BLOCK_SIZE = 4;

CREATE UNIQUE INDEX pjm_lmp$bk ON pjm_lmp (time, area, type);
