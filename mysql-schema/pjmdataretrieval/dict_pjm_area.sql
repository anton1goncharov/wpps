DROP TABLE IF EXISTS dict_pjm_area;

CREATE TABLE dict_pjm_area
(
    name  VARCHAR(10) PRIMARY KEY,
    label VARCHAR(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

INSERT INTO dict_pjm_area
VALUES ('PJM_RTO', 'PJM-RTO'),
       ('MIDATL', 'Mid-Atlantic Region'),
       ('APS', 'APS Zone'),
       ('BGE', 'BGE Zone'),
       ('PEPCO', 'PEPCO Zone'),
       ('WESTERN_HUB', 'WESTERN HUB');