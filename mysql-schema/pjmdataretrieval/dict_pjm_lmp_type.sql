DROP TABLE IF EXISTS dict_pjm_lmp_type;

CREATE TABLE dict_pjm_lmp_type
(
    name  VARCHAR(10) PRIMARY KEY,
    label VARCHAR(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

INSERT INTO dict_pjm_lmp_type
VALUES ('RT_5M', 'Real-Time Five Minute'),
       ('RT_1H', 'Real-Time Hourly'),
       ('DA_1H', 'Day-Ahead Hourly');
