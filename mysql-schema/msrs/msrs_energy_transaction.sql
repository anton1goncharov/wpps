DROP TABLE IF EXISTS msrs_energy_transaction;

CREATE TABLE msrs_energy_transaction
(
    id                                 BIGINT PRIMARY KEY AUTO_INCREMENT,
    order_index                        INTEGER         NOT NULL,
    date                               DATE            NOT NULL,
    transaction_type                   VARCHAR(100)    NOT NULL,
    transaction_id                     VARCHAR(20),
    total_kwh                          DECIMAL(36, 18) NOT NULL,
    source_pnode_name                  VARCHAR(50),
    source_pnode_id                    VARCHAR(20),
    sink_pnode_name                    VARCHAR(50),
    sink_pnode_id                      VARCHAR(20),
    seller                             VARCHAR(50),
    buyer                              VARCHAR(50),
    msrs_energy_transactions_report_id BIGINT          NOT NULL,
    FOREIGN KEY (msrs_energy_transactions_report_id) REFERENCES msrs_energy_transactions_report (id) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE INDEX msrs_energy_transaction$bk ON msrs_energy_transaction (msrs_energy_transactions_report_id, order_index);