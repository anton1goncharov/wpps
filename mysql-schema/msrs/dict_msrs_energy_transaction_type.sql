DROP TABLE IF EXISTS dict_msrs_energy_transaction_type;

CREATE TABLE dict_msrs_energy_transaction_type
(
    name  VARCHAR(10) PRIMARY KEY,
    label VARCHAR(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

INSERT INTO dict_msrs_energy_transaction_type
VALUES ('DA_1D', 'Day-Ahead Daily'),
       ('RT_1D', 'Real-Time Daily');
