DROP TABLE IF EXISTS msrs_month_to_date_billing_line;

CREATE TABLE msrs_month_to_date_billing_line
(
    id                                           BIGINT PRIMARY KEY AUTO_INCREMENT,
    order_index                                  INTEGER         NOT NULL,
    billing_charge_type                          VARCHAR(10)     NOT NULL,
    charge_code                                  VARCHAR(20)     NOT NULL,
    adjustment                                   BOOLEAN         NOT NULL,
    name                                         VARCHAR(200)    NOT NULL,
    source_billing_period_start                  DATE,
    amount                                       DECIMAL(36, 18) NOT NULL,
    msrs_month_to_date_billing_summary_report_id BIGINT          NOT NULL,
    FOREIGN KEY (billing_charge_type) REFERENCES dict_msrs_billing_charge_type (name) ON UPDATE CASCADE,
    FOREIGN KEY (msrs_month_to_date_billing_summary_report_id) REFERENCES msrs_month_to_date_billing_summary_report (id) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE INDEX msrs_month_to_date_billing_line$bk
    ON msrs_month_to_date_billing_line (msrs_month_to_date_billing_summary_report_id, order_index);
