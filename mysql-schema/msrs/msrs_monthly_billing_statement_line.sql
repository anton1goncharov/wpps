DROP TABLE IF EXISTS msrs_monthly_billing_statement_line;

CREATE TABLE msrs_monthly_billing_statement_line
(
    id                                       BIGINT PRIMARY KEY AUTO_INCREMENT,
    order_index                              INTEGER         NOT NULL,
    billing_charge_type                      VARCHAR(10)     NOT NULL,
    charge_code                              VARCHAR(20)     NOT NULL,
    adjustment                               BOOLEAN         NOT NULL,
    name                                     VARCHAR(200)    NOT NULL,
    source_billing_period_start              DATE,
    amount                                   DECIMAL(36, 18) NOT NULL,
    msrs_monthly_billing_statement_report_id BIGINT          NOT NULL,
    FOREIGN KEY (billing_charge_type) REFERENCES dict_msrs_billing_charge_type (name) ON UPDATE CASCADE,
    FOREIGN KEY (msrs_monthly_billing_statement_report_id) REFERENCES msrs_monthly_billing_statement_report (id) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE INDEX msrs_monthly_billing_statement_line$bk
    ON msrs_monthly_billing_statement_line (msrs_monthly_billing_statement_report_id, order_index);
