DROP TABLE IF EXISTS dict_msrs_billing_charge_type;

CREATE TABLE dict_msrs_billing_charge_type
(
    name  VARCHAR(10) PRIMARY KEY,
    label VARCHAR(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

INSERT INTO dict_msrs_billing_charge_type
VALUES ('CHARGE', 'Charge'),
       ('CREDIT', 'Credit');
