DROP TABLE IF EXISTS msrs_month_to_date_billing_line_daily_amount;

CREATE TABLE msrs_month_to_date_billing_line_daily_amount
(
    id                                 BIGINT PRIMARY KEY AUTO_INCREMENT,
    `date`                             DATE            NOT NULL,
    amount                             DECIMAL(36, 18) NOT NULL,
    msrs_month_to_date_billing_line_id BIGINT          NOT NULL,
    FOREIGN KEY (msrs_month_to_date_billing_line_id) REFERENCES msrs_month_to_date_billing_line (id) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE INDEX msrs_month_to_date_billing_line_daily_amount$bk ON msrs_month_to_date_billing_line_daily_amount (msrs_month_to_date_billing_line_id, `date`);
