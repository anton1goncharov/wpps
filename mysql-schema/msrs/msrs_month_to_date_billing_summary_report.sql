DROP TABLE IF EXISTS msrs_month_to_date_billing_summary_report;

CREATE TABLE msrs_month_to_date_billing_summary_report
(
    id                                  BIGINT PRIMARY KEY AUTO_INCREMENT,
    customer_account                    VARCHAR(100)    NOT NULL,
    report_creation_timestamp           DATETIME        NOT NULL,
    billing_start_date                DATE            NOT NULL,
    billing_end_date                  DATE            NOT NULL,
    total_charges                       DECIMAL(36, 18) NOT NULL,
    total_credits                       DECIMAL(36, 18) NOT NULL,
    monthly_billing_net_total           DECIMAL(36, 18) NOT NULL,
    month_last_weekly_billing_net_total DECIMAL(36, 18) NOT NULL,
    total_due_receivable                DECIMAL(36, 18) NOT NULL,
    complete                            BOOLEAN         NOT NULL DEFAULT FALSE,
    data                                LONGBLOB        NOT NULL,
    filename                            VARCHAR(100)    NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE UNIQUE INDEX msrs_month_to_date_billing_summary_report$bk
    ON msrs_month_to_date_billing_summary_report (customer_account,billing_start_date);