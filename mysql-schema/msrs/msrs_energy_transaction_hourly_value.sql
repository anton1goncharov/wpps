DROP TABLE IF EXISTS msrs_energy_transaction_hourly_value;

CREATE TABLE msrs_energy_transaction_hourly_value
(
    id                         BIGINT PRIMARY KEY AUTO_INCREMENT,
    time                       DATETIME        NOT NULL,
    kwh                        DECIMAL(36, 18) NOT NULL,
    msrs_energy_transaction_id BIGINT          NOT NULL,
    FOREIGN KEY (msrs_energy_transaction_id) REFERENCES msrs_energy_transaction (id) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE INDEX msrs_energy_transaction_hourly_value$bk ON msrs_energy_transaction_hourly_value (msrs_energy_transaction_id, time);