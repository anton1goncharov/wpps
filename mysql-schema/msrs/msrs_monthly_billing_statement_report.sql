DROP TABLE IF EXISTS msrs_monthly_billing_statement_report;

CREATE TABLE msrs_monthly_billing_statement_report
(
    id                                BIGINT PRIMARY KEY AUTO_INCREMENT,
    customer_account                  VARCHAR(100)    NOT NULL,
    customer_code                     VARCHAR(10)     NOT NULL,
    customer_id                       VARCHAR(10)     NOT NULL,
    report_creation_timestamp         DATETIME        NULL,
    final_billing_statement_issued    DATETIME        NOT NULL,
    billing_start_date                DATE            NOT NULL,
    billing_end_date                  DATE            NOT NULL,
    invoice_number                    VARCHAR(20)     NOT NULL,
    invoice_due_date                  DATETIME        NOT NULL,
    total_charges                     DECIMAL(36, 18) NOT NULL,
    total_credits                     DECIMAL(36, 18) NOT NULL,
    monthly_billing_net_total         DECIMAL(36, 18) NOT NULL,
    previous_weekly_billing_net_total DECIMAL(36, 18) NOT NULL,
    total_due_receivable              DECIMAL(36, 18) NOT NULL,
    complete                          BOOLEAN         NOT NULL DEFAULT FALSE,
    data                              LONGBLOB        NOT NULL,
    filename                          VARCHAR(100)    NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE UNIQUE INDEX msrs_monthly_billing_statement_report$bk ON msrs_monthly_billing_statement_report (customer_account, billing_start_date);