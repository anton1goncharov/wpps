DROP TABLE IF EXISTS msrs_energy_transactions_report;

CREATE TABLE msrs_energy_transactions_report
(
    id                        BIGINT PRIMARY KEY AUTO_INCREMENT,
    energy_transaction_type   VARCHAR(10)  NOT NULL,
    customer_account          VARCHAR(100) NOT NULL,
    report_creation_timestamp DATETIME     NOT NULL,
    billing_start_date        DATE         NOT NULL,
    billing_end_date          DATE         NOT NULL,
    complete                  BOOLEAN      NOT NULL DEFAULT FALSE,
    data                      LONGBLOB     NOT NULL,
    filename                  VARCHAR(100) NULL,
    FOREIGN KEY (energy_transaction_type) REFERENCES dict_msrs_energy_transaction_type (name) ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE UNIQUE INDEX msrs_energy_transactions_report$bk
    ON msrs_energy_transactions_report (customer_account, billing_start_date, energy_transaction_type);
