package com.enelytics.wpps.service.msrs.updater;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class MonthlyBillingStatementReportsUpdaterTest {

    @Autowired
    MsrsMonthlyBillingStatementReportsUpdateService updateService;

    @Test
    public void updateMonthlyBillingStatementReports() {
        updateService.updateMonthlyBillingStatementReports();
    }

}