package com.enelytics.wpps.service.msrs.updater;

import com.enelytics.wpps.service.msrs.parser.exception.MsrsReportParsingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class EnergyTransactionsReportsUpdaterTest {

    @Autowired
    private MsrsEnergyTransactionsReportsUpdateService updater;

    @Test
    public void updateDADailyEnergyTransactions() throws IOException, MsrsReportParsingException {
        updater.updateDADailyEnergyTransactionsReports();
    }

    @Test
    public void updateRTDailyEnergyTransactions() throws IOException, MsrsReportParsingException {
        updater.updateRTDailyEnergyTransactionsReports();
    }

}