package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.common;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class PricingNodeIdFetcherTest {

    @Autowired
    private PjmPricingNodeIdFetcher pricingNodeIdFetcher;

    @Test
    public void fetchPricingNodeIdByPricingNodeName() throws IOException, InvalidApiResponseException {
        long nodeId = pricingNodeIdFetcher.getPricingNodeId(PjmArea.PJM_RTO);
        pricingNodeIdFetcher.getPricingNodeId(PjmArea.PJM_RTO);
        pricingNodeIdFetcher.getPricingNodeId(PjmArea.PJM_RTO);
    }
}