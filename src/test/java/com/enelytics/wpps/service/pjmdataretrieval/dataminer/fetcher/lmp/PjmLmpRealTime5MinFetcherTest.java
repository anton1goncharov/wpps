package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.time.Instant;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class PjmLmpRealTime5MinFetcherTest {

    @Autowired
    PjmLmpRealTime5MinFetcher pjmLmpRealTime5MinFetcher;

    @Test
    public void fetchStartDate() throws IOException, InvalidApiResponseException {
        Instant instant = pjmLmpRealTime5MinFetcher.fetchStartDate(PjmArea.PJM_RTO);
        pjmLmpRealTime5MinFetcher.fetchStartDate(PjmArea.MIDATL);
    }

}