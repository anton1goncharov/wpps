package com.enelytics.wpps.persistance.repositories.msrs;

import com.enelytics.wpps.persistance.domain.entities.msrs.MsrsReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface MsrsReportBaseRepository<T extends MsrsReport> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {

    @Query("select t from #{#entityName} t where t.billingStartDate = ?1 and t.billingEndDate = ?2 " +
            "order by t.billingStartDate asc")
    List<T> findByBillingPeriod(LocalDate billingStartDate, LocalDate billingEndDate);

    default List<T> findByMonth(YearMonth month) {
        return findByBillingPeriod(month.atDay(1), month.atEndOfMonth());
    }

    default Optional<T> findFirstByMonth(YearMonth month) {
        List<T> byMonth = findByMonth(month);

        return getFirstOptional(byMonth);
    }

    @Query("select t from #{#entityName} t where t.complete = false order by t.billingStartDate asc ")
    List<T> findNotComplete();

    default Optional<T> findFirstNotComplete() {
        List<T> notComplete = findNotComplete();

        return getFirstOptional(notComplete);
    }

    List<T> findAll(Specification<T> specification);

    Page<T> findAll(Specification<T> specification, Pageable pageable);

    default Optional<T> getFirstOptional(List<T> reports) {
        if (reports.isEmpty())
            return Optional.empty();

        return Optional.of(reports.get(0));
    }

}
