package com.enelytics.wpps.persistance.repositories.msrs;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingSummaryReport;
import org.springframework.stereotype.Repository;

import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

@Repository
public interface MsrsMonthToDateBillingSummaryReportRepository extends MsrsReportBaseRepository<MsrsMonthToDateBillingSummaryReport> {

    default Optional<MsrsMonthToDateBillingSummaryReport> findFirstByMonth(YearMonth month) {
        List<MsrsMonthToDateBillingSummaryReport> byMonth = findByMonth(month);

        return getFirstOptional(byMonth);
    }

}
