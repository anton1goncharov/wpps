package com.enelytics.wpps.persistance.repositories.msrs;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MsrsEnergyTransactionRepository extends JpaRepository<MsrsEnergyTransaction, Long>, JpaSpecificationExecutor<MsrsEnergyTransaction> {

}
