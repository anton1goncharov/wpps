package com.enelytics.wpps.persistance.repositories.msrs;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionsReport;
import com.enelytics.wpps.persistance.domain.types.msrs.MsrsEnergyTransactionType;
import org.springframework.stereotype.Repository;

import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

@Repository
public interface MsrsEnergyTransactionsReportRepository extends MsrsReportBaseRepository<MsrsEnergyTransactionsReport> {

    default Optional<MsrsEnergyTransactionsReport> findFirstNotCompleteByType(MsrsEnergyTransactionType type) {
        List<MsrsEnergyTransactionsReport> notComplete = findNotComplete();

        if (notComplete.isEmpty()) {
            return Optional.empty();
        }

        return getByEnergyTransactionType(notComplete, type);
    }

    default Optional<MsrsEnergyTransactionsReport> findFirstByTypeAndMonth(MsrsEnergyTransactionType type, YearMonth month) {
        List<MsrsEnergyTransactionsReport> byMonth = findByMonth(month);

        if (byMonth.isEmpty()) {
            return Optional.empty();
        }

        return getByEnergyTransactionType(byMonth, type);
    }

    default Optional<MsrsEnergyTransactionsReport> getByEnergyTransactionType(List<MsrsEnergyTransactionsReport> reports, MsrsEnergyTransactionType type) {
        return reports.stream()
                .filter(report -> report.getEnergyTransactionType().equals(type))
                .findFirst();
    }

}
