package com.enelytics.wpps.persistance.repositories.msrs;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementReport;
import org.springframework.stereotype.Repository;

import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

@Repository
public interface MsrsMonthlyBillingStatementReportRepository extends MsrsReportBaseRepository<MsrsMonthlyBillingStatementReport> {

    default Optional<MsrsMonthlyBillingStatementReport> findFirstByMonth(YearMonth month) {
        List<MsrsMonthlyBillingStatementReport> byMonth = findByMonth(month);

        return getFirstOptional(byMonth);
    }

}
