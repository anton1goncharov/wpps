package com.enelytics.wpps.persistance.repositories.msrs;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionHourlyValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MsrsEnergyTransactionHourlyValueRepository extends JpaRepository<MsrsEnergyTransactionHourlyValue, Long>, JpaSpecificationExecutor<MsrsEnergyTransactionHourlyValue> {

}
