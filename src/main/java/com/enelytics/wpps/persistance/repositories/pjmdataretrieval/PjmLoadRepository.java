package com.enelytics.wpps.persistance.repositories.pjmdataretrieval;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLoad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PjmLoadRepository extends JpaRepository<PjmLoad, Long>, JpaSpecificationExecutor<PjmLoad> {

}
