package com.enelytics.wpps.persistance.repositories.pjmdataretrieval;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLmp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PjmLmpRepository extends JpaRepository<PjmLmp, Long>, JpaSpecificationExecutor<PjmLmp> {

}
