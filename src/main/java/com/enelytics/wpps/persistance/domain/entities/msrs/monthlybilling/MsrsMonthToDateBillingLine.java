package com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity
public class MsrsMonthToDateBillingLine extends MsrsMonthlyBillingLine {

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "monthToDateBillingLine", orphanRemoval = true)
    private List<MsrsMonthToDateBillingLineDailyAmount> dailyAmounts = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "msrs_month_to_date_billing_summary_report_id", referencedColumnName = "id")
    private MsrsMonthToDateBillingSummaryReport monthToDateBillingSummaryReport;

}
