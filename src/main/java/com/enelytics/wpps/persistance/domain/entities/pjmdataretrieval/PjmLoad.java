package com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLoadType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(of = {"time", "area", "type"})
@ToString(exclude = "id")
@Entity
public class PjmLoad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Date time;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PjmArea area;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PjmLoadType type;

    @NotNull
    private BigDecimal loadKw;

}
