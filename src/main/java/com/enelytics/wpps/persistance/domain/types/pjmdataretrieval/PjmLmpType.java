package com.enelytics.wpps.persistance.domain.types.pjmdataretrieval;

public enum PjmLmpType {

    RT_5M("Real-Time Five Minute"),
    RT_1H("Real-Time Hourly"),
    DA_1H("Day-Ahead Hourly");

    private final String label;

    PjmLmpType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
