package com.enelytics.wpps.persistance.domain.converters;

import javax.persistence.AttributeConverter;
import java.util.Date;

public class DateMillisecondsConverter implements AttributeConverter<Date, Long> {

    @Override
    public Long convertToDatabaseColumn(Date attribute) {
        return attribute == null ? null : attribute.getTime();
    }

    @Override
    public Date convertToEntityAttribute(Long dbData) {
        return dbData == null ? null : new Date(dbData);
    }
}
