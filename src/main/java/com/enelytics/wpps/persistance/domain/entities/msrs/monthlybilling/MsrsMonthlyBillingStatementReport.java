package com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity
public class MsrsMonthlyBillingStatementReport extends MsrsMonthlyBillingReport {

    @NotNull
    private String customerCode;

    @NotNull
    private String customerId;

    @NotNull
    private LocalDateTime finalBillingStatementIssued;

    @NotNull
    private String invoiceNumber;

    @NotNull
    private LocalDateTime invoiceDueDate;

    @NotNull
    private BigDecimal previousWeeklyBillingNetTotal;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "monthlyBillingStatementReport", orphanRemoval = true)
    private List<MsrsMonthlyBillingStatementLine> monthlyBillingStatementLines = new ArrayList<>();

}
