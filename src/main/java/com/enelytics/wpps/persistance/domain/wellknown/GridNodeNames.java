package com.enelytics.wpps.persistance.domain.wellknown;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.GridNodeName;

import java.util.Objects;

public enum GridNodeNames {

    APS("APS"),
    BGE("BGE"),
    SMECO("SMECO"),
    PEPCO("PEPCO"),
    HOOVERSV("HOOVERSV");

    private final GridNodeName gridNodeName;

    GridNodeNames(String value) {
        gridNodeName = GridNodeName.of(value);
    }

    public GridNodeName gridNodeName() {
        return gridNodeName;
    }

    /**
     * Compares enum's grid node name with passed value.
     *
     * @param gridNodeName to test
     * @return {@code true} if this enum item matches passed value
     */
    public boolean is(GridNodeName gridNodeName) {
        Objects.requireNonNull(gridNodeName, "gridNodeName");
        return this.gridNodeName.equals(gridNodeName);
    }

}
