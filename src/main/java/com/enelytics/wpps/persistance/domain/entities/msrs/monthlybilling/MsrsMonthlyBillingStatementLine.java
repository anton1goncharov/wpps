package com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity
public class MsrsMonthlyBillingStatementLine extends MsrsMonthlyBillingLine {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "msrs_monthly_billing_statement_report_id", referencedColumnName = "id")
    private MsrsMonthlyBillingStatementReport monthlyBillingStatementReport;

}
