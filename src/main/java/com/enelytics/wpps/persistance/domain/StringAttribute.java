package com.enelytics.wpps.persistance.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * Parent class for all string-based attributes.
 */
public abstract class StringAttribute implements Serializable {

    protected String value;

    public StringAttribute(String value) {
        this.value = Objects.requireNonNull(value, "attribute value");
    }

    public String value() {
        return value;
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null)
            return false;
        if (obj.getClass() != this.getClass())
            return false;
        StringAttribute that = (StringAttribute) obj;

        return value.equals(that.value);
    }

    @Override
    public String toString() {
        return value;
    }
}
