package com.enelytics.wpps.persistance.domain.types.pjmdataretrieval;

public enum PjmLoadType {

    I_5M("Instantaneous 5 min"),
    F_H("Forecast hourly");

    private final String label;

    PjmLoadType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
