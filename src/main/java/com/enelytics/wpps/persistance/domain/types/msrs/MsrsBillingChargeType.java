package com.enelytics.wpps.persistance.domain.types.msrs;

public enum MsrsBillingChargeType {

    CHARGE("Charge"),
    CREDIT("Credit");

    private final String label;

    MsrsBillingChargeType(String label) {
        this.label = label;
    }

    public String label() {
        return label;
    }

}
