package com.enelytics.wpps.persistance.domain.entities.msrs;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;

@Getter
@Setter
@MappedSuperclass
@EqualsAndHashCode(of = {"customerAccount", "billingStartDate"})
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class MsrsReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String customerAccount;

    private LocalDateTime reportCreationTimestamp;

    @NotNull
    private LocalDate billingStartDate;

    @NotNull
    private LocalDate billingEndDate;

    @NotNull
    private Boolean complete = false;

    @NotNull
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] data;

    private String filename;

    public YearMonth getMonth() {
        return YearMonth.of(billingStartDate.getYear(), billingStartDate.getMonth());
    }

}
