package com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(of = {"monthToDateBillingLine", "date"})
@Entity
public class MsrsMonthToDateBillingLineDailyAmount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private LocalDate date;

    @NotNull
    private BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "msrs_month_to_date_billing_line_id", referencedColumnName = "id")
    private MsrsMonthToDateBillingLine monthToDateBillingLine;

}
