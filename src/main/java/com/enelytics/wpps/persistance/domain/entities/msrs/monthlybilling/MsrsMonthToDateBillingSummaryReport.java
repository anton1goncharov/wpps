package com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity
public class MsrsMonthToDateBillingSummaryReport extends MsrsMonthlyBillingReport {

    @NotNull
    private BigDecimal monthLastWeeklyBillingNetTotal;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "monthToDateBillingSummaryReport", orphanRemoval = true)
    private List<MsrsMonthToDateBillingLine> monthToDateBillingLines = new ArrayList<>();

}
