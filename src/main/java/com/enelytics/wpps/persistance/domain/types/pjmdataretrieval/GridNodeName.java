package com.enelytics.wpps.persistance.domain.types.pjmdataretrieval;

import com.enelytics.wpps.persistance.domain.StringAttribute;
import com.enelytics.wpps.persistance.domain.StringAttributeIntern;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class GridNodeName extends StringAttribute implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final StringAttributeIntern<GridNodeName> INTERN = new StringAttributeIntern<>();

    private GridNodeName(String value) {
        super(value);
    }

    public static GridNodeName of(String value) {
        return value == null ? null : INTERN.canonize(new GridNodeName(value));
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        out.writeUTF(value());
    }

    private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
        in.defaultReadObject();
        value = in.readUTF();
    }
}
