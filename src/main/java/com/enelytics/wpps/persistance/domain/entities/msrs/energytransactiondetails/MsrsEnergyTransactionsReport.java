package com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails;

import com.enelytics.wpps.persistance.domain.entities.msrs.MsrsReport;
import com.enelytics.wpps.persistance.domain.types.msrs.MsrsEnergyTransactionType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(of = "energyTransactionType", callSuper = true)
@Entity
public class MsrsEnergyTransactionsReport extends MsrsReport {

    @NotNull
    @Enumerated(EnumType.STRING)
    private MsrsEnergyTransactionType energyTransactionType;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "energyTransactionsReport", orphanRemoval = true)
    private List<MsrsEnergyTransaction> energyTransactions = new ArrayList<>();

}
