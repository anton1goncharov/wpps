package com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling;

import com.enelytics.wpps.persistance.domain.entities.msrs.MsrsReport;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@MappedSuperclass
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
abstract class MsrsMonthlyBillingReport extends MsrsReport {

    @NotNull
    private BigDecimal totalCharges;

    @NotNull
    private BigDecimal totalCredits;

    @NotNull
    private BigDecimal monthlyBillingNetTotal;

    @NotNull
    private BigDecimal totalDueReceivable;

}
