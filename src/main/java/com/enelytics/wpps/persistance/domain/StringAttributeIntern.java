package com.enelytics.wpps.persistance.domain;

import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.WeakHashMap;

/**
 * Attribute canonization utility class.
 * <p>
 * There should be no referenceCode that relies on difference between canonized and non canonized value.
 *
 * @param <T> pool's object type
 */
public class StringAttributeIntern<T extends StringAttribute> {

    private WeakHashMap<T, WeakReference<T>> weakRefs = new WeakHashMap<>();

    /**
     * Returns a canonical representation of the attribute.
     * @param attribute to canonize
     * @return canonical representation or exactly this attribute
     *                  if attribute pool doesn't have representation for this attribute
     */
    public synchronized T canonize(T attribute) {
        Objects.requireNonNull(attribute, "attribute");

        WeakReference<T> ref = weakRefs.get(attribute);
        T interned = ref == null ? null : ref.get();
        if (interned == null) {
            weakRefs.put(attribute, new WeakReference<>(attribute));
            interned = attribute;
        }
        return interned;
    }


}
