package com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(of = {"energyTransaction", "time"})
@Entity
public class MsrsEnergyTransactionHourlyValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Date time;

    @NotNull
    private BigDecimal kwh;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "msrs_energy_transaction_id", referencedColumnName = "id")
    private MsrsEnergyTransaction energyTransaction;

}
