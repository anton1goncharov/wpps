package com.enelytics.wpps.persistance.domain.types.msrs;

/**
 * Describes MSRS report energy transaction type:
 * <ul>
 *     <li>Day-Ahead Daily</li>
 *     <li>Real-Time Daily</li>
 * </ul>
 */
public enum MsrsEnergyTransactionType {

    DA_1D("Day-Ahead Daily"),
    RT_1D("Real-Time Daily");

    private final String label;

    MsrsEnergyTransactionType(String label) {
        this.label = label;
    }

    public String label() {
        return label;
    }
}

