package com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling;

import com.enelytics.wpps.persistance.domain.types.msrs.MsrsBillingChargeType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(of = {"orderIndex", "billingChargeType", "chargeCode"})
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class MsrsMonthlyBillingLine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Integer orderIndex;

    @NotNull
    @Enumerated(EnumType.STRING)
    private MsrsBillingChargeType billingChargeType;

    @NotNull
    private String chargeCode;

    @NotNull
    private Boolean adjustment;

    @NotNull
    private String name;

    private LocalDate sourceBillingPeriodStart;

    @NotNull
    private BigDecimal amount;

}
