package com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(of = {"orderIndex", "date", "transactionType", "transactionId"})
@Entity
public class MsrsEnergyTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Integer orderIndex;

    @NotNull
    private LocalDate date;

    @NotNull
    private String transactionType;

    private String transactionId;

    @NotNull
    private BigDecimal totalKwh;

    private String sourcePnodeName;

    private String sourcePnodeId;

    private String sinkPnodeName;

    private String sinkPnodeId;

    private String seller;

    private String buyer;

    @OneToMany(mappedBy = "energyTransaction", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MsrsEnergyTransactionHourlyValue> hourlyValues = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "msrs_energy_transactions_report_id", referencedColumnName = "id")
    private MsrsEnergyTransactionsReport energyTransactionsReport;

}
