package com.enelytics.wpps.persistance.domain.types.pjmdataretrieval;

import com.enelytics.wpps.persistance.domain.wellknown.GridNodeNames;

public enum PjmArea {

    PJM_RTO("PJM-RTO", "PJM-RTO"),
    MIDATL("Mid-Atlantic Region", "MID-ATL/APS"),
    APS("APS Zone", "APS", GridNodeNames.APS.gridNodeName()),
    BGE("BGE Zone", "BGE", GridNodeNames.BGE.gridNodeName()),
    PEPCO("PEPCO Zone", "PEPCO", GridNodeNames.PEPCO.gridNodeName()),
    WESTERN_HUB("WESTERN HUB", "WESTERN HUB");

    /**
     * Arbitrary text label for humans, feel free to change it.
     */
    private final String label;
    /**
     * Pricing node name as known by PJM Data Miner 2
     */
    private final String pricingNodeName;
    /**
     * Corresponding grid node name if any
     */
    private final GridNodeName gridNodeName;

    PjmArea(String label, String pricingNodeName) {
        this(label, pricingNodeName, null);
    }

    PjmArea(String label, String pricingNodeName, GridNodeName gridNodeName) {
        this.label = label;
        this.pricingNodeName = pricingNodeName;
        this.gridNodeName = gridNodeName;
    }

    public String getLabel() {
        return label;
    }

    public String getPricingNodeName() {
        return pricingNodeName;
    }

    public GridNodeName getGridNodeName() {
        return gridNodeName;
    }

}
