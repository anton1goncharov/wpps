package com.enelytics.wpps.persistance.domain.misc;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;

/**
 * Represents a PJM year.
 *<p>
 * Pjm is a regional transmission organization (RTO).
 *
 * @see <a href="http://www.pjm.com/">www.pjm.com</a>
 */
public class PjmYear implements Serializable, Comparable<PjmYear> {

    private static final Month YEAR_START = Month.JUNE;

    private final int year;

    private static final long serialVersionUID = 1L;

    private PjmYear(int year) {
        this.year = year;
    }

    /**
     * Create PJM organization year instance.
     *
     * @param year PJM year
     * @return immutable instance of PJM year
     */
    public static PjmYear of(int year) {
        return new PjmYear(year);
    }

    /**
     * Creates PJM year that contains the given day.
     * @param date a day within PJM year
     * @return PJM year that contains the given day
     */
    public static PjmYear from(LocalDate date) {
        PjmYear year = new PjmYear(date.getYear());
        if (year.contains(date))
            return year;
        PjmYear nextYear = year.plusYears(1);
        if (nextYear.contains(date))
            return nextYear;
        PjmYear prevYear = year.minusYears(1);
        if (prevYear.contains(date))
            return prevYear;
        throw new AssertionError("Can't create PJM year from " + date);
    }

    /**
     * Returns integer value of PJM year.
     * @return integer value of PJM year
     */
    public int getValue() {
        return year;
    }

    /**
     * Returns first months of this PJM year.
     * @return first months of this PJM year
     */
    public YearMonth firstMonth() {
        return lastMonth().minusMonths(11);
    }

    /**
     * Return first month of the PJM year
     * @return first month of the PJM year
     */
    public YearMonth lastMonth() {
        return Year.of(year).atMonth(YEAR_START.minus(1));
    }

    /**
     * Return first day of the PJM year
     * @return first day of the PJM year
     */
    public LocalDate firstDay() {
        return firstMonth().atDay(1);
    }

    /**
     * Return last day of the PJM year
     * @return last day of the PJM year
     */
    public LocalDate lastDay() {
        return lastMonth().atEndOfMonth();
    }

    /**
     * Returns PJM year with n years added. Value of <var>n</var> can be negative.
     * @return new PJM year instance
     */
    public PjmYear plusYears(int n) {
        return new PjmYear(year + n);
    }

    /**
     * Returns PJM year with n years subtracted. Value of <var>n</var> can be negative.
     * @return new PJM year instance
     */
    public PjmYear minusYears(int n) {
        return new PjmYear(year - n);
    }

    /**
     * Determines if <var>date</var> belongs to this year.
     * @return true if <var>date</var> belongs to this year, false otherwise
     */
    public boolean contains(LocalDate date) {
        return !date.isBefore(firstDay()) && !date.isAfter(lastDay());
    }

    /**
     * Determines if <var>month</var> belongs to this year.
     * @return true if <var>month</var> belongs to this year, false otherwise
     */
    public boolean contains(YearMonth month) {
        return !month.isBefore(firstMonth()) && !month.isAfter(month);
    }

    @Override
    public int compareTo(PjmYear other) {
        return Integer.compare(year, other.year);
    }

    /**
     * Checks if this year is after the specified year.
     * @param other the other year to compare to, not null
     * @return true if this year is after the specified year
     */
    public boolean isAfter(PjmYear other) {
        return year > other.year;
    }

    /**
     * Checks if this year is before the specified year.
     * @param other the other year to compare to, not null
     * @return true if this year is before the specified year
     */
    public boolean isBefore(PjmYear other) {
        return year < other.year;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PjmYear))
            return false;
        PjmYear that = (PjmYear) obj;
        return year == that.year;
    }

    @Override
    public int hashCode() {
        return year;
    }

    @Override
    public String toString() {
        return Integer.toString(year);
    }
}
