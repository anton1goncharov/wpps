package com.enelytics.wpps.persistance.domain.wellknown;

public enum PricingNodeType {

    ZONE("ZONE"),
    HUB("HUB");

    private final String type;

    PricingNodeType(String value) {
        type = value;
    }

    public String getType() {
        return type;
    }

}
