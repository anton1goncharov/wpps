package com.enelytics.wpps.persistance.specifications.pjmdata;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLoad;
import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLoad_;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLoadType;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

public class PjmLoadSpecification {

    private PjmLoadSpecification() {
        throw new IllegalStateException("PJM Load specification class");
    }

    /**
     * Find PJM Load by date range
     *
     * @return prepared specification
     */
    public static Specification<PjmLoad> between(Date start, Date end) {
        return (root, query, cb) -> cb.between(root.get(PjmLoad_.time), start, end);
    }

    /**
     * Find PJM Load by time
     *
     * @return prepared specification
     */
    public static Specification<PjmLoad> withTime(Date time) {
        return (root, query, cb) -> cb.equal(root.get(PjmLoad_.time), time);
    }

    /**
     * Find PJM Load by area
     *
     * @return prepared specification
     */
    public static Specification<PjmLoad> withArea(PjmArea area) {
        return (root, query, cb) -> cb.equal(root.get(PjmLoad_.area), area);
    }

    /**
     * Find PJM Load by type
     *
     * @return prepared specification
     */
    public static Specification<PjmLoad> withType(PjmLoadType type) {
        return (root, query, cb) -> cb.equal(root.get(PjmLoad_.type), type);
    }

}
