package com.enelytics.wpps.persistance.specifications.msrs;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingSummaryReport;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingSummaryReport_;
import org.springframework.data.jpa.domain.Specification;

import java.time.YearMonth;

public class MsrsMonthToDateBillingSummaryReportSpecification {

    private MsrsMonthToDateBillingSummaryReportSpecification() {
        throw new IllegalStateException("MSRS month-to-date billing summary report specification class");
    }

    /**
     * Find MSRS month-to-date billing summary report by year-month
     *
     * @return prepared specification
     */
    public static Specification<MsrsMonthToDateBillingSummaryReport> withMonth(YearMonth month) {
        return (root, query, cb) -> cb.and(
                cb.equal(root.get(MsrsMonthToDateBillingSummaryReport_.billingStartDate), month.atDay(1)),
                cb.equal(root.get(MsrsMonthToDateBillingSummaryReport_.billingEndDate), month.atEndOfMonth())
        );
    }

    /**
     * Find MSRS month-to-date billing summary report by complete flag
     *
     * @return prepared specification
     */
    public static Specification<MsrsMonthToDateBillingSummaryReport> complete(Boolean complete) {
        return (root, query, cb) -> cb.equal(root.get(MsrsMonthToDateBillingSummaryReport_.complete), complete);
    }

}
