package com.enelytics.wpps.persistance.specifications.msrs;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionsReport;
import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionsReport_;
import com.enelytics.wpps.persistance.domain.types.msrs.MsrsEnergyTransactionType;
import org.springframework.data.jpa.domain.Specification;

import java.time.YearMonth;

public class MsrsEnergyTransactionReportSpecification {

    private MsrsEnergyTransactionReportSpecification() {
        throw new IllegalStateException("MSRS energy transaction report specification class");
    }

    /**
     * Find MSRS energy transaction report by energy transaction type
     *
     * @return prepared specification
     */
    public static Specification<MsrsEnergyTransactionsReport> withEnergyTransactionType(MsrsEnergyTransactionType type) {
        return (root, query, cb) -> cb.equal(root.get(MsrsEnergyTransactionsReport_.energyTransactionType), type);
    }

    /**
     * Find MSRS energy transaction report by year-month
     *
     * @return prepared specification
     */
    public static Specification<MsrsEnergyTransactionsReport> withMonth(YearMonth month) {
        return (root, query, cb) -> cb.and(
                cb.equal(root.get(MsrsEnergyTransactionsReport_.billingStartDate), month.atDay(1)),
                cb.equal(root.get(MsrsEnergyTransactionsReport_.billingEndDate), month.atEndOfMonth())
        );
    }

    /**
     * Find MSRS energy transaction report by by complete flag
     *
     * @return prepared specification
     */
    public static Specification<MsrsEnergyTransactionsReport> complete(Boolean complete) {
        return (root, query, cb) -> cb.equal(root.get(MsrsEnergyTransactionsReport_.complete), complete);
    }

}
