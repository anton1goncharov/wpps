package com.enelytics.wpps.persistance.specifications.msrs;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementReport;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementReport_;
import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionsReport;
import org.springframework.data.jpa.domain.Specification;

import java.time.YearMonth;

public class MsrsMonthlyBillingStatementReportSpecification {

    private MsrsMonthlyBillingStatementReportSpecification() {
        throw new IllegalStateException("MSRS monthly billing statement report specification class");
    }

    /**
     * Find MSRS monthly billing statement report by year-month
     *
     * @return prepared specification
     */
    public static Specification<MsrsMonthlyBillingStatementReport> withMonth(YearMonth month) {
        return (root, query, cb) -> cb.and(
                cb.equal(root.get(MsrsMonthlyBillingStatementReport_.billingStartDate), month.atDay(1)),
                cb.equal(root.get(MsrsMonthlyBillingStatementReport_.billingEndDate), month.atEndOfMonth())
        );
    }

    /**
     * Find MSRS monthly billing statement report by complete flag
     *
     * @return prepared specification
     */
    public static Specification<MsrsEnergyTransactionsReport> complete(Boolean complete) {
        return (root, query, cb) -> cb.equal(root.get(MsrsMonthlyBillingStatementReport_.complete), complete);
    }

}
