package com.enelytics.wpps.persistance.specifications.pjmdata;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLmp;
import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLmp_;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLmpType;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

public class PjmLmpSpecification {

    private PjmLmpSpecification() {
        throw new IllegalStateException("PJM LMP specification class");
    }

    /**
     * Find PJM LMP by date range
     *
     * @return prepared specification
     */
    public static Specification<PjmLmp> between(Date start, Date end) {
        return (root, query, cb) -> cb.between(root.get(PjmLmp_.time), start, end);
    }

    /**
     * Find PJM LMP by time
     *
     * @return prepared specification
     */
    public static Specification<PjmLmp> withTime(Date time) {
        return (root, query, cb) -> cb.equal(root.get(PjmLmp_.time), time);
    }

    /**
     * Find PJM LMP by area
     *
     * @return prepared specification
     */
    public static Specification<PjmLmp> withArea(PjmArea area) {
        return (root, query, cb) -> cb.equal(root.get(PjmLmp_.area), area);
    }

    /**
     * Find PJM LMP by type
     *
     * @return prepared specification
     */
    public static Specification<PjmLmp> withType(PjmLmpType type) {
        return (root, query, cb) -> cb.equal(root.get(PjmLmp_.type), type);
    }

}
