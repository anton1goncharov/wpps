package com.enelytics.wpps;

import com.enelytics.wpps.config.WppsCacheConfig;
import com.enelytics.wpps.config.SchedulingConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration
@Configuration
@Import({
        WppsCacheConfig.class,
        SchedulingConfig.class
})
public class WppsApplicationConfig {

}
