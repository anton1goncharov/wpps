package com.enelytics.wpps.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

/**
 * Convenience methods to work with {@link BigDecimal} values in a null-safe way.
 */
public class BigDecimalUtil {

    // Not a magic constant: 18 is slightly better than precision of double
    private static final MathContext DEFAULT_ROUNDING = new MathContext(18);

    private BigDecimalUtil(){
        throw new IllegalStateException("Utility class");
    }

    /**
     * Returns {@code true} if value is null or zero.
     * @param value to check
     */
    public static boolean isNullOrZero(BigDecimal value) {
        return value == null || value.compareTo(ZERO) == 0;
    }

    /**
     * Divides two values.
     * @return division result or null is {@code divisor} is zero.
     */
    public static BigDecimal divide(BigDecimal dividend, BigDecimal divisor){
        if (divisor == null || divisor.compareTo(ZERO) == 0)
            return null;
        return nullToZero(dividend).divide(nullToZero(divisor), DEFAULT_ROUNDING);
    }

    /**
     * Adds {@code values}.
     * @param values - values to add.
     * @return values sum
     */
    public static BigDecimal sum(List<BigDecimal> values) {
        return values.stream()
                .map(BigDecimalUtil::nullToZero)
                .reduce(ZERO, BigDecimal::add);
    }

    /**
     * Adds {@code values}.
     * @param values - values to add.
     * @return values sum
     */
    public static BigDecimal add(BigDecimal... values) {
        return sum(Arrays.asList(values));
    }

    /**
     * Subtracts second value from the first one. Null-input is treated a zero.
     */
    public static BigDecimal subtract(BigDecimal a, BigDecimal b) {
        return nullToZero(a).subtract(nullToZero(b));
    }

    /**
     * Multiplies several values. Null-input is treated a zero.
     */
    public static BigDecimal mult(BigDecimal... values) {
        return Stream.of(values)
                .map(BigDecimalUtil::nullToZero)
                .reduce(ONE, BigDecimal::multiply);
    }

    private static BigDecimal nullToZero(BigDecimal n) {
        return n == null ? ZERO : n;
    }

    /**
     * Rounds to long using passed {@link RoundingMode} mode.
     * If BigDecimal holds value that cannot be represented by long, then exception is thrown.
     * <p>
     *     This method and {@link BigDecimal#longValue()} produce different results. {@link BigDecimal#longValue()}
     *     simply throws fractional part without any rounding.
     *
     * @param n value to round
     * @param roundingMode round to integer value using this mode
     * @return rounded long value
     *
     * @throws ArithmeticException if passed value is too big to be represented by long
     */
    public static long roundExactly(BigDecimal n, RoundingMode roundingMode) {
        return n.setScale(0, roundingMode).toBigIntegerExact().longValue();
    }

    /**
     * Null-safe {@link BigDecimal#max(BigDecimal)} version. Returns maximum among all values. Nulls are ignored.
     * If there are only nulls passed, then null returned.
     *
     * @param values to find maximum in
     * @return max value or null if all values are nulls
     */
    public static BigDecimal max(BigDecimal ... values) {
        BigDecimal max = null;
        for (BigDecimal value : values)
            if (value != null)
                max = max == null ? value : max.max(value);
        return null;
    }

    public static BigDecimal round(BigDecimal n) {
        return n.setScale(18, RoundingMode.HALF_EVEN);
    }

}
