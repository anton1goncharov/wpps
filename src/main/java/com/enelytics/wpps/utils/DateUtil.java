package com.enelytics.wpps.utils;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * Keeps util functions for dates processing
 */
public class DateUtil {

    private DateUtil(){
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return {@code true} if {@code checkDate} is between {@code from} and {@code to}.
     * @param checkDate - date to check
     * @param from - date range start
     * @param to = date range end
     * @return {@code true} if {@code checkDate} is between {@code from} and {@code to}.
     */
    public static boolean isBetween(LocalDate checkDate,
                                    LocalDate from,
                                    LocalDate to) {
        return (checkDate.isEqual(from) || checkDate.isAfter(from)) &&
               (checkDate.isEqual(to) || checkDate.isBefore(to));
    }

    /**
     * Return {@code true} if {@code checkDate} is between {@code from} and {@code to}.
     * @param checkDate - date to check
     * @param from - date range start
     * @param to = date range end
     * @return {@code true} if {@code checkDate} is between {@code from} and {@code to}.
     */
    public static boolean isBetween(LocalDateTime checkDate,
                                    LocalDateTime from,
                                    LocalDateTime to) {
        return (checkDate.isEqual(from) || checkDate.isAfter(from)) &&
                (checkDate.isEqual(to) || checkDate.isBefore(to));
    }

    /**
     * Get milliseconds from {@link LocalDateTime} instance.
     * @param localDateTime to get millis for
     * @return milliseconds
     */
    public static long getMilliseconds(LocalDateTime localDateTime) {
        final ZoneId zoneId = ZoneId.systemDefault();
        final ZonedDateTime zdt = localDateTime.atZone(zoneId);

        return zdt.toInstant().toEpochMilli();
    }

    /**
     * Converts given time to Date at the System timezone.
     * @param time time to convert
     * @return Date representation of the passed time in System's timezone
     */
    public static Date toDate(LocalDateTime time) {
        return toDate(time, ZoneId.systemDefault());
    }

    /**
     * Converts given time to Date at the zoneId timezone.
     * @param time time to convert
     * @return Date representation of the passed time in zoneId timezone
     */
    public static Date toDate(LocalDateTime time, ZoneId zoneId) {
        return Date.from(time.atZone(zoneId).toInstant());
    }

    /**
     * Converts {@link Date} to {@link LocalDate}.
     * @param date to convert
     * @return {@link LocalDate}
     */
    public static LocalDate toLocalDate(Date date) {
        return toLocalDateTime(date).toLocalDate();
    }

    /**
     * Converts {@link Date} to {@link LocalDateTime}.
     * @param date to convert
     * @return {@link LocalDateTime}
     */
    public static LocalDateTime toLocalDateTime(Date date) {
        final long millis = date.getTime();
        final Instant instant = Instant.ofEpochMilli(millis);
        final ZoneId zoneId = TimeZone.getDefault().toZoneId();

        return LocalDateTime.ofInstant(instant, zoneId);
    }

    /**
     * Generates a number of time points starting from {@code dateFrom}.
     * @param count - amount of {@code unit}s to generate.
     * @param unit   - date/time unit.
     * @param dateFrom - start date.
     */
    public static List<LocalDate> getTimePoints(LocalDate dateFrom, long count, ChronoUnit unit) {
        return LongStream.range(0, count)
                .mapToObj(index -> dateFrom.plus(index, unit))
                .collect(Collectors.toList());
    }

    /**
     * Creates date from start of the day (<var>day</var> rounded to 00:00) and hours shift.
     * Most of the time this shift is equal to the time of day except daylight savings.
     * <p>
     *
     * <p>
     * Example for "US/Eastern" timezone:
     * <pre>
     *     final Date d = new Date(1509854400000L); // 2017-11-05 In US/Eastern timezone is the day when time shifts back
     *     final TimeZone tz = TimeZone.getTimeZone("US/Eastern");
     *     composeDate(d, 0, tz); // "Sun Nov 05 00:00:00 EDT 2017" in US/Eastern timezone
     *     composeDate(d, 1, tz); // "Sun Nov 05 01:00:00 EDT 2017" in US/Eastern timezone
     *     composeDate(d, 2, tz); // "Sun Nov 05 01:00:00 EST 2017" in US/Eastern timezone
     *     composeDate(d, 3, tz); // "Sun Nov 05 02:00:00 EST 2017" in US/Eastern timezone
     * </pre>
     *
     * @param day date that designates the day; hours, minutes and lesser parts of time ignored
     * @param hoursShift shift this amount of hours from the start of the <var>day</var> (00:00)
     * @param timeZone use this timezone in calculations
     * @return shifted time
     */
    public static Date composeDate(Date day, int hoursShift, TimeZone timeZone) {
        ZoneId zoneId = timeZone.toZoneId();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(day.toInstant(), zoneId);
        LocalDate localDate = localDateTime.toLocalDate();
        Instant startOfDay = localDate.atStartOfDay().atZone(zoneId).toInstant();
        Instant wantedHour = startOfDay.plus(hoursShift, ChronoUnit.HOURS);

        return Date.from(wantedHour);
    }
}
