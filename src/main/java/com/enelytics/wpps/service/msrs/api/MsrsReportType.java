package com.enelytics.wpps.service.msrs.api;

public enum MsrsReportType {

    MBS("Monthly Billing Statement", "MonthlyBillingStatement-CSVandXML"),
    MTDBS("Month-to-Date Billing Summary", "Month-to-DateBill-CSVandXML"),
    DA_DET("DA Daily Energy Transactions", "DADailyEnergyTransactions"),
    RT_DET("RT Daily Energy Transactions", "RTDailyEnergyTransactions");

    private final String label;
    private final String reportQueryParam;

    MsrsReportType(String label, String reportQueryParam) {
        this.label = label;
        this.reportQueryParam = reportQueryParam;
    }

    public String getLabel() {
        return label;
    }

    public String getReportQueryParam() {
        return reportQueryParam;
    }

}
