package com.enelytics.wpps.service.msrs.parser.util;

import com.enelytics.wpps.service.msrs.parser.exception.MsrsReportParsingException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;

public class CsvParserUtils {

    private CsvParserUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static String[][] readCsvToMatrix(byte[] csv) throws MsrsReportParsingException {
        try (CSVParser csvParser = CSVParser.parse(new String(csv), CSVFormat.DEFAULT)) {
            List<CSVRecord> records = csvParser.getRecords();
            int maxLength = 0;

            for (CSVRecord record : records) {
                maxLength = Math.max(maxLength, record.size());
            }

            String[][] data = new String[records.size()][maxLength];

            for (int r = 0; r < records.size(); r++) {
                CSVRecord row = records.get(r);
                maxLength = Math.max(maxLength, row.size());
                for (int c = 0; c < row.size(); c++) {
                    data[r][c] = row.get(c).trim();
                }
            }

            return data;
        } catch (IOException e) {
            throw new MsrsReportParsingException("Bad csv", e);
        }
    }

    public static void validateCell(String[][] data, String expected, Cell address) throws MsrsReportParsingException {
        validateCell(data, expected, address.getRow(), address.getColumn());
    }

    public static void validateCell(String[][] data, String expected, int row, int column) throws MsrsReportParsingException {
        if (row < 0 || column < 0 || row > data.length || column > data[row].length
                || !Objects.equals(expected, data[row][column])) {
            String message = format("Not valid row=%d, column=%d, expected value=%s", row + 1, column + 1, expected);
            throw new MsrsReportParsingException(message);
        }
    }

}
