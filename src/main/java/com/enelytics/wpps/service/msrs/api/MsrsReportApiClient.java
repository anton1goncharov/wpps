package com.enelytics.wpps.service.msrs.api;

import java.io.IOException;

public interface MsrsReportApiClient {

    MsrsReportResponse get(MsrsReportRequest request) throws IOException;

}
