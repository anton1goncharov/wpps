package com.enelytics.wpps.service.msrs.parser;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransaction;
import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionHourlyValue;
import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionsReport;
import com.enelytics.wpps.persistance.domain.types.msrs.MsrsEnergyTransactionType;
import com.enelytics.wpps.service.msrs.api.MsrsReportResponse;
import com.enelytics.wpps.service.msrs.parser.exception.MsrsReportParsingException;
import com.enelytics.wpps.service.msrs.parser.util.Cell;
import com.enelytics.wpps.service.msrs.parser.util.CsvParserUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.enelytics.wpps.service.msrs.parser.EnergyTransactionsCsvParser.EnergyTransactionsCsvConfig.*;
import static com.enelytics.wpps.service.msrs.parser.util.Cell.cell;
import static com.enelytics.wpps.service.msrs.parser.util.CsvParserUtils.validateCell;
import static com.enelytics.wpps.utils.BigDecimalUtil.round;

/**
 * Parser for Energy Transactions csv. Currently supports Day-Ahead and Real-Time.
 */
public class EnergyTransactionsCsvParser {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    private static final DateTimeFormatter REPORT_CREATION_TIMESTAMP_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm:ss a");
    private static final Pattern NORMAL_HOUR_PATTERN = Pattern.compile("EPT HE (\\d\\d)");
    private static final Pattern LATER_OFFSET_OVERLAP_HOUR_PATTERN = Pattern.compile("EPT HE ?(\\d\\d)[*X]");
    private static final ZoneId TIME_ZONE = ZoneId.of("US/Eastern");


    public MsrsEnergyTransactionsReport parse(MsrsReportResponse response) throws MsrsReportParsingException {
        byte[] reportData = response.getData();
        String[][] data = CsvParserUtils.readCsvToMatrix(reportData);
        EnergyTransactionsCsvConfig config = prepareCsvConfig(data);

        MsrsEnergyTransactionsReport report = parse(data, config);
        report.setData(reportData);
        report.setFilename(response.getFilename());

        return report;
    }

    private EnergyTransactionsCsvConfig prepareCsvConfig(String[][] data) throws MsrsReportParsingException {
        EnergyTransactionsCsvConfig config = new EnergyTransactionsCsvConfig();
        int headerRow = config.getHeaderRow();
        int firstHourColumn = config.getFirstHourColumn();

        validateCells(data, config);

        int lastHourColumn = findHeaderColumn(data, headerRow, EnergyTransactionsCsvConfig.LAST_HOUR_HEADER);
        int totalMWhColumn = findHeaderColumn(data, headerRow, EnergyTransactionsCsvConfig.TOTAL_MWH_HEADER);

        config.setLastHourColumn(lastHourColumn);
        config.setTotalMwhColumn(totalMWhColumn);
        config.setSourcePnodeNameColumn(findHeaderColumn(data, headerRow, EnergyTransactionsCsvConfig.SOURCE_PNODE_NAME_HEADER));
        config.setSourcePnodeIdColumn(findHeaderColumn(data, headerRow, EnergyTransactionsCsvConfig.SOURCE_PNODE_ID_HEADER));
        config.setSinkPnodeNameColumn(findHeaderColumn(data, headerRow, EnergyTransactionsCsvConfig.SINK_PNODE_NAME_HEADER));
        config.setSinkPnodeIdColumn(findHeaderColumn(data, headerRow, SINK_PNODE_ID_HEADER));
        config.setSellerColumn(findHeaderColumn(data, headerRow, SELLER_HEADER));
        config.setBuyerColumn(findHeaderColumn(data, headerRow, BUYER_HEADER));
        findLastDataRow(data, config);

        if (totalMWhColumn - 1 != lastHourColumn)
            throw new MsrsReportParsingException("'Total MWh' expected to follow 'EPT HE 24'");
        if (lastHourColumn - firstHourColumn < 23 || lastHourColumn - firstHourColumn > 25)
            throw new MsrsReportParsingException("Expected 23, 24, or 25 hours");

        prepareColumnToStartHour(data, config);

        return config;
    }

    private void validateCells(String[][] data, EnergyTransactionsCsvConfig config) throws MsrsReportParsingException {
        validateCell(data, CUSTOMER_ACCOUNT_HEADER, config.getCustomerAccountLabel());
        validateCell(data, REPORT_CREATION_TIMESTAMP_HEADER, config.getReportCreationTimestampLabel());
        validateCell(data, START_DATE_HEADER, config.getStartDateLabel());
        validateCell(data, END_DATE_HEADER, config.getEndDateLabel());
        validateCell(data, FIRST_COLUMN_CODE, config.getColumnCodeRow(), config.getCustomerIdColumn());
        validateCell(data, CUSTOMER_ID_HEADER, config.getHeaderRow(), config.getCustomerIdColumn());
        validateCell(data, CUSTOMER_CODE_HEADER, config.getHeaderRow(), config.getCustomerCodeColumn());
        validateCell(data, DATE_HEADER, config.getHeaderRow(), config.getDateColumn());
        validateCell(data, TRANSACTION_TYPE_HEADER, config.getHeaderRow(), config.getTransactionTypeColumn());
        validateCell(data, TRANSACTION_ID_HEADER, config.getHeaderRow(), config.getTransactionIdColumn());
        validateCell(data, FIRST_HOUR_HEADER, config.getHeaderRow(), config.getFirstHourColumn());
        validateCell(data, SECOND_HOUR_HEADER, config.getHeaderRow(), config.getFirstHourColumn() + 1);
    }

    private int findHeaderColumn(String[][] data, int headerRow, String headerText) throws MsrsReportParsingException {
        Objects.requireNonNull(headerText, "headerText");

        for (int column = 0; column < data[headerRow].length; column++) {
            if (headerText.equals(data[headerRow][column])) {
                return column;
            }
        }

        throw new MsrsReportParsingException("'" + headerText + "' column not found");
    }

    private void findLastDataRow(String[][] data, EnergyTransactionsCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getMainDataFirstRow(); row < data.length; row++) {
            String cell = data[row][0];

            if ("End of Report".equals(cell)) {
                config.setMainDataLastRow(row - 1);
                break;
            }
        }

        if (config.getMainDataFirstRow() == config.getMainDataLastRow())
            throw new MsrsReportParsingException("No energy transactions data found");
    }

    private void prepareColumnToStartHour(String[][] data, EnergyTransactionsCsvConfig config) throws MsrsReportParsingException {
        for (int column = config.getFirstHourColumn(); column <= config.getLastHourColumn(); column++) {
            String cellValue = getNotBlankCellValue(data, config.getHeaderRow(), column);
            Matcher normalMatcher = NORMAL_HOUR_PATTERN.matcher(cellValue);
            final int endHour;

            if (normalMatcher.matches()) {
                endHour = Integer.parseInt(normalMatcher.group(1));
            } else {
                Matcher overlapMatcher = LATER_OFFSET_OVERLAP_HOUR_PATTERN.matcher(cellValue);
                if (!overlapMatcher.matches() || config.getColumnWithLaterOverlappingHour() != EnergyTransactionsCsvConfig.DETECT)
                    throw new MsrsReportParsingException("Bad hour header: " + cellValue);
                config.setColumnWithLaterOverlappingHour(column);
                endHour = Integer.parseInt(overlapMatcher.group(1));
            }

            final int startHour = endHour - 1;

            if (startHour < 0 || startHour > 23)
                throw new MsrsReportParsingException("Bad hour header: " + cellValue);

            config.getColumnToStartHour().put(column, LocalTime.of(startHour, 0));
        }
    }

    private MsrsEnergyTransactionsReport parse(String[][] data, EnergyTransactionsCsvConfig config) throws MsrsReportParsingException {
        MsrsEnergyTransactionsReport energyTransactionsReport = new MsrsEnergyTransactionsReport();

        MsrsEnergyTransactionType type = detectDocumentType(data, config.getDocId());
        energyTransactionsReport.setEnergyTransactionType(type);

        Cell customerName = config.getCustomerAccount();
        energyTransactionsReport.setCustomerAccount(data[customerName.getRow()][customerName.getColumn()]);

        energyTransactionsReport.setBillingStartDate(LocalDate.parse(getNotBlankCellValue(data, config.getStartDate()), DATE_FORMATTER));
        energyTransactionsReport.setBillingEndDate(LocalDate.parse(getNotBlankCellValue(data, config.getEndDate()), DATE_FORMATTER));
        energyTransactionsReport.setReportCreationTimestamp(LocalDateTime.parse(getNotBlankCellValue(data, config.getReportCreationTimestamp()), REPORT_CREATION_TIMESTAMP_FORMATTER));

        for (int row = config.getMainDataFirstRow(); row <= config.getMainDataLastRow(); row++) {
            BigDecimal totalMwhValue = new BigDecimal(getNotBlankCellValue(data, row, config.getTotalMwhColumn()));
            BigDecimal totalKwh = round(totalMwhValue.movePointRight(3)); // multiply by 1000 for MWh => kWh

            MsrsEnergyTransaction transaction = new MsrsEnergyTransaction();
            transaction.setOrderIndex(row);
            transaction.setDate(LocalDate.parse(getNotBlankCellValue(data, row, config.getDateColumn()), DATE_FORMATTER));
            transaction.setTransactionType(getNotBlankCellValue(data, row, config.getTransactionTypeColumn()));
            transaction.setTransactionId(getNullIfBlankCellValue(data, row, config.getTransactionIdColumn()));
            transaction.setTotalKwh(totalKwh);
            transaction.setSourcePnodeId(getNullIfBlankCellValue(data, row, config.getSinkPnodeIdColumn()));
            transaction.setSourcePnodeName(getNullIfBlankCellValue(data, row, config.getSourcePnodeNameColumn()));
            transaction.setSinkPnodeId(getNullIfBlankCellValue(data, row, config.getSinkPnodeIdColumn()));
            transaction.setSinkPnodeName(getNullIfBlankCellValue(data, row, config.getSinkPnodeNameColumn()));
            transaction.setSeller(getNullIfBlankCellValue(data, row, config.getSellerColumn()));
            transaction.setBuyer(getNullIfBlankCellValue(data, row, config.getBuyerColumn()));
            transaction.setEnergyTransactionsReport(energyTransactionsReport);
            energyTransactionsReport.getEnergyTransactions().add(transaction);

            for (int column = config.getFirstHourColumn(); column <= config.getLastHourColumn(); column++) {
                MsrsEnergyTransactionHourlyValue hourlyValue = new MsrsEnergyTransactionHourlyValue();

                Date time = resolveTime(config, transaction.getDate(), column);
                if (time == null) // if invalid overlapping hour
                    continue;

                String mWhValue = getNotBlankCellValue(data, row, column);
                BigDecimal mWh = new BigDecimal(mWhValue);
                BigDecimal kWh = round(mWh.movePointRight(3)); // multiply by 1000 to convert MWh to kWh

                hourlyValue.setTime(time);
                hourlyValue.setKwh(kWh);

                hourlyValue.setEnergyTransaction(transaction);
                transaction.getHourlyValues().add(hourlyValue);
            }
        }

        return energyTransactionsReport;
    }

    private MsrsEnergyTransactionType detectDocumentType(String[][] data, Cell docId) throws MsrsReportParsingException {
        String signature = getNotBlankCellValue(data, docId);

        if ("Day-Ahead Daily Energy Transactions".equals(signature))
            return MsrsEnergyTransactionType.DA_1D;
        if ("Real-Time Daily Energy Transactions".equals(signature))
            return MsrsEnergyTransactionType.RT_1D;
        throw new MsrsReportParsingException("Unknown document type signature: " + signature);
    }

    private Date resolveTime(EnergyTransactionsCsvConfig config, LocalDate day, int column) {
        LocalTime localTime = config.getColumnToStartHour().get(column);

        if (localTime == null)
            throw new IllegalArgumentException("Failed to resolve time from column: " + column);

        ZonedDateTime time = ZonedDateTime.of(day, localTime, TIME_ZONE);

        if (column == config.getColumnWithLaterOverlappingHour()) {
            ZonedDateTime later = time.withLaterOffsetAtOverlap();
            if (later.equals(time))
                return null;
            time = later;
        }

        return Date.from(time.toInstant());
    }

    private String getNotBlankCellValue(String[][] data, int row, int column) throws MsrsReportParsingException {
        return getNotBlankCellValue(data, cell(row, column));
    }

    private String getNotBlankCellValue(String[][] data, Cell cell) throws MsrsReportParsingException {
        String cellValue = getCellValue(data, cell.getRow(), cell.getColumn());

        if (StringUtils.isBlank(cellValue))
            throw new MsrsReportParsingException("Blank cell " + cell);

        return cellValue;
    }

    private String getNullIfBlankCellValue(String[][] data, int row, int column) {
        String cellValue = getCellValue(data, row, column);

        return StringUtils.isBlank(cellValue) ? null : cellValue;
    }

    private String getCellValue(String[][] data, int row, int column) {
        return data.length < row || data[row].length < column ? null : data[row][column];
    }

    @Getter
    @Setter
    static class EnergyTransactionsCsvConfig {

        static final String CUSTOMER_ACCOUNT_HEADER = "Customer Account:";
        static final String REPORT_CREATION_TIMESTAMP_HEADER = "Report Creation Timestamp (EPT):";
        static final String START_DATE_HEADER = "Start Date:";
        static final String END_DATE_HEADER = "End Date:";
        static final String FIRST_COLUMN_CODE = "4000.01";
        static final String CUSTOMER_ID_HEADER = "Customer ID";
        static final String CUSTOMER_CODE_HEADER = "Customer Code";
        static final String DATE_HEADER = "Date";
        static final String TRANSACTION_TYPE_HEADER = "Transaction Type";
        static final String TRANSACTION_ID_HEADER = "Transaction ID";
        static final String FIRST_HOUR_HEADER = "EPT HE 01";
        static final String SECOND_HOUR_HEADER = "EPT HE 02";
        static final String LAST_HOUR_HEADER = "EPT HE 24";
        static final String TOTAL_MWH_HEADER = "Total MWh";
        static final String SOURCE_PNODE_NAME_HEADER = "Source PNODE Name";
        static final String SOURCE_PNODE_ID_HEADER = "Source PNODE ID";
        static final String SINK_PNODE_NAME_HEADER = "Sink PNODE Name";
        static final String SINK_PNODE_ID_HEADER = "Sink PNODE ID";
        static final String SELLER_HEADER = "Seller";
        static final String BUYER_HEADER = "Buyer";

        private static final int DETECT = -1;

        private Cell docId = cell(0, 0);
        private Cell customerAccountLabel = cell(1, 0);
        private Cell customerAccount = cell(1, 1);
        private Cell reportCreationTimestampLabel = cell(1, 2);
        private Cell reportCreationTimestamp = cell(1, 3);
        private Cell startDateLabel = cell(2, 0);
        private Cell startDate = cell(2, 1);
        private Cell endDateLabel = cell(2, 2);
        private Cell endDate = cell(2, 3);

        // Main Data
        private int columnCodeRow = 3;
        private int headerRow = 4;
        private int mainDataFirstRow = 5;
        private int mainDataLastRow = DETECT;

        private int customerIdColumn = 0;
        private int customerCodeColumn = 1;
        private int dateColumn = 2;
        private int transactionTypeColumn = 3;
        private int transactionIdColumn = 4;
        private int firstHourColumn = 7;
        private int lastHourColumn = DETECT;
        private int totalMwhColumn = DETECT;
        private int sourcePnodeNameColumn = DETECT;
        private int sourcePnodeIdColumn = DETECT;
        private int sinkPnodeNameColumn = DETECT;
        private int sinkPnodeIdColumn = DETECT;
        private int sellerColumn = DETECT;
        private int buyerColumn = DETECT;
        private int columnWithLaterOverlappingHour = DETECT;
        private Map<Integer, LocalTime> columnToStartHour = new HashMap<>();

    }

}
