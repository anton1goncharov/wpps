package com.enelytics.wpps.service.msrs.parser;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingLine;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingLineDailyAmount;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingSummaryReport;
import com.enelytics.wpps.persistance.domain.types.msrs.MsrsBillingChargeType;
import com.enelytics.wpps.service.msrs.api.MsrsReportResponse;
import com.enelytics.wpps.service.msrs.parser.exception.MsrsReportParsingException;
import com.enelytics.wpps.service.msrs.parser.util.Cell;
import com.enelytics.wpps.service.msrs.parser.util.CsvParserUtils;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static com.enelytics.wpps.service.msrs.parser.util.Cell.cell;
import static com.enelytics.wpps.service.msrs.parser.util.CsvParserUtils.validateCell;
import static com.enelytics.wpps.service.msrs.parser.MonthToDateBillingSummaryCsvParser.MonthToDateBillingSummaryCsvConfig.*;
import static com.enelytics.wpps.utils.BigDecimalUtil.round;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class MonthToDateBillingSummaryCsvParser {

    private static final DateTimeFormatter REPORT_CREATION_TIMESTAMP_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm:ss a");
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    public MsrsMonthToDateBillingSummaryReport parse(MsrsReportResponse response) throws MsrsReportParsingException {
        byte[] reportData = response.getData();
        String[][] data = CsvParserUtils.readCsvToMatrix(reportData);
        MonthToDateBillingSummaryCsvConfig config = prepareCsvConfig(data);

        MsrsMonthToDateBillingSummaryReport report = parse(data, config);
        report.setData(reportData);
        report.setFilename(response.getFilename());

        return report;
    }

    private MonthToDateBillingSummaryCsvConfig prepareCsvConfig(String[][] data) throws MsrsReportParsingException {
        MonthToDateBillingSummaryCsvConfig config = new MonthToDateBillingSummaryCsvConfig();

        validateCells(data, config);

        findChargesDataStartRow(data, config);
        findChargesDataEndRow(data, config);
        findCreditsHeaderRow(data, config);
        findCreditsDataStartRow(data, config);
        findCreditsDataEndRow(data, config);
        findMonthlyBillingNetTotalRow(data, config);
        findLastWeeklyBillingNetTotalRow(data, config);
        findTotalDueReceivableRow(data, config);
        findDatesEndColumn(data, config);

        return config;
    }

    private void validateCells(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        validateCell(data, CUSTOMER_ACCOUNT_HEADER, config.getCustomerAccountLabel());
        validateCell(data, REPORT_CREATION_TIMESTAMP_HEADER, config.getReportCreationTimestampLabel());
        validateCell(data, BILLING_PERIOD_START_HEADER, config.getBillingStartDateLabel());
        validateCell(data, BILLING_PERIOD_END_HEADER, config.getBillingEndDateLabel());
        validateCell(data, CHARGES_HEADER, config.getChargesHeaderRow(), config.getChargeCodeColumn());
        validateCell(data, ADJ_HEADER, config.getChargesHeaderRow(), config.getAdjColumn());
        validateCell(data, BILLING_LINE_ITEM_NAME_HEADER, config.getChargesHeaderRow(), config.getBillingLineItemNameColumn());
        validateCell(data, SOURCE_BILLING_PERIOD_START_HEADER, config.getChargesHeaderRow(), config.getSourceBillingPeriodStartColumn());
        validateCell(data, BILLING_PERIOD_TO_DATE_AMOUNT_HEADER, config.getChargesHeaderRow(), config.getAmountColumn());
    }

    private void findChargesDataStartRow(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        int row = config.getChargesHeaderRow() + 1;

        if (row >= data.length || isBlank(data[row][config.getChargeCodeColumn()]))
            throw new MsrsReportParsingException("No charges data found");

        config.setChargesDataStartRow(row);
    }

    private void findChargesDataEndRow(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getChargesDataStartRow(); row < data.length; row++) {
            String cell = data[row][config.getChargeCodeColumn()];

            if (TOTAL_CHARGES_HEADER.equals(cell)) {
                config.setChargesDataEndRow(row - 1);
                config.setTotalChargesRow(row);
                break;
            }
        }

        if (config.getChargesDataEndRow() < 0)
            throw new MsrsReportParsingException("Charges data end row not found");
    }

    private void findCreditsHeaderRow(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getChargesDataEndRow() + 1; row < data.length; row++) {
            String cell = data[row][config.getChargeCodeColumn()];

            if (CREDITS_HEADER.equals(cell)) {
                config.setCreditsHeaderRow(row);
                break;
            }
        }

        if (config.getCreditsHeaderRow() < 0)
            throw new MsrsReportParsingException("Credits header row not found");
    }

    private void findCreditsDataStartRow(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        int row = config.getCreditsHeaderRow() + 1;

        if (row >= data.length || isBlank(data[row][config.getChargeCodeColumn()]))
            throw new MsrsReportParsingException("No credits data found");

        config.setCreditsDataStartRow(row);
    }

    private void findCreditsDataEndRow(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getCreditsDataStartRow(); row < data.length; row++) {
            String cell = data[row][config.getChargeCodeColumn()];

            if (TOTAL_CREDITS_HEADER.equals(cell)) {
                config.setCreditsDataEndRow(row - 1);
                config.setTotalCreditsRow(row);
                break;
            }
        }

        if (config.getCreditsDataEndRow() < 0)
            throw new MsrsReportParsingException("Credits data end row not found");
    }

    private void findMonthlyBillingNetTotalRow(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getTotalCreditsRow(); row < data.length; row++) {
            String cell = data[row][config.getChargeCodeColumn()];

            if (MONTHLY_BILLING_NET_TOTAL_HEADER.equals(cell)) {
                config.setMonthlyBillingNetTotalRow(row);
                break;
            }
        }

        if (config.getMonthlyBillingNetTotalRow() < 0)
            throw new MsrsReportParsingException("Monthly Billing Net Total row not found");
    }

    private void findLastWeeklyBillingNetTotalRow(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getTotalCreditsRow(); row < data.length; row++) {
            String cell = data[row][config.getChargeCodeColumn()];

            if (MONTH_LAST_WEEKLY_BILLING_NET_TOTAL_HEADER.equals(cell)) {
                config.setMonthLastWeeklyBillingNetTotalRow(row);
                break;
            }
        }

        if (config.getMonthLastWeeklyBillingNetTotalRow() < 0)
            throw new MsrsReportParsingException("Net Total for Month's Last Weekly Billing Period row not found");
    }

    private void findTotalDueReceivableRow(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getTotalCreditsRow(); row < data.length; row++) {
            String cell = data[row][config.getChargeCodeColumn()];

            if (TOTAL_DUE_RECEIVABLE_HEADER.equals(cell)) {
                config.setTotalDueReceivableRow(row);
                break;
            }
        }

        if (config.getTotalDueReceivableRow() < 0)
            throw new MsrsReportParsingException("Total Due / Receivable row not found");
    }

    private void findDatesEndColumn(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        for (int column = config.getDatesStartColumn(); column < data[config.getChargesHeaderRow()].length; column++) {
            String[] rowData = data[config.getChargesHeaderRow()];

            if (isBlank(rowData[column]))
                break;

            config.setDatesEndColumn(column);
        }

        if (config.getDatesEndColumn() < 0)
            throw new MsrsReportParsingException("Dates end column not found");
    }

    private MsrsMonthToDateBillingSummaryReport parse(String[][] data, MonthToDateBillingSummaryCsvConfig config) throws MsrsReportParsingException {
        String customerAccount = getCellValue(data, config.getCustomerAccount());
        String reportCreationTimestamp = getCellValue(data, config.getReportCreationTimestamp());
        String billingStartDate = getCellValue(data, config.getBillingStartDate());
        String billingEndDate = getCellValue(data, config.getBillingEndDate());

        int amountColumn = config.getAmountColumn();
        BigDecimal totalCharges = convertAmountCell(getCellValue(data, cell(config.getTotalChargesRow(), amountColumn)));
        BigDecimal totalCredits = convertAmountCell(getCellValue(data, cell(config.getTotalCreditsRow(), amountColumn)));
        BigDecimal monthlyBillingNetTotal = convertAmountCell(getCellValue(data, cell(config.getMonthlyBillingNetTotalRow(), amountColumn)));
        BigDecimal monthLastWeeklyBillingNetTotal = convertAmountCell(getCellValue(data, cell(config.getMonthLastWeeklyBillingNetTotalRow(), amountColumn)));
        BigDecimal totalDueReceivable = convertAmountCell(getCellValue(data, cell(config.getTotalDueReceivableRow(), amountColumn)));

        MsrsMonthToDateBillingSummaryReport report = new MsrsMonthToDateBillingSummaryReport();
        report.setCustomerAccount(customerAccount);
        report.setReportCreationTimestamp(LocalDateTime.parse(reportCreationTimestamp, REPORT_CREATION_TIMESTAMP_FORMATTER));
        report.setBillingStartDate(LocalDate.parse(billingStartDate, DATE_FORMATTER));
        report.setBillingEndDate(LocalDate.parse(billingEndDate, DATE_FORMATTER));
        report.setTotalCharges(totalCharges);
        report.setTotalCredits(totalCredits);
        report.setMonthlyBillingNetTotal(monthlyBillingNetTotal);
        report.setMonthLastWeeklyBillingNetTotal(monthLastWeeklyBillingNetTotal);
        report.setTotalDueReceivable(totalDueReceivable);

        // Dates
        Map<Integer, LocalDate> columnToDate = new HashMap<>();

        for (int column = config.getDatesStartColumn(); column <= config.getDatesEndColumn(); column++) {
            String cell = data[config.getChargesHeaderRow()][column];
            columnToDate.put(column, LocalDate.parse(cell, DATE_FORMATTER));
        }

        collectLines(data, config.getChargesDataStartRow(), config.getChargesDataEndRow(), config, report, columnToDate, MsrsBillingChargeType.CHARGE);
        collectLines(data, config.getCreditsDataStartRow(), config.getCreditsDataEndRow(), config, report, columnToDate, MsrsBillingChargeType.CREDIT);

        return report;
    }

    private String getCellValue(String[][] data, Cell address) {
        return data[address.getRow()][address.getColumn()];
    }

    private void collectLines(String[][] data, int firstRow, int lastRow, MonthToDateBillingSummaryCsvConfig config,
                              MsrsMonthToDateBillingSummaryReport report, Map<Integer, LocalDate> columnToDate,
                              MsrsBillingChargeType type) throws MsrsReportParsingException {
        for (int row = firstRow; row <= lastRow; row++) {
            String[] rowData = data[row];

            MsrsMonthToDateBillingLine line = new MsrsMonthToDateBillingLine();
            line.setOrderIndex(row);
            line.setBillingChargeType(type);
            line.setChargeCode(rowData[config.getChargeCodeColumn()]);
            line.setAdjustment(convertAdjustmentCell(rowData[config.getAdjColumn()]));
            line.setName(rowData[config.getBillingLineItemNameColumn()]);
            line.setSourceBillingPeriodStart(convertSourceBillingPeriodStartCell(rowData[config.getSourceBillingPeriodStartColumn()]));
            line.setAmount(convertAmountCell(rowData[config.getAmountColumn()]));
            line.setMonthToDateBillingSummaryReport(report);

            for (int column = config.getDatesStartColumn(); column <= config.getDatesEndColumn(); column++) {
                MsrsMonthToDateBillingLineDailyAmount dailyAmount = new MsrsMonthToDateBillingLineDailyAmount();
                LocalDate date = columnToDate.get(column);

                if (date == null)
                    throw new MsrsReportParsingException("No date for column " + column);

                dailyAmount.setDate(date);
                dailyAmount.setAmount(round(new BigDecimal(rowData[column])));
                dailyAmount.setMonthToDateBillingLine(line);

                line.getDailyAmounts().add(dailyAmount);
            }

            report.getMonthToDateBillingLines().add(line);
        }
    }

    private boolean convertAdjustmentCell(String value) throws MsrsReportParsingException {
        if (isBlank(value))
            return false;

        if ("A".equals(value))
            return true;

        throw new MsrsReportParsingException("Unexpected adjustment value: " + value);
    }

    private LocalDate convertSourceBillingPeriodStartCell(String value) {
        return isBlank(value) ? null : LocalDate.parse(value, DATE_FORMATTER);
    }

    private BigDecimal convertAmountCell(String value) {
        return new BigDecimal(value);
    }

    @Getter
    @Setter
    class MonthToDateBillingSummaryCsvConfig {

        static final String CUSTOMER_ACCOUNT_HEADER = "Customer Account:";
        static final String REPORT_CREATION_TIMESTAMP_HEADER = "Report Creation Timestamp (EPT):";
        static final String BILLING_PERIOD_START_HEADER = "Billing Period Start:";
        static final String BILLING_PERIOD_END_HEADER = "Billing Period End:";
        static final String CHARGES_HEADER = "CHARGES";
        static final String TOTAL_CHARGES_HEADER = "Total Charges";
        static final String CREDITS_HEADER = "CREDITS";
        static final String TOTAL_CREDITS_HEADER = "Total Credits";
        static final String ADJ_HEADER = "ADJ";
        static final String BILLING_LINE_ITEM_NAME_HEADER = "BILLING LINE ITEM NAME";
        static final String SOURCE_BILLING_PERIOD_START_HEADER = "SOURCE BILLING PERIOD START";
        static final String BILLING_PERIOD_TO_DATE_AMOUNT_HEADER = "BILLING PERIOD-TO-DATE AMOUNT ($)";
        static final String MONTHLY_BILLING_NET_TOTAL_HEADER = "Monthly Billing Net Total";
        static final String MONTH_LAST_WEEKLY_BILLING_NET_TOTAL_HEADER = "Net Total for Month's Last Weekly Billing Period";
        static final String TOTAL_DUE_RECEIVABLE_HEADER = "Total Due / Receivable";
        static final String END_OF_REPORT_HEADER = "End of Report";

        private static final int DETECT = -1;

        // Addresses
        private Cell customerAccountLabel = cell(1, 0);
        private Cell customerAccount = cell(1, 1);
        private Cell reportCreationTimestampLabel = cell(1, 2);
        private Cell reportCreationTimestamp = cell(1, 3);
        private Cell billingStartDateLabel = cell(2, 0);
        private Cell billingStartDate = cell(2, 1);
        private Cell billingEndDateLabel = cell(2, 2);
        private Cell billingEndDate = cell(2, 3);

        // CHARGES
        private int chargesHeaderRow = 3;
        private int chargesDataStartRow = DETECT;
        private int chargesDataEndRow = DETECT;
        private int totalChargesRow = DETECT;

        // CREDITS
        private int creditsHeaderRow = DETECT;
        private int creditsDataStartRow = DETECT;
        private int creditsDataEndRow = DETECT;
        private int totalCreditsRow = DETECT;

        // Code of charge/credit
        private int chargeCodeColumn = 0;

        // ADJ
        private int adjColumn = 1;

        // BILLING LINE ITEM NAME
        private int billingLineItemNameColumn = 2;

        // SOURCE BILLING PERIOD START
        private int sourceBillingPeriodStartColumn = 3;

        // BILLING PERIOD-TO-DATE AMOUNT ($)
        private int amountColumn = 4;

        // Dates
        private int datesStartColumn = 5;
        private int datesEndColumn = DETECT;

        private int monthlyBillingNetTotalRow = DETECT;
        private int monthLastWeeklyBillingNetTotalRow = DETECT;
        private int totalDueReceivableRow = DETECT;

        private int endOfReportRow = DETECT;

    }

}

