package com.enelytics.wpps.service.msrs.api;

import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class MsrsReportApiClientImpl implements MsrsReportApiClient {
    private static final Logger logger = LoggerFactory.getLogger(MsrsReportApiClientImpl.class);

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

    private static final String MSRS_REPORT_URL = "https://msrs.pjm.com/msrs/browserless.do?report=${report}&version=L&format=${format}&start=${start}&stop=${stop}";

    private final MsrsReportApiConfig msrsReportApiConfig;

    public MsrsReportApiClientImpl(MsrsReportApiConfig msrsReportApiConfig) {
        this.msrsReportApiConfig = msrsReportApiConfig;
    }

    @Override
    public MsrsReportResponse get(MsrsReportRequest request) throws IOException {
        try (CloseableHttpClient httpClient = newHttpClient()) {
            logger.info("GET {}", request);
            HttpGet httpGet = prepareGetRequest(request);
            HttpResponse httpResponse = httpClient.execute(httpGet);

            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new IOException(String.format("Failed to retrieve '%s'; HTTP Status: %s; HTTP Status Reason: %s",
                        request, httpResponse.getStatusLine().getStatusCode(), httpResponse.getStatusLine().getReasonPhrase()));
            }

            return getMsrsReportResponse(httpResponse);
        }
    }

    private static CloseableHttpClient newHttpClient() {
        SocketConfig socketConfig = SocketConfig.custom()
                .setSoTimeout(30_000)
                .build();
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(30_000)
                .build();

        return HttpClientBuilder.create()
                .setUserAgent(USER_AGENT)
                .setDefaultSocketConfig(socketConfig)
                .setDefaultRequestConfig(requestConfig)
                .setDefaultCookieStore(new BasicCookieStore())
                .build();
    }

    private MsrsReportResponse getMsrsReportResponse(HttpResponse httpResponse) throws IOException {
        byte[] data = getData(httpResponse);
        Optional<String> filenameOptional = getFilename(httpResponse);

        MsrsReportResponse response = new MsrsReportResponse(data);
        filenameOptional.ifPresent(response::setFilename);

        return response;
    }

    private HttpGet prepareGetRequest(MsrsReportRequest request) {
        String url = prepareMsrsReportUrl(request);
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Accept-Encoding", "gzip");
        httpGet.setHeader("Authorization", getBasicAuthorizationHeader());

        return httpGet;
    }

    private byte[] getData(HttpResponse httpResponse) throws IOException {
        HttpEntity entity = httpResponse.getEntity();

        return IOUtils.toByteArray(entity.getContent());
    }

    private Optional<String> getFilename(HttpResponse response) {
        Header dispositionHeader = response.getFirstHeader("Content-Disposition");

        if (dispositionHeader != null) {
            String filename = dispositionHeader.toString().replaceFirst("(?i)^.*filename=\"?([^\"]+)\"?.*$", "$1");
            return Optional.of(filename);
        }

        return Optional.empty();
    }

    private String prepareMsrsReportUrl(MsrsReportRequest msrsReportRequest) {
        Map<String, String> valueMap = new HashMap<>();
        valueMap.put("report", msrsReportRequest.getType().getReportQueryParam());
        valueMap.put("format", msrsReportRequest.getFormat().getFormatQueryParam());
        valueMap.put("start", msrsReportRequest.getStartDate().format(DATE_FORMAT));
        valueMap.put("stop", msrsReportRequest.getEndDate().format(DATE_FORMAT));

        return StringSubstitutor.replace(MSRS_REPORT_URL, valueMap);

    }


    private String getBasicAuthorizationHeader() {
        String authString = msrsReportApiConfig.getUsername() + ":" + msrsReportApiConfig.getPassword();

        return "Basic " + DatatypeConverter.printBase64Binary(authString.getBytes());
    }

}
