package com.enelytics.wpps.service.msrs.updater;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransaction;
import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionHourlyValue;
import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionsReport;
import com.enelytics.wpps.persistance.domain.types.msrs.MsrsEnergyTransactionType;
import com.enelytics.wpps.persistance.repositories.msrs.MsrsEnergyTransactionsReportRepository;
import com.enelytics.wpps.service.msrs.api.MsrsReportApiClient;
import com.enelytics.wpps.service.msrs.api.MsrsReportFormat;
import com.enelytics.wpps.service.msrs.api.MsrsReportResponse;
import com.enelytics.wpps.service.msrs.fetcher.MsrsReportSourceFetcher;
import com.enelytics.wpps.service.msrs.parser.EnergyTransactionsCsvParser;
import com.enelytics.wpps.service.msrs.parser.exception.MsrsReportParsingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.enelytics.wpps.service.msrs.updater.UpdateHelper.getStartMonth;

@Service
public class MsrsEnergyTransactionsReportsUpdateService {

    private static final Logger logger = LoggerFactory.getLogger(MsrsEnergyTransactionsReportsUpdateService.class);

    private final EntityManager entityManager;
    private final MsrsEnergyTransactionsReportRepository repository;
    private final MsrsReportSourceFetcher fetcher;
    private final EnergyTransactionsCsvParser parser;

    public MsrsEnergyTransactionsReportsUpdateService(EntityManager entityManager,
                                                      MsrsEnergyTransactionsReportRepository repository,
                                                      MsrsReportApiClient apiClient) {
        this.entityManager = entityManager;
        this.repository = repository;
        this.fetcher = new MsrsReportSourceFetcher(apiClient);
        this.parser = new EnergyTransactionsCsvParser();
    }

    @Transactional
    public void updateDADailyEnergyTransactionsReports() throws IOException, MsrsReportParsingException {
        YearMonth month = getStartMonth(repository, MsrsEnergyTransactionType.RT_1D);

        do {
            updateDADailyEnergyTransactionsReport(month);
            month = month.plusMonths(1);
        } while (!month.isAfter(YearMonth.now()));
    }

    @Transactional
    public void updateRTDailyEnergyTransactionsReports() throws IOException, MsrsReportParsingException {
        YearMonth month = getStartMonth(repository, MsrsEnergyTransactionType.RT_1D);

        do {
            updateRTDailyEnergyTransactionsReport(month);
            month = month.plusMonths(1);
        } while (!month.isAfter(YearMonth.now()));
    }

    private void updateDADailyEnergyTransactionsReport(YearMonth month) throws MsrsReportParsingException, IOException {
        MsrsReportResponse response = fetcher.fetchDADailyEnergyTransactions(MsrsReportFormat.CSV, month);
        Optional<MsrsEnergyTransactionsReport> existing = repository.findFirstByTypeAndMonth(MsrsEnergyTransactionType.DA_1D, month);
        MsrsEnergyTransactionsReport newReport = parser.parse(response);

        if (existing.isPresent()) {
            MsrsEnergyTransactionsReport existingReport = existing.get();
            updateEnergyTransactionsReport(existingReport, newReport);
            logger.info("Successfully updated existing Day-Ahead Daily Energy Transaction report {}", month);
        } else {
            repository.save(newReport);
            logger.info("Successfully saved new Day-Ahead Daily Energy Transaction report {}", month);
        }

    }

    private void updateRTDailyEnergyTransactionsReport(YearMonth month) throws IOException, MsrsReportParsingException {
        MsrsReportResponse response = fetcher.fetchRTDailyEnergyTransactions(MsrsReportFormat.CSV, month);
        Optional<MsrsEnergyTransactionsReport> existing = repository.findFirstByTypeAndMonth(MsrsEnergyTransactionType.RT_1D, month);
        MsrsEnergyTransactionsReport newReport = parser.parse(response);

        if (existing.isPresent()) {
            MsrsEnergyTransactionsReport existingReport = existing.get();
            updateEnergyTransactionsReport(existingReport, newReport);
            logger.info("Successfully updated existing Real-Time Daily Energy Transaction report {}", month);
        } else {
            repository.save(newReport);
            logger.info("Successfully saved new Real-Time Daily Energy Transaction report {}", month);
        }
    }

    private void updateEnergyTransactionsReport(MsrsEnergyTransactionsReport existingReport, MsrsEnergyTransactionsReport newReport) {
        BeanUtils.copyProperties(newReport, existingReport, "id", "energyTransactions");

        List<MsrsEnergyTransaction> updatedEnergyTransactions = getUpdatedEnergyTransactions(existingReport, newReport.getEnergyTransactions());
        boolean complete = isComplete(existingReport, updatedEnergyTransactions);

        existingReport.getEnergyTransactions().clear();
        existingReport.getEnergyTransactions().addAll(updatedEnergyTransactions);
        existingReport.setComplete(complete);

        entityManager.merge(existingReport);
    }

    private List<MsrsEnergyTransaction> getUpdatedEnergyTransactions(MsrsEnergyTransactionsReport existingReport, List<MsrsEnergyTransaction> newEnergyTransactions) {
        List<MsrsEnergyTransaction> existingEnergyTransactions = existingReport.getEnergyTransactions();

        return newEnergyTransactions.stream()
                .peek(newEnergyTransaction -> {
                    // find index of existing energy transaction
                    int existingEnergyTransactionIndex = existingReport.getEnergyTransactions().indexOf(newEnergyTransaction);

                    if (existingEnergyTransactionIndex != -1) {
                        MsrsEnergyTransaction existingEnergyTransaction = existingEnergyTransactions.get(existingEnergyTransactionIndex);
                        List<MsrsEnergyTransactionHourlyValue> updatedHourlyValues = getUpdatedHourlyValues(existingEnergyTransaction, newEnergyTransaction);

                        newEnergyTransaction.setId(existingEnergyTransaction.getId());
                        newEnergyTransaction.getHourlyValues().clear();
                        newEnergyTransaction.getHourlyValues().addAll(updatedHourlyValues);
                    }

                    newEnergyTransaction.setEnergyTransactionsReport(existingReport);
                })
                .collect(Collectors.toList());
    }

    private List<MsrsEnergyTransactionHourlyValue> getUpdatedHourlyValues(MsrsEnergyTransaction existingEnergyTransaction, MsrsEnergyTransaction newEnergyTransaction) {
        List<MsrsEnergyTransactionHourlyValue> existingHourlyValues = existingEnergyTransaction.getHourlyValues();

        return newEnergyTransaction.getHourlyValues().stream()
                .peek(newHourlyValue -> {
                    int existingHourlyValueIndex = existingHourlyValues.indexOf(newHourlyValue);

                    if (existingHourlyValueIndex != -1) {
                        MsrsEnergyTransactionHourlyValue existingHourlyValue = existingHourlyValues.get(existingHourlyValueIndex);

                        newHourlyValue.setId(existingHourlyValue.getId());
                    } else {
                        throw new IllegalStateException("Something went wrong: existing and new energy transaction should have 24 hourly values (with same time)");
                    }
                }).collect(Collectors.toList());
    }

    private boolean isComplete(MsrsEnergyTransactionsReport existingReport, List<MsrsEnergyTransaction> updatedEnergyTransactions) {
        return updatedEnergyTransactions.stream()
                .anyMatch(energyTransaction -> energyTransaction.getDate().equals(existingReport.getMonth().atEndOfMonth()));
    }

}
