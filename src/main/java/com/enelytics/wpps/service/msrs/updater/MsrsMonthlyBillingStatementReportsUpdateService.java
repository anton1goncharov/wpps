package com.enelytics.wpps.service.msrs.updater;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementLine;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementReport;
import com.enelytics.wpps.persistance.repositories.msrs.MsrsMonthlyBillingStatementReportRepository;
import com.enelytics.wpps.service.msrs.api.MsrsReportApiClient;
import com.enelytics.wpps.service.msrs.api.MsrsReportFormat;
import com.enelytics.wpps.service.msrs.api.MsrsReportResponse;
import com.enelytics.wpps.service.msrs.fetcher.MsrsReportSourceFetcher;
import com.enelytics.wpps.service.msrs.parser.MonthlyBillingStatementCsvParser;
import com.enelytics.wpps.service.msrs.parser.exception.MsrsReportParsingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.enelytics.wpps.service.msrs.updater.UpdateHelper.getStartMonth;

@Service
public class MsrsMonthlyBillingStatementReportsUpdateService {

    private static final Logger logger = LoggerFactory.getLogger(MsrsMonthlyBillingStatementReportsUpdateService.class);

    private final EntityManager entityManager;
    private final MsrsMonthlyBillingStatementReportRepository repository;
    private final MsrsReportSourceFetcher fetcher;
    private final MonthlyBillingStatementCsvParser monthlyBillingStatementCsvParser;

    public MsrsMonthlyBillingStatementReportsUpdateService(EntityManager entityManager,
                                                           MsrsMonthlyBillingStatementReportRepository repository,
                                                           MsrsReportApiClient apiClient) {
        this.entityManager = entityManager;
        this.repository = repository;
        this.fetcher = new MsrsReportSourceFetcher(apiClient);
        this.monthlyBillingStatementCsvParser = new MonthlyBillingStatementCsvParser();
    }

    @Transactional
    public void updateMonthlyBillingStatementReports() {
        YearMonth month = getStartMonth(repository);

        do {
            try {
                updateMonthlyBillingStatementReport(month);
            } catch (IOException | MsrsReportParsingException e) {
                logger.error("Failed to update Monthly Billing Statement report {}", month, e);
            }
            month = month.plusMonths(1);
        } while (!month.isAfter(YearMonth.now()));
    }

    private void updateMonthlyBillingStatementReport(YearMonth month) throws IOException, MsrsReportParsingException {
        MsrsReportResponse response = fetcher.fetchMonthlyBillingStatement(MsrsReportFormat.CSV, month);
        Optional<MsrsMonthlyBillingStatementReport> existing = repository.findFirstByMonth(month);
        MsrsMonthlyBillingStatementReport newReport = monthlyBillingStatementCsvParser.parse(response);

        if (existing.isPresent()) {
            MsrsMonthlyBillingStatementReport existingReport = existing.get();
            updateMonthlyBillingStatementReport(existingReport, newReport);
            logger.info("Successfully updated existing Monthly Billing Statement report {}", month);
        } else {
            repository.save(newReport);
            logger.info("Successfully saved new Monthly Billing Statement report {}", month);
        }
    }

    private void updateMonthlyBillingStatementReport(MsrsMonthlyBillingStatementReport existingReport, MsrsMonthlyBillingStatementReport newReport) {
        BeanUtils.copyProperties(newReport, existingReport, "id", "monthlyBillingStatementLines");
        List<MsrsMonthlyBillingStatementLine> updatedLines = getUpdatedMonthlyBillingStatementLines(existingReport, newReport);

        existingReport.getMonthlyBillingStatementLines().clear();
        existingReport.getMonthlyBillingStatementLines().addAll(updatedLines);

        entityManager.merge(existingReport);
    }

    private List<MsrsMonthlyBillingStatementLine> getUpdatedMonthlyBillingStatementLines(MsrsMonthlyBillingStatementReport existingReport, MsrsMonthlyBillingStatementReport newReport) {
        List<MsrsMonthlyBillingStatementLine> monthlyLines = existingReport.getMonthlyBillingStatementLines();

        return newReport.getMonthlyBillingStatementLines().stream()
                .peek(newLine -> {
                    // find index of existing monthly billing statement line
                    int existingLineIndex = monthlyLines.indexOf(newLine);

                    if (existingLineIndex != -1) {
                        MsrsMonthlyBillingStatementLine existingLine = monthlyLines.get(existingLineIndex);
                        newLine.setId(existingLine.getId());
                    }

                    newLine.setMonthlyBillingStatementReport(existingReport);
                }).collect(Collectors.toList());
    }

}
