package com.enelytics.wpps.service.msrs.parser.exception;

public class MsrsReportParsingException extends Exception {

    private static final long serialVersionUID = 1L;

    public MsrsReportParsingException(String message) {
        super(message);
    }

    public MsrsReportParsingException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
