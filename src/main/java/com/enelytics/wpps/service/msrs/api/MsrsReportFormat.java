package com.enelytics.wpps.service.msrs.api;

public enum MsrsReportFormat {

    CSV("CSV", "C"),
    XML("XML", "X");

    private final String label;
    private final String formatQueryParam;

    MsrsReportFormat(String label, String formatQueryParam) {
        this.label = label;
        this.formatQueryParam = formatQueryParam;
    }

    public String getLabel() {
        return label;
    }

    public String getFormatQueryParam() {
        return formatQueryParam;
    }

}
