package com.enelytics.wpps.service.msrs.parser.util;

import lombok.Data;

@Data
public class Cell {

    private final int row;
    private int column;

    private Cell(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public static Cell cell(int row, int column) {
        return new Cell(row, column);
    }

}
