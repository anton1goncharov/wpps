package com.enelytics.wpps.service.msrs.fetcher;

import com.enelytics.wpps.service.msrs.api.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.YearMonth;

public class MsrsReportSourceFetcher {

    private final MsrsReportApiClient apiClient;

    public MsrsReportSourceFetcher(MsrsReportApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public MsrsReportResponse fetchMonthToDateBillingSummary(MsrsReportFormat format, YearMonth month) throws IOException {
        return fetchMsrsReport(MsrsReportType.MTDBS, format, month);
    }

    public MsrsReportResponse fetchMonthlyBillingStatement(MsrsReportFormat format, YearMonth month) throws IOException {
        return fetchMsrsReport(MsrsReportType.MBS, format, month);
    }

    public MsrsReportResponse fetchDADailyEnergyTransactions(MsrsReportFormat format, YearMonth month) throws IOException {
        return fetchMsrsReport(MsrsReportType.DA_DET, format, month);
    }

    public MsrsReportResponse fetchRTDailyEnergyTransactions(MsrsReportFormat format, YearMonth month) throws IOException {
        return fetchMsrsReport(MsrsReportType.RT_DET, format, month);
    }

    private MsrsReportResponse fetchMsrsReport(MsrsReportType type, MsrsReportFormat format, YearMonth month) throws IOException {
        LocalDate startDate = month.atDay(1);
        LocalDate endDate = month.atEndOfMonth();
        MsrsReportRequest request = MsrsReportRequest.builder()
                .type(type)
                .format(format)
                .startDate(startDate)
                .endDate(endDate)
                .build();

        return apiClient.get(request);
    }

}
