package com.enelytics.wpps.service.msrs.parser;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementLine;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementReport;
import com.enelytics.wpps.persistance.domain.types.msrs.MsrsBillingChargeType;
import com.enelytics.wpps.service.msrs.api.MsrsReportResponse;
import com.enelytics.wpps.service.msrs.parser.exception.MsrsReportParsingException;
import com.enelytics.wpps.service.msrs.parser.util.Cell;
import com.enelytics.wpps.service.msrs.parser.util.CsvParserUtils;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.enelytics.wpps.service.msrs.parser.util.Cell.cell;
import static com.enelytics.wpps.service.msrs.parser.util.CsvParserUtils.validateCell;
import static com.enelytics.wpps.service.msrs.parser.MonthlyBillingStatementCsvParser.MonthlyBillingStatementCsvConfig.*;
import static com.enelytics.wpps.utils.BigDecimalUtil.round;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class MonthlyBillingStatementCsvParser {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    public MsrsMonthlyBillingStatementReport parse(MsrsReportResponse response) throws MsrsReportParsingException {
        byte[] reportData = response.getData();
        String[][] data = CsvParserUtils.readCsvToMatrix(reportData);
        MonthlyBillingStatementCsvConfig config = prepareCsvConfig(data);

        MsrsMonthlyBillingStatementReport report = parse(data, config);
        report.setData(reportData);
        report.setFilename(response.getFilename());

        return report;
    }

    private MonthlyBillingStatementCsvConfig prepareCsvConfig(String[][] data) throws MsrsReportParsingException {
        MonthlyBillingStatementCsvConfig config = new MonthlyBillingStatementCsvConfig();

        validateCells(data, config);

        findChargesDataStartRow(data, config);

        try {
            findChargesDataEndRow(data, config);
        } catch (MsrsReportParsingException e) {
            throw new MsrsReportParsingException("Report is not available yet", e);
        }

        findCreditsHeaderRow(data, config);
        findCreditsDataStartRow(data, config);
        findCreditsDataEndRow(data, config);
        findMonthlyBillingNetTotalRow(data, config);
        findLastWeeklyBillingNetTotalRow(data, config);
        findTotalDueReceivableRow(data, config);

        return config;
    }

    private void validateCells(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        validateCell(data, CUSTOMER_ACCOUNT_HEADER, config.getCustomerAccountLabel());
        validateCell(data, CUSTOMER_CODE_HEADER, config.getCustomerCodeLabel());
        validateCell(data, CUSTOMER_ID_HEADER, config.getCustomerIdLabel());
        validateCell(data, FINAL_BILLING_STATEMENT_ISSUED_HEADER, config.getFinalBillingsStatementIssuedLabel());
        validateCell(data, BILLING_PERIOD_START_HEADER, config.getBillingStartDateLabel());
        validateCell(data, BILLING_PERIOD_END_HEADER, config.getBillingEndDateLabel());
        validateCell(data, INVOICE_NUMBER_HEADER, config.getInvoiceNumberLabel());
        validateCell(data, INVOICE_DUE_DATE_HEADER, config.getInvoiceDueDateLabel());
        validateCell(data, CHARGES_HEADER, config.getChargesHeaderRow(), config.getChargeCodeColumn());
        validateCell(data, ADJ_HEADER, config.getChargesHeaderRow(), config.getAdjColumn());
        validateCell(data, BILLING_LINE_ITEM_NAME_HEADER, config.getChargesHeaderRow(), config.getBillingLineItemNameColumn());
        validateCell(data, SOURCE_BILLING_PERIOD_START_HEADER, config.getChargesHeaderRow(), config.getSourceBillingPeriodStartColumn());
        validateCell(data, AMOUNT_HEADER, config.getChargesHeaderRow(), config.getAmountColumn());
    }

    private void findChargesDataStartRow(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        int row = config.getChargesHeaderRow() + 1;

        if (row >= data.length || isBlank(data[row][config.getChargeCodeColumn()]))
            throw new MsrsReportParsingException("No charges data found");

        config.setChargesDataStartRow(row);
    }

    private void findChargesDataEndRow(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getChargesDataStartRow(); row < data.length; row++) {
            String cell = data[row][config.getBillingLineItemNameColumn()];

            if (TOTAL_CHARGES_HEADER.equals(cell)) {
                config.setChargesDataEndRow(row - 1);
                config.setTotalChargesRow(row);
                break;
            }
        }

        if (config.getChargesDataEndRow() < 0)
            throw new MsrsReportParsingException("Charges data end row not found");
    }

    private void findCreditsHeaderRow(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getChargesDataEndRow() + 1; row < data.length; row++) {
            String cell = data[row][config.getChargeCodeColumn()];

            if (CREDITS_HEADER.equals(cell)) {
                config.setCreditsHeaderRow(row);
                break;
            }
        }

        if (config.getCreditsHeaderRow() < 0)
            throw new MsrsReportParsingException("Credits header row not found");
    }

    private void findCreditsDataStartRow(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        int row = config.getCreditsHeaderRow() + 1;

        if (row >= data.length || isBlank(data[row][config.getChargeCodeColumn()]))
            throw new MsrsReportParsingException("No credit data found");

        config.setCreditsDataStartRow(row);
    }

    private void findCreditsDataEndRow(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getCreditsDataStartRow(); row < data.length; row++) {
            String cell = data[row][config.getBillingLineItemNameColumn()];

            if (TOTAL_CREDITS_HEADER.equals(cell)) {
                config.setCreditsDataEndRow(row - 1);
                config.setTotalCreditsRow(row);
                break;
            }
        }

        if (config.getCreditsDataEndRow() < 0)
            throw new MsrsReportParsingException("Credits data end row not found");
    }

    private void findMonthlyBillingNetTotalRow(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getTotalCreditsRow(); row < data.length; row++) {
            String cell = data[row][config.getBillingLineItemNameColumn()];

            if (MONTHLY_BILLING_NET_TOTAL_HEADER.equals(cell)) {
                config.setMonthlyBillingNetTotalRow(row);
                break;
            }
        }

        if (config.getMonthlyBillingNetTotalRow() < 0)
            throw new MsrsReportParsingException("Monthly Billing Net Total row not found");
    }

    private void findLastWeeklyBillingNetTotalRow(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getTotalCreditsRow(); row < data.length; row++) {
            String cell = data[row][config.getBillingLineItemNameColumn()];

            if (PREVIOUS_WEEKLY_BILLING_NET_TOTAL_HEADER.equals(cell)) {
                config.setPreviousWeeklyBillingNetTotalRow(row);
                break;
            }
        }

        if (config.getPreviousWeeklyBillingNetTotalRow() < 0)
            throw new MsrsReportParsingException("Net Total for Month's Last Weekly Billing Period row not found");
    }

    private void findTotalDueReceivableRow(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        for (int row = config.getTotalCreditsRow(); row < data.length; row++) {
            String cell = data[row][config.getBillingLineItemNameColumn()];

            if (TOTAL_DUE_RECEIVABLE_HEADER.equals(cell)) {
                config.setTotalDueReceivableRow(row);
                break;
            }
        }

        if (config.getTotalDueReceivableRow() < 0)
            throw new MsrsReportParsingException("Total Due / Receivable row not found");
    }

    private MsrsMonthlyBillingStatementReport parse(String[][] data, MonthlyBillingStatementCsvConfig config) throws MsrsReportParsingException {
        String customerAccount = getCellValue(data, config.getCustomerAccount());
        String customerCode = getCellValue(data, config.getCustomerCode());
        String customerId = getCellValue(data, config.getCustomerId());
        String finalBillingsStatementIssued = getCellValue(data, config.getFinalBillingsStatementIssued());
        String billingStartDate = getCellValue(data, config.getBillingStartDate());
        String billingEndDate = getCellValue(data, config.getBillingEndDate());
        String invoiceNumber = getCellValue(data, config.getInvoiceNumber());
        String invoiceDueDate = getCellValue(data, config.getInvoiceDueDate());

        int amountColumn = config.getAmountColumn();
        BigDecimal totalCharges = convertAmountCell(getCellValue(data, cell(config.getTotalChargesRow(), amountColumn)));
        BigDecimal totalCredits = convertAmountCell(getCellValue(data, cell(config.getTotalCreditsRow(), amountColumn)));
        BigDecimal monthlyBillingNetTotal = convertAmountCell(getCellValue(data, cell(config.getMonthlyBillingNetTotalRow(), amountColumn)));
        BigDecimal previousWeeklyBillingNetTotal = convertAmountCell(getCellValue(data, cell(config.getPreviousWeeklyBillingNetTotalRow(), amountColumn)));
        BigDecimal totalDueReceivable = convertAmountCell(getCellValue(data, cell(config.getTotalDueReceivableRow(), amountColumn)));

        MsrsMonthlyBillingStatementReport report = new MsrsMonthlyBillingStatementReport();
        report.setCustomerAccount(customerAccount);
        report.setCustomerCode(customerCode);
        report.setCustomerId(customerId);
        report.setFinalBillingStatementIssued(LocalDateTime.parse(finalBillingsStatementIssued, DATE_TIME_FORMATTER));
        report.setBillingStartDate(LocalDate.parse(billingStartDate, DATE_FORMATTER));
        report.setBillingEndDate(LocalDate.parse(billingEndDate, DATE_FORMATTER));
        report.setInvoiceNumber(invoiceNumber);
        report.setInvoiceDueDate(LocalDateTime.parse(invoiceDueDate, DATE_TIME_FORMATTER));
        report.setTotalCharges(totalCharges);
        report.setTotalCredits(totalCredits);
        report.setMonthlyBillingNetTotal(monthlyBillingNetTotal);
        report.setPreviousWeeklyBillingNetTotal(previousWeeklyBillingNetTotal);
        report.setTotalDueReceivable(totalDueReceivable);

        // charges lines
        collectLines(data, config.getChargesDataStartRow(), config.getChargesDataEndRow(), config, report, MsrsBillingChargeType.CHARGE);
        collectLines(data, config.getCreditsDataStartRow(), config.getCreditsDataEndRow(), config, report, MsrsBillingChargeType.CREDIT);

        return report;
    }

    private String getCellValue(String[][] data, Cell address) {
        return data[address.getRow()][address.getColumn()];
    }

    private void collectLines(String[][] data, int firstRow, int lastRow, MonthlyBillingStatementCsvConfig config,
                              MsrsMonthlyBillingStatementReport report,
                              MsrsBillingChargeType type) throws MsrsReportParsingException {
        for (int row = firstRow; row <= lastRow; row++) {
            String[] rowData = data[row];

            MsrsMonthlyBillingStatementLine line = new MsrsMonthlyBillingStatementLine();
            line.setOrderIndex(row);
            line.setBillingChargeType(type);
            line.setChargeCode(rowData[config.getChargeCodeColumn()]);
            line.setAdjustment(convertAdjustmentCell(rowData[config.getAdjColumn()]));
            line.setName(rowData[config.getBillingLineItemNameColumn()]);
            line.setSourceBillingPeriodStart(convertSourceBillingPeriodStartCell(rowData[config.getSourceBillingPeriodStartColumn()]));
            line.setAmount(convertAmountCell(rowData[config.getAmountColumn()]));
            line.setMonthlyBillingStatementReport(report);

            report.getMonthlyBillingStatementLines().add(line);
        }
    }

    private boolean convertAdjustmentCell(String value) throws MsrsReportParsingException {
        if (isBlank(value))
            return false;

        if ("A".equals(value))
            return true;

        throw new MsrsReportParsingException("Unexpected adjustment value: " + value);
    }

    private LocalDate convertSourceBillingPeriodStartCell(String value) {
        return isBlank(value) ? null : LocalDate.parse(value, DATE_FORMATTER);
    }

    private BigDecimal convertAmountCell(String value) {
        return round(new BigDecimal(value));
    }

    @Getter
    @Setter
    static class MonthlyBillingStatementCsvConfig {

        static final String CUSTOMER_ACCOUNT_HEADER = "CUSTOMER ACCOUNT:";
        static final String CUSTOMER_CODE_HEADER = "CUSTOMER CODE:";
        static final String CUSTOMER_ID_HEADER = "CUSTOMER ID:";
        static final String FINAL_BILLING_STATEMENT_ISSUED_HEADER = "FINAL BILLING STATEMENT ISSUED:";
        static final String BILLING_PERIOD_START_HEADER = "BILLING PERIOD START DATE:";
        static final String BILLING_PERIOD_END_HEADER = "BILLING PERIOD END DATE:";
        static final String INVOICE_NUMBER_HEADER = "INVOICE NUMBER:";
        static final String INVOICE_DUE_DATE_HEADER = "INVOICE DUE DATE:";
        static final String CHARGES_HEADER = "CHARGES";
        static final String TOTAL_CHARGES_HEADER = "Total Charges";
        static final String CREDITS_HEADER = "CREDITS";
        static final String TOTAL_CREDITS_HEADER = "Total Credits";
        static final String ADJ_HEADER = "ADJ";
        static final String BILLING_LINE_ITEM_NAME_HEADER = "BILLING LINE ITEM NAME";
        static final String SOURCE_BILLING_PERIOD_START_HEADER = "SOURCE BILLING PERIOD START";
        static final String AMOUNT_HEADER = "AMOUNT ($)";
        static final String MONTHLY_BILLING_NET_TOTAL_HEADER = "Monthly Billing Net Total";
        static final String PREVIOUS_WEEKLY_BILLING_NET_TOTAL_HEADER = "Previous Weekly Billing Net Total";
        static final String TOTAL_DUE_RECEIVABLE_HEADER = "Total Due / Receivable";
        static final String END_OF_REPORT_HEADER = "End of Report";

        private static final int DETECT = -1;

        private Cell customerAccountLabel = cell(1, 0);
        private Cell customerAccount = cell(1, 1);
        private Cell customerCodeLabel = cell(2, 0);
        private Cell customerCode = cell(2, 1);
        private Cell customerIdLabel = cell(3, 0);
        private Cell customerId = cell(3, 1);
        private Cell finalBillingsStatementIssuedLabel = cell(4, 0);
        private Cell finalBillingsStatementIssued = cell(4, 1);
        private Cell billingStartDateLabel = cell(5, 0);
        private Cell billingStartDate = cell(5, 1);
        private Cell billingEndDateLabel = cell(6, 0);
        private Cell billingEndDate = cell(6, 1);
        private Cell invoiceNumberLabel = cell(7, 0);
        private Cell invoiceNumber = cell(7, 1);
        private Cell invoiceDueDateLabel = cell(8, 0);
        private Cell invoiceDueDate = cell(8, 1);

        // CHARGES
        private int chargesHeaderRow = 10;
        private int chargesDataStartRow = DETECT;
        private int chargesDataEndRow = DETECT;
        private int totalChargesRow = DETECT;

        // CREDITS
        private int creditsHeaderRow = DETECT;
        private int creditsDataStartRow = DETECT;
        private int creditsDataEndRow = DETECT;
        private int totalCreditsRow = DETECT;

        // Code of charge/credit
        private int chargeCodeColumn = 0;

        // ADJ
        private int adjColumn = 1;

        // BILLING LINE ITEM NAME
        private int billingLineItemNameColumn = 2;

        // SOURCE BILLING PERIOD START
        private int sourceBillingPeriodStartColumn = 3;

        // AMOUNT ($)
        private int amountColumn = 4;

        private int monthlyBillingNetTotalRow = DETECT;
        private int previousWeeklyBillingNetTotalRow = DETECT;
        private int totalDueReceivableRow = DETECT;

        private int endOfReportRow = DETECT;
    }

}

