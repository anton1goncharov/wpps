package com.enelytics.wpps.service.msrs.api;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class MsrsReportResponse {

    @NonNull
    private byte[] data;

    private String filename;

}
