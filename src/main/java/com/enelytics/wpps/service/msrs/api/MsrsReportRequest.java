package com.enelytics.wpps.service.msrs.api;

import lombok.*;

import java.time.LocalDate;

@Builder
@Data
public class MsrsReportRequest {

    @NonNull
    private MsrsReportType type;

    @NonNull
    private MsrsReportFormat format;

    @NonNull
    private LocalDate startDate;

    @NonNull
    private LocalDate endDate;

}
