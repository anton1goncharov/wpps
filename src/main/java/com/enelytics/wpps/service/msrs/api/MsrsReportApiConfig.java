package com.enelytics.wpps.service.msrs.api;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "service.msrsreport.api")
class MsrsReportApiConfig {

    private String username;
    private String password;

}
