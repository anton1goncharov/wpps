package com.enelytics.wpps.service.msrs.updater;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingLine;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingLineDailyAmount;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingSummaryReport;
import com.enelytics.wpps.persistance.repositories.msrs.MsrsMonthToDateBillingSummaryReportRepository;
import com.enelytics.wpps.service.msrs.api.MsrsReportApiClient;
import com.enelytics.wpps.service.msrs.api.MsrsReportFormat;
import com.enelytics.wpps.service.msrs.api.MsrsReportResponse;
import com.enelytics.wpps.service.msrs.fetcher.MsrsReportSourceFetcher;
import com.enelytics.wpps.service.msrs.parser.MonthToDateBillingSummaryCsvParser;
import com.enelytics.wpps.service.msrs.parser.exception.MsrsReportParsingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.enelytics.wpps.service.msrs.updater.UpdateHelper.getStartMonth;

@Service
public class MsrsMonthToDateBillingSummaryReportsUpdateService {

    private static final Logger logger = LoggerFactory.getLogger(MsrsMonthToDateBillingSummaryReportsUpdateService.class);

    private final EntityManager entityManager;
    private final MsrsMonthToDateBillingSummaryReportRepository repository;
    private final MsrsReportSourceFetcher fetcher;
    private final MonthToDateBillingSummaryCsvParser monthToDateBillingSummaryCsvParser;

    public MsrsMonthToDateBillingSummaryReportsUpdateService(EntityManager entityManager,
                                                             MsrsMonthToDateBillingSummaryReportRepository repository,
                                                             MsrsReportApiClient apiClient) {
        this.entityManager = entityManager;
        this.repository = repository;
        this.fetcher = new MsrsReportSourceFetcher(apiClient);
        this.monthToDateBillingSummaryCsvParser = new MonthToDateBillingSummaryCsvParser();
    }


    @Transactional
    public void updateMonthToDateBillingSummaryReports() {
        YearMonth month = getStartMonth(repository);

        do {
            try {
                updateMonthToDateBillingSummaryReport(month);
            } catch (IOException | MsrsReportParsingException e) {
                logger.error("Failed to update Month-to-Date Billing Summary report " + month, e);
            }
            month = month.plusMonths(1);
        } while (!month.isAfter(YearMonth.now()));
    }

    private void updateMonthToDateBillingSummaryReport(YearMonth month) throws IOException, MsrsReportParsingException {
        MsrsReportResponse response = fetcher.fetchMonthToDateBillingSummary(MsrsReportFormat.CSV, month);
        Optional<MsrsMonthToDateBillingSummaryReport> existing = repository.findFirstByMonth(month);
        MsrsMonthToDateBillingSummaryReport newReport = monthToDateBillingSummaryCsvParser.parse(response);

        if (existing.isPresent()) {
            MsrsMonthToDateBillingSummaryReport existingReport = existing.get();
            updateMonthToDateBillingSummaryReport(existingReport, newReport);
            logger.info("Successfully updated existing Monthly Billing Statement report {}", month);
        } else {
            repository.save(newReport);
            logger.info("Successfully saved new Monthly Billing Statement report {}", month);
        }
    }

    private void updateMonthToDateBillingSummaryReport(MsrsMonthToDateBillingSummaryReport existingReport, MsrsMonthToDateBillingSummaryReport newReport) {
        BeanUtils.copyProperties(newReport, existingReport, "id", "monthToDateBillingLines");
        List<MsrsMonthToDateBillingLine> updatedLines = getUpdatedMonthToDateBillingSummaryReportLines(existingReport, newReport);

        existingReport.getMonthToDateBillingLines().clear();
        existingReport.getMonthToDateBillingLines().addAll(updatedLines);

        entityManager.merge(existingReport);
    }

    private List<MsrsMonthToDateBillingLine> getUpdatedMonthToDateBillingSummaryReportLines(MsrsMonthToDateBillingSummaryReport existingReport, MsrsMonthToDateBillingSummaryReport newReport) {
        List<MsrsMonthToDateBillingLine> monthlyLines = existingReport.getMonthToDateBillingLines();

        return newReport.getMonthToDateBillingLines().stream()
                .peek(newLine -> {
                    // find index of existing monthly billing statement line
                    int existingLineIndex = monthlyLines.indexOf(newLine);

                    if (existingLineIndex != -1) {
                        MsrsMonthToDateBillingLine existingLine = monthlyLines.get(existingLineIndex);
                        List<MsrsMonthToDateBillingLineDailyAmount> updatedDailyAmounts = getUpdatedDailyAmounts(existingLine, newLine);

                        newLine.setId(existingLine.getId());
                        newLine.getDailyAmounts().clear();
                        newLine.getDailyAmounts().addAll(updatedDailyAmounts);
                    }

                    newLine.setMonthToDateBillingSummaryReport(existingReport);
                }).collect(Collectors.toList());
    }

    private List<MsrsMonthToDateBillingLineDailyAmount> getUpdatedDailyAmounts(MsrsMonthToDateBillingLine existingLine, MsrsMonthToDateBillingLine newLine) {
        List<MsrsMonthToDateBillingLineDailyAmount> existingDailyAmounts = existingLine.getDailyAmounts();

        return newLine.getDailyAmounts().stream()
                .peek(newDailyAmount -> {
                    int existingDailyAmountIndex = existingDailyAmounts.indexOf(newDailyAmount);

                    if (existingDailyAmountIndex != -1) {
                        MsrsMonthToDateBillingLineDailyAmount existingDailyAmount = existingDailyAmounts.get(existingDailyAmountIndex);

                        newDailyAmount.setId(existingDailyAmount.getId());
                    } else {
                        throw new IllegalStateException("Something went wrong: existing and new Month-to-Date billing line should have daily amount (with same day)");
                    }
                }).collect(Collectors.toList());
    }

}
