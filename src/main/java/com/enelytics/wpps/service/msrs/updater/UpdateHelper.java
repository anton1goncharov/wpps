package com.enelytics.wpps.service.msrs.updater;

import com.enelytics.wpps.persistance.domain.entities.msrs.MsrsReport;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingSummaryReport;
import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementReport;
import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionsReport;
import com.enelytics.wpps.persistance.domain.types.msrs.MsrsEnergyTransactionType;
import com.enelytics.wpps.persistance.repositories.msrs.MsrsEnergyTransactionsReportRepository;
import com.enelytics.wpps.persistance.repositories.msrs.MsrsMonthToDateBillingSummaryReportRepository;
import com.enelytics.wpps.persistance.repositories.msrs.MsrsMonthlyBillingStatementReportRepository;

import java.time.YearMonth;
import java.util.Optional;

public class UpdateHelper {

    private static YearMonth START_MONTH = YearMonth.of(2019, 7);

    private UpdateHelper(){
        throw new IllegalStateException("Utility class");
    }

    public static YearMonth getStartMonth(MsrsEnergyTransactionsReportRepository repository, MsrsEnergyTransactionType type) {
        Optional<MsrsEnergyTransactionsReport> firstNotCompleteByType = repository.findFirstNotCompleteByType(type);

        Optional<YearMonth> startMonth = firstNotCompleteByType.map(MsrsReport::getMonth);

        return startMonth.orElse(START_MONTH);
    }

    public static YearMonth getStartMonth(MsrsMonthlyBillingStatementReportRepository repository) {
        Optional<MsrsMonthlyBillingStatementReport> firstNotComplete = repository.findFirstNotComplete();

        Optional<YearMonth> startMonth = firstNotComplete.map(MsrsReport::getMonth);

        return startMonth.orElse(START_MONTH);
    }

    public static YearMonth getStartMonth(MsrsMonthToDateBillingSummaryReportRepository repository) {
        Optional<MsrsMonthToDateBillingSummaryReport> firstNotComplete = repository.findFirstNotComplete();

        Optional<YearMonth> startMonth = firstNotComplete.map(MsrsReport::getMonth);

        return startMonth.orElse(START_MONTH);
    }

}
