package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerApiClient;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerSearchResponse;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.common.PjmPricingNodeIdFetcher;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp.response.LmpResponseItem;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Service
class PjmLmpFetcher {

    private static final DateTimeFormatter DATETIME_FORMAT = DateTimeFormatter.ofPattern("M/d/yyyy%20HH:mm");

    private static final ZoneId EPT = ZoneId.of("US/Eastern");

    private static final String RT_5MIN_URL = "https://api.pjm.com/api/v1/rt_fivemin_hrl_lmps" +
            "?RowCount=25" +
            "&Sort=datetime_beginning_ept" +
            "&Order=Asc" +
            "&StartRow=1" +
            "&IsActiveMetadata=True" +
            "&Fields=congestion_price_rt%2Cdatetime_beginning_ept%2Cdatetime_beginning_utc%2Cequipment%2Cmarginal_loss_price_rt%2Cpnode_id%2Cpnode_name%2Crow_is_current%2Csystem_energy_price_rt%2Ctotal_lmp_rt%2Ctype%2Cversion_nbr%2Cvoltage%2Czone" +
            "&datetime_beginning_ept=${datetime_beginning_ept_start}to${datetime_beginning_ept_end}" +
            "&pnode_id=${pnode_id}";

    private static final String DA_HOURLY_URL = "https://api.pjm.com/api/v1/da_hrl_lmps" +
            "?rowCount=25" +
            "&sort=datetime_beginning_ept" +
            "&startRow=1" +
            "&isActiveMetadata=true" +
            "&fields=congestion_price_da,datetime_beginning_ept,datetime_beginning_utc,equipment,marginal_loss_price_da,pnode_id,pnode_name,row_is_current,system_energy_price_da,total_lmp_da,type,version_nbr,voltage,zone" +
            "&datetime_beginning_ept=${datetime_beginning_ept_start}to${datetime_beginning_ept_end}" +
            "&pnode_id=${pnode_id}";

    private static final Function<Integer, Long> RETRY_POLICY = n -> n > 5 ? null : n * 15_000L;

    private final DataMinerApiClient apiClient;
    private final PjmPricingNodeIdFetcher pricingNodeIdFetcher;
    private final ObjectMapper objectMapper;

    @Autowired
    PjmLmpFetcher(DataMinerApiClient apiClient, PjmPricingNodeIdFetcher pricingNodeIdFetcher) {
        this.apiClient = apiClient;
        this.pricingNodeIdFetcher = pricingNodeIdFetcher;
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    DataMinerSearchResponse<LmpResponseItem> fetchFirstRealTime5MinLmp(PjmArea pjmArea) throws IOException, InvalidApiResponseException {
        LocalDate startDate = LocalDate.of(2018, 1, 1);
        DataMinerSearchResponse<LmpResponseItem> response;

        do {
            LocalDate endDate = startDate.plusMonths(6);
            String url = prepareLmpUrl(RT_5MIN_URL, pjmArea, startDate, endDate);
            response = apiClient.getFirst(url, objectMapper, LmpResponseItem.class, RETRY_POLICY);
            startDate = endDate;
        } while (response.getItems().isEmpty() && startDate.isBefore(LocalDate.now()));

        return response;
    }

    List<DataMinerSearchResponse<LmpResponseItem>> fetchRealTime5MinLmp(PjmArea pjmArea, Instant start, Instant end) throws IOException, InvalidApiResponseException {
        String url = prepareLmpUrl(RT_5MIN_URL, pjmArea, start, end);

        return apiClient.getAll(url, objectMapper, LmpResponseItem.class, RETRY_POLICY);
    }

    DataMinerSearchResponse<LmpResponseItem> fetchFirstDayAheadHourlyLmp(PjmArea pjmArea) throws IOException, InvalidApiResponseException {
        LocalDate startDate = LocalDate.of(2000, 6, 1);
        DataMinerSearchResponse<LmpResponseItem> response;

        do {
            LocalDate endDate = startDate.plusMonths(6);
            String url = prepareLmpUrl(DA_HOURLY_URL, pjmArea, startDate, endDate);
            response = apiClient.getFirst(url, objectMapper, LmpResponseItem.class, RETRY_POLICY);
            startDate = endDate;
        } while (response.getItems().isEmpty() && startDate.isBefore(LocalDate.now()));

        return response;
    }

    List<DataMinerSearchResponse<LmpResponseItem>> fetchDayAheadHourlyLmp(PjmArea pjmArea, Instant start, Instant end) throws IOException, InvalidApiResponseException {
        String url = prepareLmpUrl(DA_HOURLY_URL, pjmArea, start, end);

        return apiClient.getAll(url, objectMapper, LmpResponseItem.class, RETRY_POLICY);
    }

    private String prepareLmpUrl(String url, PjmArea pjmArea, LocalDate startDate, LocalDate endDate) throws IOException, InvalidApiResponseException {
        Instant start = startDate.atStartOfDay(EPT).toInstant();
        Instant end = endDate.atStartOfDay(EPT).toInstant();

        return prepareLmpUrl(url, pjmArea, start, end);
    }

    private String prepareLmpUrl(String url, PjmArea pjmArea, Instant start, Instant end) throws IOException, InvalidApiResponseException {
        Map<String, String> params = toParams(pjmArea, start, end);

        return StringSubstitutor.replace(url, params);
    }

    private Map<String, String> toParams(PjmArea pjmArea, Instant start, Instant end) throws IOException, InvalidApiResponseException {
        long pricingNodeId = pricingNodeIdFetcher.getPricingNodeId(pjmArea);
        LocalDateTime startDateTime = start.atZone(EPT).toLocalDateTime();
        LocalDateTime endDateTime = end.atZone(EPT).toLocalDateTime();

        Map<String, String> params = new HashMap<>();
        params.put("datetime_beginning_ept_start", startDateTime.format(DATETIME_FORMAT));
        params.put("datetime_beginning_ept_end", endDateTime.format(DATETIME_FORMAT));
        params.put("pnode_id", Long.toString(pricingNodeId));

        return params;
    }

}
