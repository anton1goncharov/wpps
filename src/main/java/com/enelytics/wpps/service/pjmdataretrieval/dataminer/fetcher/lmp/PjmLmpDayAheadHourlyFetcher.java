package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLmpType;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerSearchResponse;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp.response.LmpResponseItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Service
public class PjmLmpDayAheadHourlyFetcher {

    private final PjmLmpFetcher pjmLmpFetcher;

    @Autowired
    public PjmLmpDayAheadHourlyFetcher(PjmLmpFetcher pjmLmpFetcher) {
        this.pjmLmpFetcher = pjmLmpFetcher;
    }

    public List<DataMinerSearchResponse<LmpResponseItem>> fetchDayAheadLmp(
            PjmArea pjmArea, Instant start, Instant end) throws IOException, InvalidApiResponseException {
        return pjmLmpFetcher.fetchDayAheadHourlyLmp(pjmArea, start, end);
    }

    public Instant fetchStartDate(PjmArea pjmArea) throws IOException, InvalidApiResponseException {
        DataMinerSearchResponse<LmpResponseItem> response = pjmLmpFetcher.fetchFirstDayAheadHourlyLmp(pjmArea);

        if (response.getItems().isEmpty())
            throw new IllegalStateException("No start date for " + pjmArea + "/" + PjmLmpType.DA_1H);

        LocalDateTime beginningUTC = response.getItems().get(0).getBeginningUTC();

        return beginningUTC.atZone(ZoneOffset.UTC).toInstant();
    }

}

