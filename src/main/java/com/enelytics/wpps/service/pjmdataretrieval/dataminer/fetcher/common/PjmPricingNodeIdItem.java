package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = "pnodeId")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PjmPricingNodeIdItem {

    private long pnodeId;

    private String pnodeName;

    private String pnodeSubtype;

}
