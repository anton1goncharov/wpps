package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.common;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.wellknown.PricingNodeType;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerApiClient;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerSearchResponse;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriUtils;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.join;

@Service
public class PjmPricingNodeIdFetcher {

    private static final Logger logger = LoggerFactory.getLogger(PjmPricingNodeIdFetcher.class);

    private static final String REQUEST_URL = "https://api.pjm.com/api/v1/pnode?rowCount=25&sort=pnode_id&startRow=1&isActiveMetadata=true&fields=effective_date,pnode_id,pnode_name,pnode_subtype,pnode_type,termination_date,voltage_level,zone&termination_date=12/31/9999exact&pnode_name=*{0}*";

    private final DataMinerApiClient apiClient;
    private final ObjectReader reader;

    @Autowired
    public PjmPricingNodeIdFetcher(DataMinerApiClient apiClient) {
        this.apiClient = apiClient;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        reader = objectMapper.readerFor(
                new TypeReference<DataMinerSearchResponse<PjmPricingNodeIdItem>>() {
                }
        );
    }

    @Cacheable(value = "pricingNodeIds", sync = true)
    public long getPricingNodeId(PjmArea pjmArea) throws IOException, InvalidApiResponseException {
        String pricingNodeName = UriUtils.encodePath(pjmArea.getPricingNodeName(), "UTF-8");
        String requestUrl = MessageFormat.format(REQUEST_URL, pricingNodeName);

        logger.info("Fetching {} pricing node ID", pricingNodeName);
        String content = apiClient.get(requestUrl, n -> n > 5 ? null : 1000L * (15 * n));
        DataMinerSearchResponse<PjmPricingNodeIdItem> response = reader.readValue(content);

        if (response.getItems() == null)
            throw new InvalidApiResponseException(join(" ", "Cannot fetch", pricingNodeName, "pricing node ID"));

        List<PjmPricingNodeIdItem> pricingNodeIds = response.getItems().stream()
                .filter(node -> {
                    if (PjmArea.WESTERN_HUB.equals(pjmArea)) {
                        return PricingNodeType.HUB.getType().equals(node.getPnodeSubtype());
                    }

                    return PricingNodeType.ZONE.getType().equals(node.getPnodeSubtype());
                })
                .collect(Collectors.toList());

        if (pricingNodeIds.isEmpty())
            throw new InvalidApiResponseException("Unknown node: " + pricingNodeName);
        if (pricingNodeIds.size() > 1)
            throw new InvalidApiResponseException("Too many results found for " + pricingNodeName);

        long pricingNodeId = pricingNodeIds.get(0).getPnodeId();
        logger.info("{} pnodeId={}", pricingNodeName, pricingNodeId);

        return pricingNodeId;
    }

    @CacheEvict(value = "pricingNodeIds", allEntries = true)
    public void evictPricingNodeIds() {
        logger.info("All pricing node IDs are evicted");
    }

}
