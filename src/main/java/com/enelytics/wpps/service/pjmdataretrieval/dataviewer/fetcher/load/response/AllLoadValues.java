package com.enelytics.wpps.service.pjmdataretrieval.dataviewer.fetcher.load.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class AllLoadValues {

    private List<Series> allSeries;

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Series {
        private String id;
        private String name;
        private List<ChartPoint> data;
    }

}
