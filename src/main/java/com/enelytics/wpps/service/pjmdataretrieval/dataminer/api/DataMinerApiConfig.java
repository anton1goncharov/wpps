package com.enelytics.wpps.service.pjmdataretrieval.dataminer.api;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.*;

@Data
@Component
@ConfigurationProperties(prefix = "service.dataminer.api")
class DataMinerApiConfig {

    private String key;

    private List<String> httpHeaders;

    @Value("0")
    private long nextCallWaitMillis;

    @Value("0")
    private long nextCallJitterMillis;

    @Value("Mozilla/5.0 (Windows NT 10.0; Win64; x64) Chrome/70.0.3538.67")
    private String userAgent;

    public Map<String, String> httpHeadersAsMap() {
        for (String header : httpHeaders) {
            if (!header.contains(":"))
                throw new IllegalArgumentException("Invalid header value (':' is missing)");
        }

        return httpHeaders.stream()
                .collect(toMap(h -> trim(substringBefore(h, ":")), h -> trim(substringAfter(h, ":"))));
    }

}
