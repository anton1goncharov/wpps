package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.load;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerApiClient;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerSearchResponse;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.load.response.SevenDayLoadForecastResponseItem;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Service
public class PjmLoadFetcher {

    private static final Function<Integer, Long> RETRY_POLICY = n -> n > 5 ? null : n * 15_000L;

    private static final String SEVEN_DAY_LOAD_FORECAST_URL = "https://api.pjm.com/api/v1/load_frcstd_7_day" +
            "?rowCount=100" +
            "&startRow=1" +
            "&isActiveMetadata=true" +
            "&fields=evaluated_at_datetime_ept,evaluated_at_datetime_utc,forecast_area,forecast_datetime_beginning_ept,forecast_datetime_beginning_utc,forecast_datetime_ending_ept,forecast_datetime_ending_utc,forecast_load_mw" +
            "&forecast_area=${forecast_area}";

    private final DataMinerApiClient apiClient;
    private final ObjectMapper objectMapper;

    PjmLoadFetcher(DataMinerApiClient apiClient) {
        this.apiClient = apiClient;
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    public List<DataMinerSearchResponse<SevenDayLoadForecastResponseItem>> fetchSevenDayLoadForecast(PjmArea pjmArea) throws IOException {
        String forecastArea = toSevenDayLoadForecastArea(pjmArea);
        Map<String, String> params = new HashMap<>();
        params.put("forecast_area", forecastArea);
        String url = StringSubstitutor.replace(SEVEN_DAY_LOAD_FORECAST_URL, params);

        return apiClient.getAll(url, objectMapper, SevenDayLoadForecastResponseItem.class, RETRY_POLICY);
    }

    private String toSevenDayLoadForecastArea(PjmArea pjmArea) {
        if (pjmArea == PjmArea.PJM_RTO)
            return "RTO_COMBINED";
        if (pjmArea == PjmArea.MIDATL)
            return "MID_ATLANTIC_REGION";
        throw new UnsupportedOperationException("Can't convert area code for " + pjmArea);
    }

}
