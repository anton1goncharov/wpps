package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(of = {"beginningUTC", "pnodeId"})
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class LmpResponseItem {

    @JsonProperty("datetime_beginning_utc")
    private LocalDateTime beginningUTC;

    @JsonProperty("datetime_beginning_ept")
    private LocalDateTime beginningEPT;

    @JsonAlias({"system_energy_price_rt", "system_energy_price_da"})
    private double systemEnergyPrice;

    @JsonAlias({"total_lmp_rt", "total_lmp_da"})
    private BigDecimal totalLmp;

    @JsonAlias({"congestion_price_rt", "congestion_price_da"})
    private BigDecimal congestionPrice;

    @JsonAlias({"marginal_loss_price_rt", "marginal_loss_price_da"})
    private BigDecimal marginalLossPrice;

    @JsonProperty("version_nbr")
    private Long versionNumber;

    private Long pnodeId;
    private String pnodeName;
    private String voltage;
    private String equipment;
    private String type;
    private String zone;
    private Boolean rowIsCurrent;

}
