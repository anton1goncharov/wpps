package com.enelytics.wpps.service.pjmdataretrieval.dataminer.updater.lmp;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLmp;
import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLmp_;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLmpType;
import com.enelytics.wpps.persistance.repositories.pjmdataretrieval.PjmLmpRepository;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerSearchResponse;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp.response.LmpResponseItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import static com.enelytics.wpps.persistance.specifications.pjmdata.PjmLmpSpecification.*;
import static org.springframework.data.domain.Sort.Direction.DESC;

public class UpdateHelper {

    private static final Logger logger = LoggerFactory.getLogger(UpdateHelper.class);

    private UpdateHelper() {
        throw new UnsupportedOperationException("Utility class");
    }

    static <T extends LmpResponseItem> void persist(Collection<DataMinerSearchResponse<T>> dataMinerResults, PjmArea area, PjmLmpType type, PjmLmpRepository pjmLmpRepository) {
        for (DataMinerSearchResponse<? extends LmpResponseItem> response : dataMinerResults) {
            for (LmpResponseItem responseItem : response.getItems()) {
                Date time = Date.from(responseItem.getBeginningUTC().toInstant(ZoneOffset.UTC));
                PjmLmp pjmLmp = new PjmLmp();
                pjmLmp.setTime(time);
                pjmLmp.setArea(area);
                pjmLmp.setType(type);
                pjmLmp.setKwhPrice(responseItem.getTotalLmp().movePointRight(3)); // multiply by 1000 to get MWh = > kWh

                Specification<PjmLmp> specification = Specification.where(withTime(pjmLmp.getTime()))
                        .and(withArea(pjmLmp.getArea()))
                        .and(withType(pjmLmp.getType()));
                PjmLmp existing = pjmLmpRepository.findAll(specification).stream()
                        .findFirst()
                        .orElse(null);

                if (existing == null) {
                    logger.info("Adding new pjm_lmp record: {}", pjmLmp);
                    pjmLmpRepository.save(pjmLmp);
                } else if (existing.getKwhPrice().compareTo(pjmLmp.getKwhPrice()) != 0) {
                    if (logger.isWarnEnabled()) {
                        logger.warn("Updating pjm_lmp.kwh_price {} => {}; existing record: {}",
                                (existing.getKwhPrice() == null ? null : existing.getKwhPrice().stripTrailingZeros().toPlainString()),
                                (pjmLmp.getKwhPrice() == null ? null : pjmLmp.getKwhPrice().stripTrailingZeros().toPlainString()),
                                existing);
                    }
                    existing.setKwhPrice(pjmLmp.getKwhPrice());
                    pjmLmpRepository.save(existing);
                }
            }
        }
    }

    static Optional<PjmLmp> findLatest(PjmArea area, PjmLmpType type, PjmLmpRepository pjmLmpRepository) {
        PageRequest pageRequest = PageRequest.of(1, 1, new Sort(DESC, PjmLmp_.time.getName()));
        Specification<PjmLmp> specification = Specification.where(withArea(area))
                .and(withType(type));

        Page<PjmLmp> page = pjmLmpRepository.findAll(specification, pageRequest);

        if (page.getNumberOfElements() == 0)
            return Optional.empty();

        return Optional.of(page.getContent().get(0));
    }

}
