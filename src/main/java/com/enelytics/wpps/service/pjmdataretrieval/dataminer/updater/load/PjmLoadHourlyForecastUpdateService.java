package com.enelytics.wpps.service.pjmdataretrieval.dataminer.updater.load;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLoad;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLoadType;
import com.enelytics.wpps.persistance.repositories.pjmdataretrieval.PjmLoadRepository;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerSearchResponse;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.load.PjmLoadFetcher;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.load.response.SevenDayLoadForecastResponseItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.enelytics.wpps.persistance.specifications.pjmdata.PjmLoadSpecification.*;

@Service
public class PjmLoadHourlyForecastUpdateService {

    private static final Logger logger = LoggerFactory.getLogger(PjmLoadHourlyForecastUpdateService.class);

    private final PjmLoadRepository pjmLoadRepository;
    private final PjmLoadFetcher pjmLoadFetcher;
    private final Lock lock = new ReentrantLock();

    public PjmLoadHourlyForecastUpdateService(
            PjmLoadRepository pjmLoadRepository,
            PjmLoadFetcher pjmLoadFetcher) {
        this.pjmLoadRepository = pjmLoadRepository;
        this.pjmLoadFetcher = pjmLoadFetcher;
    }

    @Transactional
    public void update() throws IOException {
        if (lock.tryLock()) {
            try {
                updateForecast();
            } finally {
                lock.unlock();
            }
        } else {
            logger.warn("Cowardly refusing to run another update simultaneously");
        }
    }

    private void updateForecast() throws IOException {
        final List<PjmArea> areasToUpdate = Arrays.asList(PjmArea.PJM_RTO, PjmArea.MIDATL);

        for (PjmArea pjmArea : areasToUpdate) {
            updateForecast(pjmArea);
        }
    }

    private void updateForecast(PjmArea pjmArea) throws IOException {
        List<DataMinerSearchResponse<SevenDayLoadForecastResponseItem>> responses = pjmLoadFetcher.fetchSevenDayLoadForecast(pjmArea);
        persist(responses, pjmArea);
        logger.info("PjmLoad {}/{} has been updated", PjmLoadType.F_H, pjmArea);
    }

    private void persist(List<DataMinerSearchResponse<SevenDayLoadForecastResponseItem>> dataMinerResults, PjmArea pjmArea) {
        for (DataMinerSearchResponse<SevenDayLoadForecastResponseItem> response : dataMinerResults) {
            for (SevenDayLoadForecastResponseItem responseItem : response.getItems()) {
                Date time = Date.from(responseItem.getForecastDatetimeBeginningUtc().toInstant(ZoneOffset.UTC));
                PjmLoad pjmLoad = new PjmLoad();
                pjmLoad.setTime(time);
                pjmLoad.setArea(pjmArea);
                pjmLoad.setType(PjmLoadType.F_H);
                pjmLoad.setLoadKw(responseItem.getForecastLoadMw().movePointRight(3));
                // multiply by 1000 to get MWh = > kWh
                updateWith(pjmLoad);
            }
        }
    }

    private void updateWith(PjmLoad pjmLoad) {
        Specification<PjmLoad> specification = Specification.where(withTime(pjmLoad.getTime()))
                .and(withArea(pjmLoad.getArea()))
                .and(withType(pjmLoad.getType()));
        PjmLoad existing = pjmLoadRepository.findAll(specification).stream()
                .findFirst()
                .orElse(null);

        if (existing == null) {
            logger.info("Adding new pjm_load record: {}", pjmLoad);
            pjmLoadRepository.save(pjmLoad);
        } else if (existing.getLoadKw().compareTo(pjmLoad.getLoadKw()) != 0) {
            if (logger.isWarnEnabled()) {
                logger.warn("Updating pjm_load.load_kw {} => {}; existing record: {}",
                        format(existing.getLoadKw()),
                        format(pjmLoad.getLoadKw()),
                        existing);
            }

            existing.setLoadKw(pjmLoad.getLoadKw());
            pjmLoadRepository.save(existing);
        }
    }

    private static String format(BigDecimal val) {
        if (val == null) {
            return "null";
        } else {
            return val.stripTrailingZeros().toPlainString();
        }
    }

}
