package com.enelytics.wpps.service.pjmdataretrieval.dataviewer.fetcher.load.response;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ChartPoint {

    private long x;
    private BigDecimal y;

    public Date date() {
        return new Date(x);
    }

    public BigDecimal value() {
        return y;
    }

}
