package com.enelytics.wpps.service.pjmdataretrieval.dataviewer.fetcher.load;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.service.pjmdataretrieval.dataviewer.InvalidDataException;
import com.enelytics.wpps.service.pjmdataretrieval.dataviewer.fetcher.load.response.AllLoadValues;
import com.enelytics.wpps.service.pjmdataretrieval.dataviewer.fetcher.load.response.LoadChartData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

public class PjmLoadDataViewerFetcher {

    private static final String DATAVIEWER_LOAD_URL = "https://dataviewer.pjm.com/dataviewer/pages/public/load.jsf";

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private static final DateTimeFormatter INPUT_DATE_FORMAT = DateTimeFormatter.ofPattern("M.d.y");

    private int dateShift = 0;
    private PjmArea pjmArea = null;
    private CloseableHttpClient httpClient = null;

    public synchronized AllLoadValues fetch(int dateShift, PjmArea pjmArea) throws IOException, InvalidDataException {
        if (this.pjmArea != null)
            throw new IllegalStateException("This fetcher can be used only once; create another instance");

        if (dateShift < -1 || dateShift > 1)
            throw new IllegalArgumentException("dateShift must be one of -1, 0, 1");

        this.dateShift = dateShift;
        this.pjmArea = requireNonNull(pjmArea, "pjmArea");

        try {
            httpClient = newHttpClient();
            return fetch();
        } finally {
            if (httpClient != null)
                httpClient.close();
        }
    }

    private AllLoadValues fetch() throws IOException, InvalidDataException {
        HttpGet request = new HttpGet(DATAVIEWER_LOAD_URL);
        HttpResponse response = httpClient.execute(request);
        String content = getStringContent(response);

        Document document = Jsoup.parse(content);
        Element menuForm = document.selectFirst("form[name=menuForm]");
        List<Element> inputs = selectInputs(menuForm);

        return step01DoMenuChange(inputs);
    }

    private AllLoadValues step01DoMenuChange(List<Element> inputs) throws IOException, InvalidDataException {
        HttpPost request = new HttpPost(DATAVIEWER_LOAD_URL);

        List<NameValuePair> form = new ArrayList<>();
        inputs.forEach(input -> form.add(new BasicNameValuePair(input.attr("name"), input.val())));
        form.add(new BasicNameValuePair("menuForm:lmpMenuId_load", "menuForm:lmpMenuId_load"));

        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, StandardCharsets.UTF_8);
        request.setEntity(entity);

        httpClient.execute(request);

        return step02AcceptMenuChange();
    }

    private AllLoadValues step02AcceptMenuChange() throws IOException, InvalidDataException {
        HttpGet request = new HttpGet(DATAVIEWER_LOAD_URL);
        HttpResponse response = httpClient.execute(request);
        String content = getStringContent(response);

        Document document = Jsoup.parse(content);
        Elements forms = document.select("div#rightSidePanel_content form[name^=j_]");
        Elements scripts = document.select(".lowc > script:not([src])");

        if (forms.size() != 1) {
            throw new IOException("Bad data (no form found or too many forms)");
        }

        if (scripts.size() != 1)
            throw new InvalidDataException("Bad data (no script or too many scripts)");

        return step03RequestTabPanelContent(forms.first(), scripts.first());
    }

    private AllLoadValues step03RequestTabPanelContent(Element chartForm, Element script) throws IOException, InvalidDataException {
        HttpPost request = new HttpPost(DATAVIEWER_LOAD_URL);

        List<NameValuePair> form = new ArrayList<>();
        form.add(new BasicNameValuePair("javax.faces.partial.ajax", "true"));
        form.add(new BasicNameValuePair("javax.faces.partial.execute", "@all"));
        form.add(new BasicNameValuePair("javax.faces.partial.render", "tabPanel"));

        for (Element input : chartForm.select("input")) {
            form.add(new BasicNameValuePair(input.attr("name"), input.val()));
        }

        String chartFormId = chartForm.id();
        String javaxFacesSourceId = getJavaxFacesSourceId(script, chartFormId);

        if (StringUtils.isBlank(javaxFacesSourceId)) {
            throw new InvalidDataException("Couldn't get javax.faces.source ID");
        }

        form.add(new BasicNameValuePair("javax.faces.source", javaxFacesSourceId));
        form.add(new BasicNameValuePair(javaxFacesSourceId, javaxFacesSourceId));

        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, StandardCharsets.UTF_8);
        request.setEntity(entity);
        HttpResponse response = httpClient.execute(request);
        String content = getStringContent(response);

        Document document = Jsoup.parse(content);
        Element updateTabPanel = document.selectFirst("update#tabPanel");

        if (updateTabPanel == null) {
            throw new InvalidDataException("tabPanel section not found");
        }

        String partialResponseId = document.selectFirst("partial-response[id]").id();
        Element viewState = document.selectFirst("update[id^=" + partialResponseId + ":javax.faces.ViewState]");

        if (viewState == null)
            throw new InvalidDataException("javax.faces.ViewState section not found");

        String nextViewState = step04ChangeDateValue(updateTabPanel.text(), viewState.text());

        return step04ChangeAreaValue(updateTabPanel.text(), nextViewState);
    }

    private String getJavaxFacesSourceId(Element script, String chartFormId) {
        String html = script.html();
        Pattern pattern = Pattern.compile("\"(" + chartFormId + ":.*?)\"");
        Matcher matcher = pattern.matcher(html);
        String id = null;

        if (matcher.find()) {
            id = matcher.group(1);
        }

        return id;
    }

    private AllLoadValues step04ChangeAreaValue(String tabPanelContent, String viewState) throws InvalidDataException, IOException {
        Document tabPanelDoc = Jsoup.parse(tabPanelContent);
        Element chartForm = tabPanelDoc.selectFirst("form");

        if (chartForm == null)
            throw new InvalidDataException("form not found");

        String chartFormName = chartForm.attr("name");
        HttpPost request = new HttpPost(DATAVIEWER_LOAD_URL);
        Elements areaSelectorList = tabPanelDoc.select("td > div[id^=" + chartFormName + ":]");

        if (areaSelectorList.isEmpty())
            throw new InvalidDataException("area selector not found");
        if (areaSelectorList.size() > 1)
            throw new InvalidDataException("too many area selectors found");

        String elementId = areaSelectorList.first().id();

        List<NameValuePair> form = new ArrayList<>();
        form.add(new BasicNameValuePair("javax.faces.partial.ajax", "true"));
        form.add(new BasicNameValuePair("javax.faces.source", elementId));
        form.add(new BasicNameValuePair("javax.faces.partial.execute", elementId));
        form.add(new BasicNameValuePair("javax.faces.partial.render", chartFormName + ":valuesPanel"));
        form.add(new BasicNameValuePair("javax.faces.behavior.event", "valueChange"));
        form.add(new BasicNameValuePair("javax.faces.partial.event", "change"));

        // Change location
        form = new ArrayList<>();
        form.add(new BasicNameValuePair("javax.faces.partial.ajax", "true"));
        form.add(new BasicNameValuePair("javax.faces.source", elementId));
        form.add(new BasicNameValuePair("javax.faces.partial.execute", elementId));
        form.add(new BasicNameValuePair("javax.faces.partial.render", chartFormName + ":valuesPanel"));
        form.add(new BasicNameValuePair("javax.faces.behavior.event", "valueChange"));
        form.add(new BasicNameValuePair("javax.faces.partial.event", "change"));
        form.add(new BasicNameValuePair(elementId + "_input", areaToInput(pjmArea)));
        form.add(new BasicNameValuePair("javax.faces.ViewState", viewState));

        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, StandardCharsets.UTF_8);
        request.setEntity(entity);
        HttpResponse httpResponse = httpClient.execute(request);
        String content = getStringContent(httpResponse);

        return step05ParseChartData(content);
    }

    private String step04ChangeDateValue(String tabPanelContent, String viewState) throws InvalidDataException, IOException {
        Document tabPanelDoc = Jsoup.parse(tabPanelContent);
        Element chartForm = tabPanelDoc.selectFirst("form");

        if (chartForm == null)
            throw new InvalidDataException("form not found");

        String chartFormName = chartForm.attr("name");
        String elementId = chartFormName + ":dateSelection";

        List<NameValuePair> form = new ArrayList<>();
        form.add(new BasicNameValuePair("javax.faces.partial.ajax", "true"));
        form.add(new BasicNameValuePair("javax.faces.source", elementId));
        form.add(new BasicNameValuePair("javax.faces.partial.execute", elementId));
        form.add(new BasicNameValuePair("javax.faces.behavior.event", "valueChange"));
        form.add(new BasicNameValuePair("javax.faces.partial.event", "change"));

        for (Element input : selectInputs(chartForm)) {
            String name = input.attr("name");
            String value = input.val();
            // Set area filter value
            if (name.equals(elementId)) {
                String shiftedDay = dateToInput(value, dateShift);
                form.add(new BasicNameValuePair(name, shiftedDay));
            }
        }
        form.add(new BasicNameValuePair("javax.faces.ViewState", viewState));

        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, StandardCharsets.UTF_8);
        HttpPost request = new HttpPost(DATAVIEWER_LOAD_URL);
        request.setEntity(entity);
        HttpResponse httpResponse = httpClient.execute(request);
        String content = getStringContent(httpResponse);

        Document document = Jsoup.parse(content);
        String partialResponseId = document.selectFirst("partial-response[id]").id();
        Element nextViewState = document.selectFirst("update[id^=" + partialResponseId + ":javax.faces.ViewState]");

        if (nextViewState == null)
            throw new InvalidDataException("next javax.faces.ViewState not found");

        return nextViewState.text();
    }

    private AllLoadValues step05ParseChartData(String partialResponseContent) throws InvalidDataException {
        Document document = Jsoup.parse(partialResponseContent);
        Elements extensions = document.select("extension");

        if (extensions.size() != 1)
            throw new InvalidDataException("Bad data (none or too many extensions found)");

        Element extension = extensions.first();
        String chartJson = extension.text();
        LoadChartData loadChartData;
        AllLoadValues allLoadValues;

        try {
            loadChartData = objectMapper.readValue(chartJson, LoadChartData.class);
        } catch (IOException e) {
            throw new InvalidDataException("Can't parse load chart json", e);
        }

        try {
            allLoadValues = objectMapper.readValue(loadChartData.getAllLoadValues(), AllLoadValues.class);
        } catch (IOException e) {
            throw new InvalidDataException("Can't parse allLoadValues json", e);
        }

        return allLoadValues;
    }

    private String dateToInput(String currentDate, int daysToAdd) {
        LocalDate date = LocalDate.parse(currentDate, INPUT_DATE_FORMAT);
        LocalDate shiftedDate = date.plusDays(daysToAdd);

        return shiftedDate.format(INPUT_DATE_FORMAT);
    }

    private String areaToInput(PjmArea pjmArea) {
        String result;

        switch (pjmArea) {
            case PJM_RTO:
                result = "PJM RTO Total";
                break;
            case MIDATL:
                result = "Mid-Atlantic Region";
                break;
            case APS:
                result = "APS Zone";
                break;
            case BGE:
                result = "BC Zone";
                break;
            case PEPCO:
                result = "PEP Zone";
                break;
            default:
                throw new IllegalStateException("Unknown pjmArea: " + pjmArea);
        }

        return result;
    }

    private String getStringContent(HttpResponse response) throws IOException {
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
            throw new IOException(response.getStatusLine().getReasonPhrase());

        try (StringWriter sw = new StringWriter();
             WriterOutputStream outputStream = new WriterOutputStream(sw, StandardCharsets.UTF_8)) {
            response.getEntity().writeTo(outputStream);
            outputStream.flush();

            return sw.toString();
        }
    }

    private static CloseableHttpClient newHttpClient() {
        SocketConfig socketConfig = SocketConfig.custom()
                .setSoTimeout(30_000)
                .build();
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(30_000)
                .build();

        return HttpClientBuilder.create()
                .setUserAgent(USER_AGENT)
                .setDefaultSocketConfig(socketConfig)
                .setDefaultRequestConfig(requestConfig)
                .setDefaultCookieStore(new BasicCookieStore())
                .build();
    }

    private static List<Element> selectInputs(Element form) {
        List<Element> result = new ArrayList<>();

        for (Element input : form.select("input")) {
            if (input.is("input[type=radio]")) {
                if (input.is("input[checked]")) {
                    result.add(input);
                }
            } else {
                result.add(input);
            }
        }

        return result;
    }

}
