package com.enelytics.wpps.service.pjmdataretrieval.dataviewer.updater.load;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLoad;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLoadType;
import com.enelytics.wpps.persistance.repositories.pjmdataretrieval.PjmLoadRepository;
import com.enelytics.wpps.service.pjmdataretrieval.dataviewer.InvalidDataException;
import com.enelytics.wpps.service.pjmdataretrieval.dataviewer.fetcher.load.PjmLoadDataViewerFetcher;
import com.enelytics.wpps.service.pjmdataretrieval.dataviewer.fetcher.load.response.AllLoadValues;
import com.enelytics.wpps.service.pjmdataretrieval.dataviewer.fetcher.load.response.ChartPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.enelytics.wpps.persistance.specifications.pjmdata.PjmLoadSpecification.*;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

/**
 * Updates pjm_load Instantaneous values.
 * Data is taken from PJM Data Viewer Load charts.
 */
@Service
public class PjmLoadDataViewerUpdateService {

    private static final Logger logger = LoggerFactory.getLogger(PjmLoadDataViewerUpdateService.class);

    private final PjmLoadRepository pjmLoadRepository;
    private final Lock lock = new ReentrantLock();

    public PjmLoadDataViewerUpdateService(PjmLoadRepository pjmLoadRepository) {
        this.pjmLoadRepository = pjmLoadRepository;
    }

    @Transactional
    public void update() throws IOException, InvalidDataException {
        if (lock.tryLock()) {
            try {
                updateInstantaneous();
            } finally {
                lock.unlock();
            }
        } else {
            logger.warn("Cowardly refusing to run another update simultaneously");
        }
    }

    private void updateInstantaneous() throws IOException, InvalidDataException {
        List<PjmArea> areas = asList(
                PjmArea.PJM_RTO,
                PjmArea.MIDATL,
                PjmArea.APS,
                PjmArea.BGE,
                PjmArea.PEPCO
        );

        for (PjmArea area : areas) {
            update(-1, area);
            update(0, area);
        }
    }

    private void update(int daysShift, PjmArea pjmArea) throws IOException, InvalidDataException {
        PjmLoadDataViewerFetcher pjmLoadDataViewerFetcher = new PjmLoadDataViewerFetcher();
        AllLoadValues allLoadValues = pjmLoadDataViewerFetcher.fetch(daysShift, pjmArea);
        updateInstantaneous(allLoadValues, pjmArea);
        logger.info("Updated PjmLoad Instantaneous area={} daysShift={}", pjmArea, daysShift);
    }

    private void updateInstantaneous(AllLoadValues allLoadValues, PjmArea pjmArea) {
        List<AllLoadValues.Series> series = allLoadValues.getAllSeries().stream()
                .filter(s -> "Instantaneous".equals(s.getId()))
                .collect(toList());
        if (series.size() != 1)
            throw new IllegalStateException("Instantaneous series");

        for (ChartPoint chartPoint : series.get(0).getData()) {
            PjmLoad pjmLoad = new PjmLoad();
            pjmLoad.setTime(chartPoint.date());
            pjmLoad.setArea(pjmArea);
            pjmLoad.setType(PjmLoadType.I_5M);
            pjmLoad.setLoadKw(chartPoint.value().movePointRight(3)); // multiply by 1000 for MWh => kWh
            updateWith(pjmLoad);
        }
    }

    private void updateWith(PjmLoad pjmLoad) {
        Specification<PjmLoad> specification = Specification.where(withTime(pjmLoad.getTime()))
                .and(withArea(pjmLoad.getArea()))
                .and(withType(pjmLoad.getType()));
        PjmLoad existing = pjmLoadRepository.findAll(specification).stream()
                .findFirst()
                .orElse(null);

        if (existing == null) {
            logger.info("ADD pjm_load record: {}", pjmLoad);
            pjmLoadRepository.save(pjmLoad);
        } else if (existing.getLoadKw().compareTo(pjmLoad.getLoadKw()) != 0) {
            if (logger.isWarnEnabled()) {
                logger.warn("UPDATE pjm_load.load_kw {} => {}; existing record: {}",
                        format(existing.getLoadKw()),
                        format(pjmLoad.getLoadKw()),
                        existing);
            }
            existing.setLoadKw(pjmLoad.getLoadKw());
            pjmLoadRepository.save(existing);
        }
    }

    private static String format(BigDecimal val) {
        return val == null ? "null" : val.stripTrailingZeros().toPlainString();
    }

}
