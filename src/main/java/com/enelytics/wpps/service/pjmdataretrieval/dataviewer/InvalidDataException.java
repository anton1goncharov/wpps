package com.enelytics.wpps.service.pjmdataretrieval.dataviewer;

/**
 * Exception for malformed API/web responses.
 */
public class InvalidDataException extends Exception {

    private static final long serialVersionUID = 1L;

    public InvalidDataException(String message) {
        super(message);
    }

    public InvalidDataException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
