package com.enelytics.wpps.service.pjmdataretrieval.dataminer.api;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;

public interface DataMinerApiClient {

    String get(String url) throws IOException;

    String get(String url, Function<Integer, Long> retryPeriodProvider) throws IOException;

    <T> DataMinerSearchResponse<T> getFirst(String url, ObjectMapper objectMapper, Class<T> responseItemClass, Function<Integer, Long> retryPeriodProvider) throws IOException;

    <T> List<DataMinerSearchResponse<T>> getAll(String url, ObjectMapper objectMapper, Class<T> responseItemClass) throws IOException;

    <T> List<DataMinerSearchResponse<T>> getAll(String url, ObjectMapper objectMapper, Class<T> responseItemClass, Function<Integer, Long> retryPeriodProvider) throws IOException;

}
