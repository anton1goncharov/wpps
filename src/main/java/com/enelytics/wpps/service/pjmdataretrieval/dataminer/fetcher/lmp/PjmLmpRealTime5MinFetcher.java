package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLmpType;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerSearchResponse;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp.response.LmpResponseItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Service
public class PjmLmpRealTime5MinFetcher {

    private final PjmLmpFetcher pjmLmpFetcher;

    @Autowired
    public PjmLmpRealTime5MinFetcher(PjmLmpFetcher pjmLmpFetcher) {
        this.pjmLmpFetcher = pjmLmpFetcher;
    }

    public List<DataMinerSearchResponse<LmpResponseItem>> fetchRealTime5MinLmp(
            PjmArea pjmArea, Instant start, Instant end) throws IOException, InvalidApiResponseException {
        return pjmLmpFetcher.fetchRealTime5MinLmp(pjmArea, start, end);
    }

    public Instant fetchStartDate(PjmArea pjmArea) throws IOException, InvalidApiResponseException {
        DataMinerSearchResponse<LmpResponseItem> response = pjmLmpFetcher.fetchFirstRealTime5MinLmp(pjmArea);

        if (response.getItems().isEmpty())
            throw new IllegalStateException("No start date for " + pjmArea + "/" + PjmLmpType.RT_5M);

        LocalDateTime beginningUTC = response.getItems().get(0).getBeginningUTC();

        return beginningUTC.atZone(ZoneOffset.UTC).toInstant();
    }

}

