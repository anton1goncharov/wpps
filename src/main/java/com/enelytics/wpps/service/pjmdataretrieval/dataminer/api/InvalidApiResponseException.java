package com.enelytics.wpps.service.pjmdataretrieval.dataminer.api;

public class InvalidApiResponseException extends Exception {

    private static final long serialVersionUID = 1L;

    public InvalidApiResponseException() {
        super("Unexpected result has been returned by Data Miner");
    }

    public InvalidApiResponseException(String message) {
        super(message);
    }

}
