package com.enelytics.wpps.service.pjmdataretrieval.dataviewer.fetcher.load.response;

import lombok.Data;

@Data
public class LoadChartData {

    private long daLastUpdatedTime;
    private String allLoadValues;
    private String latestLoadValue;

}
