package com.enelytics.wpps.service.pjmdataretrieval.dataminer.api;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class DataMinerApiClientImpl implements DataMinerApiClient {

    private static final Logger logger = LoggerFactory.getLogger(DataMinerApiClientImpl.class);

    private final Object lastCallTimeSync = new Object();

    private final DataMinerApiConfig dataMinerApiConfig;

    private long lastCallTime = 0;

    public DataMinerApiClientImpl(DataMinerApiConfig dataMinerApiConfig) {
        this.dataMinerApiConfig = dataMinerApiConfig;
    }

    @Override
    public String get(String url) throws IOException {
        try (CloseableHttpClient httpClient = newHttpClient(
        )) {
            logger.info("GET {}", url);
            HttpUriRequest request = new HttpGet(url);
            waitBeforeNextCallAllowed();
            HttpResponse httpResponse = httpClient.execute(request);
            String content;

            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new IOException(String.format("Failed to retrieve '%s'; HTTP Status: %s; HTTP Status Reason: %s",
                        url, httpResponse.getStatusLine().getStatusCode(), httpResponse.getStatusLine().getReasonPhrase()));
            }


            try (InputStream in = httpResponse.getEntity().getContent()) {
                content = IOUtils.toString(in, StandardCharsets.UTF_8);
            }

            return content;
        }
    }

    @Override
    public String get(String url, Function<Integer, Long> retryPeriodProvider) throws IOException {
        for (int i = 0; ; i++) {
            try {
                return get(url);
            } catch (IOException e) {
                Long sleepMillis = retryPeriodProvider.apply(i + 1);

                if (sleepMillis == null)
                    throw new IOException("Failed to request " + url, e);
                logger.warn("Failed to complete API call, sleeping for {} ms before retry", sleepMillis);

                try {
                    Thread.sleep(sleepMillis);
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    @Override
    public <T> DataMinerSearchResponse<T> getFirst(String url, ObjectMapper objectMapper, Class<T> responseItemClass, Function<Integer, Long> retryPeriodProvider) throws IOException {
        JavaType javaType = objectMapper.getTypeFactory()
                .constructParametricType(DataMinerSearchResponse.class, responseItemClass);
        String content = get(url, retryPeriodProvider);

        return objectMapper.readValue(content, javaType);
    }

    @Override
    public <T> List<DataMinerSearchResponse<T>> getAll(String url, ObjectMapper objectMapper, Class<T> responseItemClass) throws IOException {
        return getAll(url, objectMapper, responseItemClass, n -> null);
    }

    @Override
    public <T> List<DataMinerSearchResponse<T>> getAll(String url, ObjectMapper objectMapper, Class<T> responseItemClass, Function<Integer, Long> retryPeriodProvider) throws IOException {
        JavaType javaType = objectMapper.getTypeFactory()
                .constructParametricType(DataMinerSearchResponse.class, responseItemClass);
        List<DataMinerSearchResponse<T>> result = new ArrayList<>();
        String nextURL = url;

        while (nextURL != null) {
            String content = get(nextURL, retryPeriodProvider);
            DataMinerSearchResponse<T> response = objectMapper.readValue(content, javaType);
            result.add(response);
            Optional<DataMinerSearchResponse.Link> nextLink = response.getNext();
            nextURL = nextLink.map(DataMinerSearchResponse.Link::getHref).orElse(null);
        }

        return result;
    }

    private void waitBeforeNextCallAllowed() {
        synchronized (lastCallTimeSync) {
            long randWait = (long) (dataMinerApiConfig.getNextCallJitterMillis() * Math.random());
            long nextCallTime = lastCallTime + dataMinerApiConfig.getNextCallWaitMillis() + randWait;
            long wait = nextCallTime - System.currentTimeMillis();

            while (wait > 0) {
                try {
                    lastCallTimeSync.wait(wait);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                wait = nextCallTime - System.currentTimeMillis();
            }

            lastCallTime = System.currentTimeMillis();
        }
    }

    private CloseableHttpClient newHttpClient() {
        List<Header> headers = dataMinerApiConfig.httpHeadersAsMap().entrySet().stream()
                .map(e -> new BasicHeader(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        SocketConfig socketConfig = SocketConfig.custom()
                .setSoTimeout(30_000)
                .build();
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(30_000)
                .build();

        return HttpClientBuilder.create()
                .setUserAgent(dataMinerApiConfig.getUserAgent())
                .setDefaultHeaders(headers)
                .setDefaultSocketConfig(socketConfig)
                .setDefaultRequestConfig(requestConfig)
                .build();
    }

}
