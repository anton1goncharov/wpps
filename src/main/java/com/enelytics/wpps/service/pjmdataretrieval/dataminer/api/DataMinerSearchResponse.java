package com.enelytics.wpps.service.pjmdataretrieval.dataminer.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Optional;

@Data
@JsonIgnoreProperties(value = { "searchSpecification", "totalRows" })
public class DataMinerSearchResponse<T> {

    private List<T> items;
    private List<Link> links;

    @Data
    public static class Link {
        private Rel rel;
        private String href;

        public enum Rel {
            @JsonProperty("self")
            SELF,

            @JsonProperty("next")
            NEXT,

            @JsonProperty("previous")
            PREV,

            @JsonProperty("metadata")
            META
        }
    }

    public Optional<Link> getNext() {
        return links.stream()
                .filter(link -> Link.Rel.NEXT == link.getRel())
                .findFirst();
    }

    public Link getSelf() {
        return links.stream()
                .filter(link -> Link.Rel.SELF == link.getRel())
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Self link not found"));
    }

}
