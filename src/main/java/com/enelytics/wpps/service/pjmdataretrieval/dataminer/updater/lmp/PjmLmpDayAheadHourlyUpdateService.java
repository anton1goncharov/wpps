package com.enelytics.wpps.service.pjmdataretrieval.dataminer.updater.lmp;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLmp;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLmpType;
import com.enelytics.wpps.persistance.repositories.pjmdataretrieval.PjmLmpRepository;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerSearchResponse;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp.PjmLmpDayAheadHourlyFetcher;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp.response.LmpResponseItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class PjmLmpDayAheadHourlyUpdateService {

    private static final Logger logger = LoggerFactory.getLogger(PjmLmpDayAheadHourlyUpdateService.class);

    private final PjmLmpRepository pjmLmpRepository;
    private final PjmLmpDayAheadHourlyFetcher pjmLmpDayAheadHourlyFetcher;
    private final Lock lock = new ReentrantLock();

    public PjmLmpDayAheadHourlyUpdateService(
            PjmLmpRepository pjmLmpRepository,
            PjmLmpDayAheadHourlyFetcher pjmLmpDayAheadHourlyFetcher) {
        this.pjmLmpRepository = pjmLmpRepository;
        this.pjmLmpDayAheadHourlyFetcher = pjmLmpDayAheadHourlyFetcher;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean updateInDedicatedTransaction() throws IOException, InvalidApiResponseException {
        return update();
    }

    /**
     * Update pjm_lmp day ahead data using Data Miner 2 data
     *
     * @return true if everything is updated, false otherwise
     *
     * @throws IOException network exception occurred
     */
    @Transactional
    public boolean update() throws IOException, InvalidApiResponseException {
        if (lock.tryLock()) {
            try {
                return updateDayAhead();
            } finally {
                lock.unlock();
            }
        } else {
            logger.warn("Cowardly refusing to run another update simultaneously");
            return false;
        }
    }

    private boolean updateDayAhead() throws IOException, InvalidApiResponseException {
        final List<PjmArea> areasToUpdate = Arrays.asList(
                PjmArea.PJM_RTO,
                PjmArea.MIDATL,
                PjmArea.APS,
                PjmArea.BGE,
                PjmArea.PEPCO,
                PjmArea.WESTERN_HUB
        );
        boolean isEverythingUpdatedUntilNow = true;

        for (PjmArea pjmArea : areasToUpdate) {
            isEverythingUpdatedUntilNow &= updateDayAhead(pjmArea);
        }

        return isEverythingUpdatedUntilNow;
    }

    private boolean updateDayAhead(PjmArea pjmArea) throws IOException, InvalidApiResponseException {
        Instant start = resolveStartDate(pjmArea);
        Instant end = start.plus(7, ChronoUnit.DAYS);
        boolean updatedTillNow = Instant.now().isBefore(end);

        if (updatedTillNow) {
            end = Instant.now().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.HOURS);
        }

        List<DataMinerSearchResponse<LmpResponseItem>> dataMinerResults = pjmLmpDayAheadHourlyFetcher.fetchDayAheadLmp(pjmArea, start, end);
        UpdateHelper.persist(dataMinerResults, pjmArea, PjmLmpType.DA_1H, pjmLmpRepository);
        logger.info("PJM Day-Ahead Hourly for {} updated in range {} .. {}", pjmArea.getPricingNodeName(), start, end);

        return updatedTillNow;
    }

    private Instant resolveStartDate(PjmArea pjmArea) throws IOException, InvalidApiResponseException {
        Optional<PjmLmp> latest = UpdateHelper.findLatest(pjmArea, PjmLmpType.DA_1H, pjmLmpRepository);

        if (latest.isPresent()) {
            return latest.get().getTime().toInstant();
        }

        return pjmLmpDayAheadHourlyFetcher.fetchStartDate(pjmArea);
    }

}

