package com.enelytics.wpps.service.pjmdataretrieval.dataminer.updater.common;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.common.PjmPricingNodeIdFetcher;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class PjmPricingNodeIdUpdateService {

    private final PjmPricingNodeIdFetcher pjmPricingNodeIdFetcher;

    public PjmPricingNodeIdUpdateService(PjmPricingNodeIdFetcher pjmPricingNodeIdFetcher) {
        this.pjmPricingNodeIdFetcher = pjmPricingNodeIdFetcher;
    }

    public void updatePricingNodeIds() throws IOException, InvalidApiResponseException {
        // Clear cached pricing node IDs
        pjmPricingNodeIdFetcher.evictPricingNodeIds();

        final List<PjmArea> pjmAreas = Arrays.asList(
                PjmArea.PJM_RTO,
                PjmArea.MIDATL,
                PjmArea.APS,
                PjmArea.BGE,
                PjmArea.PEPCO,
                PjmArea.WESTERN_HUB
        );

        for (PjmArea pjmArea : pjmAreas) {
            pjmPricingNodeIdFetcher.getPricingNodeId(pjmArea);
        }
    }

}
