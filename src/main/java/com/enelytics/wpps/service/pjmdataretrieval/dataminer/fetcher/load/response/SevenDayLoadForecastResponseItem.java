package com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.load.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Response for Seven-Day Load Forecast
 * @see  <a href="https://dataminer2.pjm.com/feed/load_frcstd_7_day">Seven-Day Load Forecast</a>
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SevenDayLoadForecastResponseItem {

    private LocalDateTime evaluatedAtDatetimeUtc;
    private LocalDateTime evaluatedAtDatetimeEpt;
    private LocalDateTime forecastDatetimeBeginningUtc;
    private LocalDateTime forecastDatetimeBeginningEpt;
    private LocalDateTime forecastDatetimeEndingUtc;
    private LocalDateTime forecastDatetimeEndingEpt;
    private String forecastArea;
    private BigDecimal forecastLoadMw;

}
