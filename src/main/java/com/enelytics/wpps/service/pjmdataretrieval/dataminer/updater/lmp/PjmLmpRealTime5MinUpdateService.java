package com.enelytics.wpps.service.pjmdataretrieval.dataminer.updater.lmp;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLmp;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLmpType;
import com.enelytics.wpps.persistance.repositories.pjmdataretrieval.PjmLmpRepository;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.DataMinerSearchResponse;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp.PjmLmpRealTime5MinFetcher;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.fetcher.lmp.response.LmpResponseItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class PjmLmpRealTime5MinUpdateService {

    private static final Logger logger = LoggerFactory.getLogger(PjmLmpRealTime5MinUpdateService.class);

    private final PjmLmpRepository pjmLmpRepository;
    private final PjmLmpRealTime5MinFetcher pjmLmpRealTime5MinFetcher;
    private final Lock lock = new ReentrantLock();

    public PjmLmpRealTime5MinUpdateService(
            PjmLmpRepository pjmLmpRepository,
            PjmLmpRealTime5MinFetcher pjmLmpRealTime5MinFetcher) {
        this.pjmLmpRepository = pjmLmpRepository;
        this.pjmLmpRealTime5MinFetcher = pjmLmpRealTime5MinFetcher;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean updateInDedicatedTransaction() throws IOException, InvalidApiResponseException {
        return update();
    }

    /**
     * Update pjm_lmp real time 5 min data using Data Miner 2 data
     *
     * @return true if everything is updated, false otherwise
     *
     * @throws IOException network exception occurred
     */
    @Transactional
    public boolean update() throws IOException, InvalidApiResponseException {
        if (lock.tryLock()) {
            try {
                return updateRealTime5Min();
            } finally {
                lock.unlock();
            }
        } else {
            logger.warn("Cowardly refusing to run another update simultaneously");
            return false;
        }
    }

    private boolean updateRealTime5Min() throws IOException, InvalidApiResponseException {
        final List<PjmArea> areasToUpdate = Arrays.asList(
                PjmArea.PJM_RTO,
                PjmArea.MIDATL,
                PjmArea.APS,
                PjmArea.BGE,
                PjmArea.PEPCO,
                PjmArea.WESTERN_HUB
        );
        boolean isEverythingUpdatedUntilNow = true;

        for (PjmArea pjmArea : areasToUpdate) {
            isEverythingUpdatedUntilNow &= updateRealTime5Min(pjmArea);
        }

        return isEverythingUpdatedUntilNow;
    }

    private boolean updateRealTime5Min(PjmArea pjmArea) throws IOException, InvalidApiResponseException {
        Instant start = resolveStartDate(pjmArea);
        Instant end = start.plus(7, ChronoUnit.DAYS);
        boolean updatedTillNow = Instant.now().isBefore(end);

        if (updatedTillNow) {
            end = Instant.now().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.HOURS);
        }

        List<DataMinerSearchResponse<LmpResponseItem>> dataMinerResults = pjmLmpRealTime5MinFetcher.fetchRealTime5MinLmp(pjmArea, start, end);
        UpdateHelper.persist(dataMinerResults, pjmArea, PjmLmpType.RT_5M, pjmLmpRepository);
        logger.info("PJM Real-Time Five Min for {} updated in range {} .. {}", pjmArea, start, end);

        return updatedTillNow;
    }

    private Instant resolveStartDate(PjmArea pjmArea) throws IOException, InvalidApiResponseException {
        Optional<PjmLmp> latest = UpdateHelper.findLatest(pjmArea, PjmLmpType.RT_5M, pjmLmpRepository);

        if (latest.isPresent()) {
            return latest.get().getTime().toInstant();
        }

        return pjmLmpRealTime5MinFetcher.fetchStartDate(pjmArea);
    }

}

