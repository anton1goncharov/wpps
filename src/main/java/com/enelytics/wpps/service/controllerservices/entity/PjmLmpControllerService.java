
package com.enelytics.wpps.service.controllerservices.entity;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLmp;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLmpType;
import com.enelytics.wpps.persistance.repositories.pjmdataretrieval.PjmLmpRepository;
import com.enelytics.wpps.serialization.dto.pjmdataretrieval.PjmLmpDTO;
import com.enelytics.wpps.serialization.mappers.pjmdataretrieval.PjmLmpMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.enelytics.wpps.persistance.specifications.pjmdata.PjmLmpSpecification.*;

@Service
public class PjmLmpControllerService {

    private final PjmLmpMapper pjmLmpMapper;
    private final PjmLmpRepository pjmLmpRepository;

    public PjmLmpControllerService(PjmLmpMapper pjmLmpMapper, PjmLmpRepository pjmLmpRepository) {
        this.pjmLmpMapper = pjmLmpMapper;
        this.pjmLmpRepository = pjmLmpRepository;
    }

    public List<PjmLmpDTO> findAll(Date start, Date end, PjmArea area, PjmLmpType type) {
        Specification<PjmLmp> specification = Specification.where(between(start, end));

        if (area != null) {
            specification = specification.and(withArea(area));
        }

        if (type != null) {
            specification = specification.and(withType(type));
        }

        return pjmLmpRepository.findAll(specification).stream()
                .map(pjmLmpMapper::toPjmLmpDTO)
                .collect(Collectors.toList());
    }

    public PjmLmpDTO findOne(Long id) {
        PjmLmp pjmLmp = pjmLmpRepository.findById(id)
                .orElse(null);

        return pjmLmpMapper.toPjmLmpDTO(pjmLmp);
    }

}
