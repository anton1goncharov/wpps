
package com.enelytics.wpps.service.controllerservices.report;

import com.enelytics.wpps.persistance.domain.misc.PjmYear;
import com.enelytics.wpps.persistance.repositories.pjmdataretrieval.PjmLoadRepository;
import com.enelytics.wpps.service.reports.pjmhistoricalmax.PjmHistoricalMaxReport;
import com.enelytics.wpps.service.reports.pjmhistoricalmax.PjmHistoricalMaxReporter;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

@Service
public class PjmHistoricalMaxReportControllerService {

    private final PjmLoadRepository pjmLoadRepository;

    public PjmHistoricalMaxReportControllerService(PjmLoadRepository pjmLoadRepository) {
        this.pjmLoadRepository = pjmLoadRepository;
    }

    public PjmHistoricalMaxReport reportPjmForLastFullYear(int topN) {
        PjmYear lastFullYear = PjmYear.from(LocalDate.now().minusYears(1));
        PjmHistoricalMaxReporter reporter = new PjmHistoricalMaxReporter(pjmLoadRepository);
        return reporter.report(Collections.singleton(lastFullYear), topN);
    }

    public PjmHistoricalMaxReport reportPjmHistoricalMax(int topN) {
        PjmHistoricalMaxReporter reporter = new PjmHistoricalMaxReporter(pjmLoadRepository);
        SortedSet<PjmYear> years = new TreeSet<>();
        PjmYear lastYear = PjmYear.from(LocalDate.now());

        for (PjmYear year = PjmYear.of(2005); !year.isAfter(lastYear); year = year.plusYears(1))
            years.add(year);

        return reporter.report(years, topN);
    }

}
