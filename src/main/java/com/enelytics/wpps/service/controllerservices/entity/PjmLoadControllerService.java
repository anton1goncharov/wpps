
package com.enelytics.wpps.service.controllerservices.entity;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLoad;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLoadType;
import com.enelytics.wpps.persistance.repositories.pjmdataretrieval.PjmLoadRepository;
import com.enelytics.wpps.serialization.dto.pjmdataretrieval.PjmLoadDTO;
import com.enelytics.wpps.serialization.mappers.pjmdataretrieval.PjmLoadMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.enelytics.wpps.persistance.specifications.pjmdata.PjmLoadSpecification.*;

@Service
public class PjmLoadControllerService {

    private final PjmLoadMapper pjmLoadMapper;
    private final PjmLoadRepository pjmLoadRepository;

    public PjmLoadControllerService(PjmLoadMapper pjmLoadMapper, PjmLoadRepository pjmLoadRepository) {
        this.pjmLoadMapper = pjmLoadMapper;
        this.pjmLoadRepository = pjmLoadRepository;
    }

    public List<PjmLoadDTO> findAll(Date start, Date end, PjmArea area, PjmLoadType type) {
        Specification<PjmLoad> specification = Specification.where(between(start, end));

        if (area != null) {
            specification = specification.and(withArea(area));
        }

        if (type != null) {
            specification = specification.and(withType(type));
        }

        return pjmLoadRepository.findAll(specification).stream()
                .map(pjmLoadMapper::toPjmLoadDTO)
                .collect(Collectors.toList());
    }

    public PjmLoadDTO findOne(Long id) {
        PjmLoad pjmLoad = pjmLoadRepository.findById(id)
                .orElse(null);

        return pjmLoadMapper.toPjmLoadDTO(pjmLoad);
    }

}
