package com.enelytics.wpps.service.reports.pjmhistoricalmax;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLoad;
import com.enelytics.wpps.persistance.domain.misc.PjmYear;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.repositories.pjmdataretrieval.PjmLoadRepository;
import com.enelytics.wpps.utils.BigDecimalUtil;
import com.enelytics.wpps.utils.DateUtil;
import org.springframework.data.jpa.domain.Specification;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

import static com.enelytics.wpps.persistance.specifications.pjmdata.PjmLoadSpecification.between;
import static com.enelytics.wpps.persistance.specifications.pjmdata.PjmLoadSpecification.withArea;
import static com.enelytics.wpps.utils.DateUtil.toLocalDate;
import static com.enelytics.wpps.utils.DateUtil.toLocalDateTime;
import static java.math.BigDecimal.ZERO;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;
import static org.springframework.data.jpa.domain.Specification.where;

public class PjmHistoricalMaxReporter {

    private final PjmLoadRepository pjmLoadRepository;

    public PjmHistoricalMaxReporter(PjmLoadRepository pjmLoadRepository) {
        this.pjmLoadRepository = pjmLoadRepository;
    }

    public PjmHistoricalMaxReport report(Set<PjmYear> years, int topN) {
        List<PjmLoad> loads = selectByYears(years);
        Map<LocalDate, Long> daily = makeDaily(loads);

        PjmHistoricalMaxReport report = new PjmHistoricalMaxReport();
        for (Map.Entry<LocalDate, Long> entry : daily.entrySet()) {
            LocalDate day = entry.getKey();
            Long kwh = entry.getValue();

            PjmHistoricalMaxReport.Daily item = new PjmHistoricalMaxReport.Daily();
            item.setDay(day);
            item.setKwh(kwh);
            item.setYear(PjmYear.from(day));
            report.getDaily().add(item);
        }

        // Process top n for each year
        Map<PjmYear, List<PjmHistoricalMaxReport.Daily>> byYear = report.getDaily().stream()
                .collect(groupingBy(PjmHistoricalMaxReport.Daily::getYear));

        Map<PjmYear, List<LocalDate>> topDays = new TreeMap<>();
        for (Map.Entry<PjmYear, List<PjmHistoricalMaxReport.Daily>> entry : byYear.entrySet()) {
            PjmYear year = entry.getKey();
            List<PjmHistoricalMaxReport.Daily> days = entry.getValue();

            List<PjmHistoricalMaxReport.Daily> top = days.stream()
                    .sorted(Comparator.comparing(PjmHistoricalMaxReport.Daily::getKwh).reversed())
                    .limit(topN)
                    .collect(Collectors.toList());

            for (PjmHistoricalMaxReport.Daily d : top) {
                d.setMax(true); // mark day as top day
                if (!topDays.containsKey(year))
                    topDays.put(year, new ArrayList<>());
                topDays.get(year).add(d.getDay());
            }
        }

        fillHours(report, topDays, loads);
        return report;
    }

    private List<PjmLoad> selectByYears(Set<PjmYear> years) {
        Specification<PjmLoad> timeSpec = null;

        for (PjmYear pjmYear : years) {
            Date from = DateUtil.toDate(pjmYear.firstDay().atStartOfDay());
            Date to = DateUtil.toDate(pjmYear.lastDay().atTime(LocalTime.MAX));

            Specification<PjmLoad> thisYearSpec = between(from, to);

            if (timeSpec == null) {
                timeSpec = thisYearSpec;
            } else {
                timeSpec = where(timeSpec).or(thisYearSpec);
            }
        }

        return pjmLoadRepository.findAll(where(timeSpec).and(withArea(PjmArea.PJM_RTO)));
    }

    private Map<LocalDate, Long> makeDaily(List<PjmLoad> loads) {
        Map<LocalDate, BigDecimal> dailyExact = loads.stream()
                .collect(groupingBy(e -> toLocalDate(e.getTime()),
                        reducing(ZERO, PjmLoad::getLoadKw, BigDecimalUtil::add)));
        Map<LocalDate, Long> daily = new TreeMap<>();

        dailyExact.forEach((day, sum) -> daily.put(day, BigDecimalUtil.roundExactly(sum, RoundingMode.HALF_EVEN)));

        return daily;
    }

    private void fillHours(PjmHistoricalMaxReport report, Map<PjmYear, List<LocalDate>> topDays, List<PjmLoad> loads) {
        BinaryOperator<PjmLoad> maxLoadOp = (a, b) ->
                a != null && a.getLoadKw().compareTo(b.getLoadKw()) >= 0 ? a : b;

        Map<LocalDate, PjmLoad> maxHourByDay = loads.stream()
                .collect(groupingBy(load -> toLocalDate(load.getTime()), reducing(null, maxLoadOp)));

        for (Map.Entry<PjmYear, List<LocalDate>> tde : topDays.entrySet()) {
            PjmYear year = tde.getKey();
            List<LocalDate> days = tde.getValue();

            for (int i = 0; i < days.size(); i++) {
                PjmLoad load = maxHourByDay.get(days.get(i));
                if (load != null) {
                    PjmHistoricalMaxReport.TopHour topHour = new PjmHistoricalMaxReport.TopHour();
                    topHour.setYear(year);
                    topHour.setHour(toLocalDateTime(load.getTime()));
                    topHour.setIndex(i + 1);
                    topHour.setKw(BigDecimalUtil.roundExactly(load.getLoadKw(), RoundingMode.HALF_EVEN));
                    report.getTopHours().add(topHour);
                }
            }
        }
    }

}
