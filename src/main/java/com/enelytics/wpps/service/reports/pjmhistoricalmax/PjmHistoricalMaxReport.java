package com.enelytics.wpps.service.reports.pjmhistoricalmax;

import com.enelytics.wpps.persistance.domain.misc.PjmYear;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PjmHistoricalMaxReport {

    private List<Daily> daily = new ArrayList<>();

    private List<TopHour> topHours = new ArrayList<>();

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class Daily {

        @JsonSerialize(using = ToStringSerializer.class)
        private PjmYear year;

        @JsonSerialize(using = LocalDateSerializer.class)
        private LocalDate day;

        private long kwh;

        private boolean max;

    }

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class TopHour {

        @JsonSerialize(using = ToStringSerializer.class)
        private PjmYear year;

        @JsonSerialize(using = LocalDateTimeSerializer.class)
        private LocalDateTime hour;

        private long kw;

        private int index;

    }


}
