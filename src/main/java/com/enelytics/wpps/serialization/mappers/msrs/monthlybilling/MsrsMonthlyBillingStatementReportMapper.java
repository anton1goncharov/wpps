
package com.enelytics.wpps.serialization.mappers.msrs.monthlybilling;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementReport;
import com.enelytics.wpps.serialization.dto.msrs.monthlybilling.MsrsMonthlyBillingStatementReportDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MsrsMonthlyBillingStatementReportMapper {

    MsrsMonthlyBillingStatementReportDTO toMsrsMonthlyBillingStatementReportDTO(MsrsMonthlyBillingStatementReport report);

}