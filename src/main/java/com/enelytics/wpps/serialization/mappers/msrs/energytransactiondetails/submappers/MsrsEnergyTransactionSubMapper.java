package com.enelytics.wpps.serialization.mappers.msrs.energytransactiondetails.submappers;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransaction;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MsrsEnergyTransactionSubMapper {

    default Long toMsrsEnergyTransactionId(MsrsEnergyTransaction energyTransaction) {
        return energyTransaction == null ? null : energyTransaction.getId();
    }

}
