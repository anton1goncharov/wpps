
package com.enelytics.wpps.serialization.mappers.msrs.energytransactiondetails;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionHourlyValue;
import com.enelytics.wpps.serialization.dto.msrs.energytransactiondetails.MsrsEnergyTransactionHourlyValueDTO;
import com.enelytics.wpps.serialization.mappers.msrs.energytransactiondetails.submappers.MsrsEnergyTransactionSubMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {
        MsrsEnergyTransactionSubMapper.class
})
public interface MsrsEnergyTransactionHourlyValueMapper {

    @Mapping(source = "energyTransaction", target = "energyTransactionId")
    MsrsEnergyTransactionHourlyValueDTO toMsrsEnergyTransactionHourlyValueDTO(MsrsEnergyTransactionHourlyValue hourlyValue);

}