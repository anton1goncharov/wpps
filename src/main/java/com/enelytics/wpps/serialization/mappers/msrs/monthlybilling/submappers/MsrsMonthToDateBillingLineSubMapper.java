package com.enelytics.wpps.serialization.mappers.msrs.monthlybilling.submappers;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingLine;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MsrsMonthToDateBillingLineSubMapper {

    default Long toMsrsMonthToDateBillingLineId(MsrsMonthToDateBillingLine line) {
        return line == null ? null : line.getId();
    }

}
