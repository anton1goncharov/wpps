package com.enelytics.wpps.serialization.mappers.msrs.monthlybilling.submappers;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingSummaryReport;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MsrsMonthToDateBillingSummaryReportSubMapper {

    default Long toMsrsMonthToDateBillingSummaryReportId(MsrsMonthToDateBillingSummaryReport report) {
        return report == null ? null : report.getId();
    }

}
