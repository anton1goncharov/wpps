
package com.enelytics.wpps.serialization.mappers.msrs.monthlybilling;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingSummaryReport;
import com.enelytics.wpps.serialization.dto.msrs.monthlybilling.MsrsMonthToDateBillingSummaryReportDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MsrsMonthToDateBillingSummaryReportMapper {

    MsrsMonthToDateBillingSummaryReportDTO toMsrsMonthToDateBillingSummaryReportDTO(MsrsMonthToDateBillingSummaryReport report);

}