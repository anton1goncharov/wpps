
package com.enelytics.wpps.serialization.mappers.msrs.monthlybilling;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingLineDailyAmount;
import com.enelytics.wpps.serialization.dto.msrs.monthlybilling.MsrsMonthToDateBillingLineDailyAmountDTO;
import com.enelytics.wpps.serialization.mappers.msrs.monthlybilling.submappers.MsrsMonthToDateBillingLineSubMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {
        MsrsMonthToDateBillingLineSubMapper.class
})
public interface MsrsMonthToDateBillingLineDailyAmountMapper {

    @Mapping(source = "monthToDateBillingLine", target = "monthToDateBillingLineId")
    MsrsMonthToDateBillingLineDailyAmountDTO toMsrsMonthToDateBillingLineDailyAmountDTO(MsrsMonthToDateBillingLineDailyAmount dailyAmount);

}