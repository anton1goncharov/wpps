package com.enelytics.wpps.serialization.mappers.msrs.monthlybilling.submappers;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementReport;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MsrsMonthlyBillingStatementReportSubMapper {

    default Long toMsrsMonthlyBillingStatementReportId(MsrsMonthlyBillingStatementReport report) {
        return report == null ? null : report.getId();
    }

}
