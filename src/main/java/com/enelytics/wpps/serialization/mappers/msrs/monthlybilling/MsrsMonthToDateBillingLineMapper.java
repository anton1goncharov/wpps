
package com.enelytics.wpps.serialization.mappers.msrs.monthlybilling;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthToDateBillingLine;
import com.enelytics.wpps.serialization.dto.msrs.monthlybilling.MsrsMonthToDateBillingLineDTO;
import com.enelytics.wpps.serialization.mappers.msrs.monthlybilling.submappers.MsrsMonthToDateBillingSummaryReportSubMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {
        MsrsMonthToDateBillingSummaryReportSubMapper.class
})
public interface MsrsMonthToDateBillingLineMapper {

    @Mapping(source = "monthToDateBillingSummaryReport", target = "monthToDateBillingSummaryReportId")
    MsrsMonthToDateBillingLineDTO toMsrsMonthToDateBillingLineDTO(MsrsMonthToDateBillingLine line);

}