package com.enelytics.wpps.serialization.mappers.msrs.energytransactiondetails.submappers;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionsReport;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MsrsEnergyTransactionsReportSubMapper {

    default Long toMsrsEnergyTransactionsReportId(MsrsEnergyTransactionsReport report) {
        return report == null ? null : report.getId();
    }

}
