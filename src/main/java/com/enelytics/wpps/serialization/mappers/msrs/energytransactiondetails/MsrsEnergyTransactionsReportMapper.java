
package com.enelytics.wpps.serialization.mappers.msrs.energytransactiondetails;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransactionsReport;
import com.enelytics.wpps.serialization.dto.msrs.energytransactiondetails.MsrsEnergyTransactionsReportDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MsrsEnergyTransactionsReportMapper {

    MsrsEnergyTransactionsReportDTO toMsrsEnergyTransactionsReportDTO(MsrsEnergyTransactionsReport report);

}