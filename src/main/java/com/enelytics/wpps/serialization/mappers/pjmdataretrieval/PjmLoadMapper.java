
package com.enelytics.wpps.serialization.mappers.pjmdataretrieval;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLoad;
import com.enelytics.wpps.serialization.dto.pjmdataretrieval.PjmLoadDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PjmLoadMapper {

    PjmLoadDTO toPjmLoadDTO(PjmLoad pjmLoad);

}