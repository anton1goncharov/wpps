
package com.enelytics.wpps.serialization.mappers.msrs.monthlybilling;

import com.enelytics.wpps.persistance.domain.entities.msrs.monthlybilling.MsrsMonthlyBillingStatementLine;
import com.enelytics.wpps.serialization.dto.msrs.monthlybilling.MsrsMonthlyBillingStatementLineDTO;
import com.enelytics.wpps.serialization.mappers.msrs.monthlybilling.submappers.MsrsMonthlyBillingStatementReportSubMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {
        MsrsMonthlyBillingStatementReportSubMapper.class
})
public interface MsrsMonthlyBillingStatementLineMapper {

    @Mapping(source = "monthlyBillingStatementReport", target = "monthlyBillingStatementReportId")
    MsrsMonthlyBillingStatementLineDTO toMsrsMonthlyBillingStatementLineDTO(MsrsMonthlyBillingStatementLine line);

}