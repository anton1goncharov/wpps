
package com.enelytics.wpps.serialization.mappers.pjmdataretrieval;

import com.enelytics.wpps.persistance.domain.entities.pjmdataretrieval.PjmLmp;
import com.enelytics.wpps.serialization.dto.pjmdataretrieval.PjmLmpDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PjmLmpMapper {

    PjmLmpDTO toPjmLmpDTO(PjmLmp pjmLmp);

}