
package com.enelytics.wpps.serialization.mappers.msrs.energytransactiondetails;

import com.enelytics.wpps.persistance.domain.entities.msrs.energytransactiondetails.MsrsEnergyTransaction;
import com.enelytics.wpps.serialization.dto.msrs.energytransactiondetails.MsrsEnergyTransactionDTO;
import com.enelytics.wpps.serialization.mappers.msrs.energytransactiondetails.submappers.MsrsEnergyTransactionsReportSubMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {
        MsrsEnergyTransactionsReportSubMapper.class
})
public interface MsrsEnergyTransactionMapper {

    @Mapping(source = "energyTransactionsReport", target = "energyTransactionsReportId")
    MsrsEnergyTransactionDTO toMsrsEnergyTransactionDTO(MsrsEnergyTransaction energyTransaction);

}