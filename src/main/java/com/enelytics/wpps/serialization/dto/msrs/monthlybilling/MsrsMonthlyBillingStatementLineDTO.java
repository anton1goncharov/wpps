package com.enelytics.wpps.serialization.dto.msrs.monthlybilling;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MsrsMonthlyBillingStatementLineDTO extends MsrsMonthlyBillingLineDTO {

    private Long monthlyBillingStatementReportId;

}
