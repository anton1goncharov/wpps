package com.enelytics.wpps.serialization.dto.msrs.monthlybilling;

import com.enelytics.wpps.serialization.json.BigDecimalSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class MsrsMonthToDateBillingSummaryReportDTO extends MsrsMonthlyBillingReportDTO {

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal monthLastWeeklyBillingNetTotal;

}
