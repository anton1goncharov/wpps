package com.enelytics.wpps.serialization.dto.msrs.energytransactiondetails;

import com.enelytics.wpps.persistance.domain.types.msrs.MsrsEnergyTransactionType;
import com.enelytics.wpps.serialization.dto.msrs.MsrsReportDTO;
import com.enelytics.wpps.serialization.json.StringAttributeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MsrsEnergyTransactionsReportDTO extends MsrsReportDTO {

    @JsonSerialize(using = StringAttributeSerializer.class)
    private MsrsEnergyTransactionType energyTransactionType;

}
