package com.enelytics.wpps.serialization.dto.msrs.monthlybilling;

import com.enelytics.wpps.persistance.domain.types.msrs.MsrsBillingChargeType;
import com.enelytics.wpps.serialization.json.BigDecimalSerializer;
import com.enelytics.wpps.serialization.json.LocalDateSerializer;
import com.enelytics.wpps.serialization.json.StringAttributeSerializer;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
abstract class MsrsMonthlyBillingLineDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Integer orderIndex;

    @JsonSerialize(using = StringAttributeSerializer.class)
    private MsrsBillingChargeType billingChargeType;

    private String chargeCode;
    private Boolean adjustment;
    private String name;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate sourceBillingPeriodStart;

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal amount;

}
