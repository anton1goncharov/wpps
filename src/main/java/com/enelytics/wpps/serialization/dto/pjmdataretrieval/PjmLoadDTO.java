package com.enelytics.wpps.serialization.dto.pjmdataretrieval;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLoadType;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PjmLoadDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Date time;
    private PjmArea area;
    private PjmLoadType type;
    private BigDecimal loadKw;

}
