package com.enelytics.wpps.serialization.dto.msrs.energytransactiondetails;

import com.enelytics.wpps.serialization.json.BigDecimalSerializer;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MsrsEnergyTransactionHourlyValueDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Date time;

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal kwh;

    private Long energyTransactionId;

}
