package com.enelytics.wpps.serialization.dto.msrs.monthlybilling;

import com.enelytics.wpps.serialization.dto.msrs.MsrsReportDTO;
import com.enelytics.wpps.serialization.json.BigDecimalSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public abstract class MsrsMonthlyBillingReportDTO extends MsrsReportDTO {

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal totalCharges;

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal totalCredits;

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal monthlyBillingNetTotal;

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal totalDueReceivable;

}
