package com.enelytics.wpps.serialization.dto.msrs.energytransactiondetails;

import com.enelytics.wpps.serialization.json.BigDecimalSerializer;
import com.enelytics.wpps.serialization.json.LocalDateSerializer;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MsrsEnergyTransactionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Integer orderIndex;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate date;

    private String transactionType;
    private String transactionId;

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal totalKwh;

    private String sourcePnodeName;
    private String sourcePnodeId;
    private String sinkPnodeName;
    private String sinkPnodeId;
    private String seller;
    private String buyer;
    private Long energyTransactionsReportId;

}
