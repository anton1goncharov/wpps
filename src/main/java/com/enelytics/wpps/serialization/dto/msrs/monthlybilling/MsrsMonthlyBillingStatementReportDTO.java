package com.enelytics.wpps.serialization.dto.msrs.monthlybilling;

import com.enelytics.wpps.serialization.json.BigDecimalSerializer;
import com.enelytics.wpps.serialization.json.LocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class MsrsMonthlyBillingStatementReportDTO extends MsrsMonthlyBillingReportDTO {

    private String customerCode;
    private String customerId;
    private String invoiceNumber;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime finalBillingStatementIssued;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime invoiceDueDate;

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal previousWeeklyBillingNetTotal;

}
