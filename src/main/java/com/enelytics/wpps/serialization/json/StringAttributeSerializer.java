package com.enelytics.wpps.serialization.json;

import com.enelytics.wpps.persistance.domain.StringAttribute;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class StringAttributeSerializer extends NonNullSerializer<StringAttribute> {

    @Override
    public void serializeNonNull(StringAttribute value, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
        jsonGenerator.writeString(value.value());
    }

}