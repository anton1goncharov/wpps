package com.enelytics.wpps.serialization.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.NonNull;

import java.io.IOException;

public abstract class NonNullSerializer<T> extends JsonSerializer<T> {

    @Override
    public void serialize(T value,
                          JsonGenerator jsonGenerator,
                          SerializerProvider provider) throws IOException {

        if (value == null) {
            jsonGenerator.writeNull();
        } else {
            serializeNonNull(value, jsonGenerator, provider);
        }
    }

    /**
     * Serializes non-null value.
     * Does not need explicit null-check.
     * @param value to serialize
     * @param jsonGenerator
     * @param provider
     * @throws IOException
     */
    public abstract void serializeNonNull(@NonNull T value,
                                JsonGenerator jsonGenerator,
                                SerializerProvider provider) throws IOException;

}
