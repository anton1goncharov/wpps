package com.enelytics.wpps.serialization.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

public class BigDecimalSerializer extends NonNullSerializer<BigDecimal> {

    @Override
    public void serializeNonNull(BigDecimal value,
                          JsonGenerator jsonGenerator,
                          SerializerProvider provider) throws IOException {
        jsonGenerator.writeNumber(value.stripTrailingZeros().toPlainString());
    }

}
