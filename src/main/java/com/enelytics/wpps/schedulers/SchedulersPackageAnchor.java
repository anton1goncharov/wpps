package com.enelytics.wpps.schedulers;

/**
 *  The only purpose is to be used in {@code @ComponentScan} as {@code basePackageClasses} value.
 */
public class SchedulersPackageAnchor {
    private SchedulersPackageAnchor(){}
}
