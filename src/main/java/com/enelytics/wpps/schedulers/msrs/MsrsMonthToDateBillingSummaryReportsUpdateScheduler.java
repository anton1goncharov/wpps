package com.enelytics.wpps.schedulers.msrs;

import com.enelytics.wpps.service.msrs.updater.MsrsMonthToDateBillingSummaryReportsUpdateService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class MsrsMonthToDateBillingSummaryReportsUpdateScheduler {

    private final MsrsMonthToDateBillingSummaryReportsUpdateService updateService;

    public MsrsMonthToDateBillingSummaryReportsUpdateScheduler(
            MsrsMonthToDateBillingSummaryReportsUpdateService updateService) {
        this.updateService = updateService;
    }

    @Scheduled(cron = "${scheduler.MsrsMonthToDateBillingSummaryReportsUpdateScheduler.cron}")
    public void updateMonthToDateBillingSummaryReports() {
        updateService.updateMonthToDateBillingSummaryReports();
    }

}
