package com.enelytics.wpps.schedulers.msrs;

import com.enelytics.wpps.service.msrs.updater.MsrsMonthlyBillingStatementReportsUpdateService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class MsrsMonthlyBillingStatementReportsUpdateScheduler {

    private final MsrsMonthlyBillingStatementReportsUpdateService updateService;

    public MsrsMonthlyBillingStatementReportsUpdateScheduler(
            MsrsMonthlyBillingStatementReportsUpdateService updateService) {
        this.updateService = updateService;
    }

    @Scheduled(cron = "${scheduler.MsrsMonthlyBillingStatementReportsUpdateScheduler.cron}")
    public void updateMonthlyBillingStatementReports() {
        updateService.updateMonthlyBillingStatementReports();
    }

}
