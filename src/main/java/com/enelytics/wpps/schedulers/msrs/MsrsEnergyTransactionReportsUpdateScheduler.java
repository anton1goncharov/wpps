package com.enelytics.wpps.schedulers.msrs;

import com.enelytics.wpps.service.msrs.parser.exception.MsrsReportParsingException;
import com.enelytics.wpps.service.msrs.updater.MsrsEnergyTransactionsReportsUpdateService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MsrsEnergyTransactionReportsUpdateScheduler {

    private final MsrsEnergyTransactionsReportsUpdateService updateService;

    public MsrsEnergyTransactionReportsUpdateScheduler(
            MsrsEnergyTransactionsReportsUpdateService updateService) {
        this.updateService = updateService;
    }

    @Scheduled(cron = "${scheduler.MsrsEnergyTransactionReportUpdateScheduler.DA.cron}")
    public void updateDADailyEnergyTransactionsReports() throws IOException, MsrsReportParsingException {
        updateService.updateDADailyEnergyTransactionsReports();
    }

    @Scheduled(cron = "${scheduler.MsrsEnergyTransactionReportUpdateScheduler.RT.cron}")
    public void updateRTDailyEnergyTransactionsReports() throws IOException, MsrsReportParsingException {
        updateService.updateRTDailyEnergyTransactionsReports();
    }

}
