package com.enelytics.wpps.schedulers.pjmdataretrieval;

import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.updater.lmp.PjmLmpDayAheadHourlyUpdateService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class PjmLmpDayAheadHourlyUpdateScheduler {

    private final PjmLmpDayAheadHourlyUpdateService lmpDayAheadHourlyUpdateService;

    public PjmLmpDayAheadHourlyUpdateScheduler(
            PjmLmpDayAheadHourlyUpdateService lmpDayAheadHourlyUpdateService) {
        this.lmpDayAheadHourlyUpdateService = lmpDayAheadHourlyUpdateService;
    }

    @Scheduled(cron = "${scheduler.PjmLmpDayAheadHourlyUpdateScheduler.cron}")
    public void updateDayAhead() throws IOException, InvalidApiResponseException {
        // This loop added to speed up the initial data load
        // Once lmp data is fresh, no looping required
        for (int i = 0; i < 100; i++) {
            if (lmpDayAheadHourlyUpdateService.updateInDedicatedTransaction()) {
                break;
            }
        }
    }

}
