package com.enelytics.wpps.schedulers.pjmdataretrieval;

import com.enelytics.wpps.service.pjmdataretrieval.dataviewer.InvalidDataException;
import com.enelytics.wpps.service.pjmdataretrieval.dataviewer.updater.load.PjmLoadDataViewerUpdateService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class PjmLoadDataViewerUpdateScheduler {

    private final PjmLoadDataViewerUpdateService pjmLoadDataViewerUpdateService;

    public PjmLoadDataViewerUpdateScheduler(
            PjmLoadDataViewerUpdateService pjmLoadDataViewerUpdateService) {
        this.pjmLoadDataViewerUpdateService = pjmLoadDataViewerUpdateService;
    }

    @Scheduled(cron = "${scheduler.PjmLoadDataViewerUpdateScheduler.cron}")
    public void updatePjmLoadInstantaneous() throws IOException, InvalidDataException {
        pjmLoadDataViewerUpdateService.update();
    }

}
