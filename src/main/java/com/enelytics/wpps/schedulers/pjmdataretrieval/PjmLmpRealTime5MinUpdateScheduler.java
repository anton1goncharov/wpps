package com.enelytics.wpps.schedulers.pjmdataretrieval;

import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.updater.lmp.PjmLmpRealTime5MinUpdateService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class PjmLmpRealTime5MinUpdateScheduler {

    private final PjmLmpRealTime5MinUpdateService pjmLmpRealTime5MinUpdateService;

    public PjmLmpRealTime5MinUpdateScheduler(
            PjmLmpRealTime5MinUpdateService pjmLmpRealTime5MinUpdateService) {
        this.pjmLmpRealTime5MinUpdateService = pjmLmpRealTime5MinUpdateService;
    }

    @Scheduled(cron = "${scheduler.PjmLmpRealTime5MinUpdateScheduler.cron}")
    public void updateRealTimeFiveMinute() throws IOException, InvalidApiResponseException {
        // This loop added to speed up the initial data load
        // Once lmp data is fresh, no looping required
        for (int i = 0; i < 100; i++) {
            if (pjmLmpRealTime5MinUpdateService.updateInDedicatedTransaction()) {
                break;
            }
        }
    }

}
