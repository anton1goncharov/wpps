package com.enelytics.wpps.schedulers.pjmdataretrieval;

import com.enelytics.wpps.service.pjmdataretrieval.dataminer.updater.load.PjmLoadHourlyForecastUpdateService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class PjmLoadHourlyForecastUpdateScheduler {

    private final PjmLoadHourlyForecastUpdateService pjmLoadHourlyForecastUpdateService;

    public PjmLoadHourlyForecastUpdateScheduler(
            PjmLoadHourlyForecastUpdateService pjmLoadHourlyForecastUpdateService) {
        this.pjmLoadHourlyForecastUpdateService = pjmLoadHourlyForecastUpdateService;
    }

    @Scheduled(cron = "${scheduler.PjmLoadHourlyForecastUpdateScheduler.cron}")
    public void updateForecast() throws IOException {
        pjmLoadHourlyForecastUpdateService.update();
    }

}
