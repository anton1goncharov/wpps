package com.enelytics.wpps.schedulers.pjmdataretrieval;

import com.enelytics.wpps.service.pjmdataretrieval.dataminer.api.InvalidApiResponseException;
import com.enelytics.wpps.service.pjmdataretrieval.dataminer.updater.common.PjmPricingNodeIdUpdateService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class PjmPricingNodeIdUpdateScheduler {

    private final PjmPricingNodeIdUpdateService pjmPricingNodeIdUpdateService;

    public PjmPricingNodeIdUpdateScheduler(PjmPricingNodeIdUpdateService pricingNodeIdUpdater) {
        this.pjmPricingNodeIdUpdateService = pricingNodeIdUpdater;
    }

    @Scheduled(cron = "${scheduler.PjmPricingNodeIdUpdateScheduler.cron}")
    public void updatePricingNodeIds() throws IOException, InvalidApiResponseException {
        pjmPricingNodeIdUpdateService.updatePricingNodeIds();
    }

}
