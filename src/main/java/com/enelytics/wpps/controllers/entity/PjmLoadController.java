
package com.enelytics.wpps.controllers.entity;

import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmArea;
import com.enelytics.wpps.persistance.domain.types.pjmdataretrieval.PjmLoadType;
import com.enelytics.wpps.serialization.dto.pjmdataretrieval.PjmLoadDTO;
import com.enelytics.wpps.service.controllerservices.entity.PjmLoadControllerService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1/rest/pjm_load")
public class PjmLoadController {

    private final PjmLoadControllerService controllerService;

    public PjmLoadController(PjmLoadControllerService controllerService) {
        this.controllerService = controllerService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findAll(@RequestParam(value = "start") @DateTimeFormat(iso = DATE) Date start,
                                  @RequestParam(value = "end") @DateTimeFormat(iso = DATE) Date end,
                                  @RequestParam(value = "area", required = false) PjmArea area,
                                  @RequestParam(value = "type", required = false) PjmLoadType type) {
        final List<PjmLoadDTO> pjmLoads = controllerService.findAll(start, end, area, type);

        return new ResponseEntity<>(pjmLoads, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findOne(@PathVariable(value = "id") Long id) {
        final PjmLoadDTO pjmLoad = controllerService.findOne(id);

        return new ResponseEntity<>(pjmLoad, HttpStatus.OK);
    }

}

