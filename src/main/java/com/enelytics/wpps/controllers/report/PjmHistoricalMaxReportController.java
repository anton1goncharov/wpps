package com.enelytics.wpps.controllers.report;

import com.enelytics.wpps.service.controllerservices.report.PjmHistoricalMaxReportControllerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/v1/pjm_data_retrieval/")
public class PjmHistoricalMaxReportController {

    private final PjmHistoricalMaxReportControllerService controllerService;

    public PjmHistoricalMaxReportController(PjmHistoricalMaxReportControllerService controllerService) {
        this.controllerService = controllerService;
    }

    @GetMapping(value = "/pjm_historical_max_report", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity reportPjmHistoricalMax(@RequestParam(value = "top_n", defaultValue = "5") int topN) {
        return ok(controllerService.reportPjmHistoricalMax(topN));
    }

}
